% run after FisherMatrixSIS_tomo

i1=3
j1=4
i2=1
j2=7
figure;
Font=20;

subplot(2,2,1);
epsilon1=(IG(i1,j1,:)-IG_SC(i1,j1,:))./IG_SC(i1,j1,:);
epsilon1=reshape(epsilon1,[1 27]);
epsilon2=(IG(i2,j2,:)-IG_SC(i2,j2,:))./IG_SC(i2,j2,:);
epsilon2=reshape(epsilon2,[1 27]);
plot(l,epsilon1);
hold on;
plot(l,epsilon2);
hold off;
xlabel('$\ell$','Interpreter','LaTex','FontSize',Font);
set(gca,'xscale','log','FontSize',Font);
ylabel('$\epsilon_{ij}$','Interpreter','LaTex','FontSize',Font);
xlim(gca,[10 10000]);

subplot(2,2,2);
plot([l(1) l(27)],[Wij(i1,j1) Wij(i1,j1)]); lgd{1}=['i=',num2str(i1),', j=',num2str(j1)];
hold on;
plot([l(1) l(27)],[Wij(i2,j2) Wij(i2,j2)]); lgd{2}=['i=',num2str(i2),', j=',num2str(j2)];
hold off;
xlabel('$\ell$','Interpreter','LaTex','FontSize',Font);
set(gca,'xscale','log','FontSize',Font);
ylabel('$W_{ij}$','Interpreter','LaTex','FontSize',Font);
ylim(gca,[0.5e-5 5.5e-5]);
xlim(gca,[10 10000]);
legend(lgd,'FontSize',Font);

subplot(2,2,3);
plot([l(1) l(27)],[Delta_i(i1) Delta_i(i1)]);
hold on;
plot([l(1) l(27)],[Delta_i(i2) Delta_i(i2)]);
hold off;
xlabel('$\ell$','Interpreter','LaTex','FontSize',Font);
set(gca,'xscale','log','FontSize',Font);
ylabel('$\Delta_i$','Interpreter','LaTex','FontSize',Font);
ylim(gca,[950 1200]);
xlim(gca,[10 10000]);

subplot(2,2,4);
plot(l,Q(i1,:));
hold on;
plot(l,Q(i2,:));
hold off;
xlabel('$\ell$','Interpreter','LaTex','FontSize',Font);
set(gca,'xscale','log','FontSize',Font);
ylabel('$Q_i$','Interpreter','LaTex','FontSize',Font);
xlim(gca,[10 10000]);



i=3
j=4

D_Ig_ii=sqrt( (1/2./(2*l+1)./(0.2*l))' .* ( reshape(gg(i,i,:).*GG(i,i,:),[1 27]) + (1+1./(3*(1-Q(i)).^2)) .* ( reshape(gg(i,i,:),[1 27]).*GGN(i)+ggN(i).*reshape(GG(i,i,:)+II(i,i,:),[1 27]) ) + ggN(i)*GGN(i)*(1+1./(1-Q(i)).^2) ) );
D_b_b=1/2* sqrt( (2./(2*l+1)./(0.2*l)/fsky)') .*(1+ggN(i)./reshape(gg(i,i,:),[1 27]));
f1=D_Ig_ii.*Wij(i,j).*Delta_i(i)/b(i)./reshape(GG(i,j,:),[1 27]);
f2=D_b_b.*Wij(i,j).*Delta_i(i).*reshape(-Ig(i,i,:),[1 27])/b(i)./reshape(GG(i,j,:),[1 27]);

e_min_ij=sqrt( ( reshape(GG(i,j,:),[1 27]).^2+reshape((GG(i,i,:)+GGN(i)).*(GG(j,j,:)+GGN(j)),[1 27]) ) ./ (2*l'*0.2.*l'*fsky.*reshape(GG(i,j,:),[1 27]).^2) );

figure;
plot(l,e_min_ij,'k','LineWidth',2);
set(gca,'xscale','log');
set(gca,'yscale','log');
lgd{1}='\Delta C^{GG}/C^{GG}';

hold on;
plot(l,f1,'b--','LineWidth',2);
lgd{2}='C^{IG(SC)}\Delta C^{Ig}/C^{Ig} / C^{GG}';
%lgd{2}='Error from C^{Ig} meansurement'
plot(l,f2,'r--','LineWidth',2);
lgd{3}='C^{IG(SC)}\Delta b_i/b_i / C^{GG}';
%lgd{3}='Error from b_i measurement';
hold off;
xlabel('$\ell$','Interpreter','LaTex','FontSize',Font);
ylabel('propagated fractional measurement error','FontSize',Font)

legend(lgd,'FontSize',15);
xlim(gca,[20 5000]);
set(gca,'xscale','log','FontSize',15);
set(gca,'yscale','log','FontSize',15);

saveas(gca,'f','png');
