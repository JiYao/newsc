import ConfigParser
config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')

partial=0.0015
dw0=0.1
dwa=0.2

omega_m=float(config.get('cosmological_parameters','omega_m'))
h0=float(config.get('cosmological_parameters','h0'))
omega_b=float(config.get('cosmological_parameters','omega_b'))
sigma8=float(config.get('cosmological_parameters','sigma8_input'))
tau=float(config.get('cosmological_parameters','tau'))
n_s=float(config.get('cosmological_parameters','n_s'))
A_s=float(config.get('cosmological_parameters','A_s'))
omega_k=float(config.get('cosmological_parameters','omega_k'))
w=float(config.get('cosmological_parameters','w'))
wa=float(config.get('cosmological_parameters','wa'))

dA=0.02
dalpha=0.01

A=float(config.get('intrinsic_alignment_parameters','A'))
alpha=float(config.get('intrinsic_alignment_parameters','alpha'))

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=omega_m*(1.+partial)
config.set('cosmological_parameters','omega_m',t)
with open(r'valuesSC6_1p.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=omega_m*(1.-partial)
config.set('cosmological_parameters','omega_m',t)
with open(r'valuesSC6_1m.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=h0*(1.+partial)
config.set('cosmological_parameters','h0',t)
with open(r'valuesSC6_2p.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=h0*(1.-partial)
config.set('cosmological_parameters','h0',t)
with open(r'valuesSC6_2m.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=sigma8*(1.+partial)
config.set('cosmological_parameters','sigma8_input',t)
with open(r'valuesSC6_3p.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=sigma8*(1.-partial)
config.set('cosmological_parameters','sigma8_input',t)
with open(r'valuesSC6_3m.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=n_s*(1.+partial)
config.set('cosmological_parameters','n_s',t)
with open(r'valuesSC6_4p.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=n_s*(1.-partial)
config.set('cosmological_parameters','n_s',t)
with open(r'valuesSC6_4m.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=w+dw0
config.set('cosmological_parameters','w',t)
with open(r'valuesSC6_7p.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=w-dw0
config.set('cosmological_parameters','w',t)
with open(r'valuesSC6_7m.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=wa+dwa
config.set('cosmological_parameters','wa',t)
with open(r'valuesSC6_8p.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=wa-dwa
config.set('cosmological_parameters','wa',t)
with open(r'valuesSC6_8m.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=A+dA
config.set('intrinsic_alignment_parameters','A',t)
with open(r'valuesSC6_11p.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=A-dA
config.set('intrinsic_alignment_parameters','A',t)
with open(r'valuesSC6_11m.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=alpha+dalpha
config.set('intrinsic_alignment_parameters','alpha',t)
with open(r'valuesSC6_12p.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'valuesSC6_fiducial.ini')
t=alpha-dalpha
config.set('intrinsic_alignment_parameters','alpha',t)
with open(r'valuesSC6_12m.ini', 'wb') as configfile:
    config.write(configfile)

print '~~~~ Complete: Generating input files for partial derivatives ~~~~'
