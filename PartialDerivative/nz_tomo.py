import fitsio as fio
import numpy as np

NZ_SAMPLE=fio.FITS('modified_cfhtlens_heymans13.fits')['NZ_SAMPLE'].read()

def pdf(zph,z):
	z0=0.3
	return 1./2./z0*(z/z0)**2*np.exp(-z/z0) * 1./np.sqrt(2.*np.pi)/0.05/(1+z)*np.exp(-(z-zph)**2/2./(0.05*(1+z))**2)

Nbin=7
	
step=NZ_SAMPLE['Z_MID'][2]-NZ_SAMPLE['Z_MID'][1]
normlize=np.zeros(Nbin)
nz=np.zeros((Nbin,len(NZ_SAMPLE['Z_MID'])))

for i in range(len(NZ_SAMPLE['Z_MID'])):
	z=NZ_SAMPLE['Z_MID'][i]
	for zph in NZ_SAMPLE['Z_MID']:
		bin=(zph-0.4)/0.2
		if (bin>=0 and bin<Nbin):
			bin=int(bin)
			nz[bin][i]=nz[bin][i]+pdf(zph,z)*step

for i in range(Nbin):
	normlize[i]=sum(nz[i])*step

NZ_SAMPLE['BIN1']=nz[0]/normlize[0]
NZ_SAMPLE['BIN2']=nz[1]/normlize[1]
NZ_SAMPLE['BIN3']=nz[2]/normlize[2]
NZ_SAMPLE['BIN4']=nz[3]/normlize[3]
NZ_SAMPLE['BIN5']=nz[4]/normlize[4]
NZ_SAMPLE['BIN6']=nz[5]/normlize[5]
NZ_SAMPLE['BIN7']=nz[6]/normlize[6]

print 'int n_1 / normlize = ',sum(NZ_SAMPLE['BIN1'])*step
print 'sum n_1..n_6 = ',sum(normlize)

f=fio.FITS('modified_cfhtlens_heymans13.fits','rw')
f['NZ_SAMPLE'].write(NZ_SAMPLE,clobber=True)
f.close()

f=open('nbar_i.txt','w')
print >>f, normlize[0]
print >>f, normlize[1]
print >>f, normlize[2]
print >>f, normlize[3]
print >>f, normlize[4]
print >>f, normlize[5]
f.close()

print 'Finished'
