import fitsio as fio
import numpy as np

import ConfigParser
config= ConfigParser.RawConfigParser()
config.read(r'photoz_para.ini')

sigma_z=float(config.get('photo-z parameters','sigma_z'))
Delta_z=float(config.get('photo-z parameters','Delta_z'))

zmax = 3.5

NZ_SAMPLE=fio.FITS('modified_cfhtlens_heymans13.fits')['NZ_SAMPLE'].read()
# NZ_SAMPLE=NZ_SAMPLE[NZ_SAMPLE['Z_MID']<=zmax]
# NZ_SAMPLE[NZ_SAMPLE['Z_MID']>zmax] = 0.

zmin = 0.4
zmed = 0.9
# z0 = zmed/1.412
z0 = 0.5
a = 1.27
b = 1.02

def pdf(zph,z):
	global z0, a, b
	return z**a * np.exp( -(z/z0)**b ) * 1./np.sqrt(2.*np.pi)/sigma_z/(1+z)*np.exp(-(z-zph-Delta_z)**2/2./(sigma_z*(1+z))**2)

def n_all(z):
	global z0 ,a ,b
	return z**a * np.exp( -(z/z0)**b )

Nbin=20

step_z=NZ_SAMPLE['Z_MID'][2]-NZ_SAMPLE['Z_MID'][1]
normlize=np.zeros(Nbin)
nz=np.zeros((Nbin,len(NZ_SAMPLE['Z_MID'])))

norm_all = sum( n_all(NZ_SAMPLE['Z_MID']) * (NZ_SAMPLE['Z_MID'][1]-NZ_SAMPLE['Z_MID'][0]) )

for i in range(len(NZ_SAMPLE['Z_MID'])):
	z=NZ_SAMPLE['Z_MID'][i]
	step_zph=0.01
	for zph in [zmin+0.01*j+0.005 for j in range(10*(Nbin+1))]:
		bin_num=(zph-zmin)/0.1
		if (bin_num>=0 and bin_num<Nbin):
			bin_num=int(bin_num)
			nz[bin_num][i]=nz[bin_num][i]+pdf(zph,z)*step_zph

for i in range(Nbin):
	nz[i][ NZ_SAMPLE['Z_MID']>zmax ] = 0.
	normlize[i]=sum(nz[i])*step_z

NZ_SAMPLE['BIN1']=nz[0]/normlize[0]
NZ_SAMPLE['BIN2']=nz[1]/normlize[1]
NZ_SAMPLE['BIN3']=nz[2]/normlize[2]
NZ_SAMPLE['BIN4']=nz[3]/normlize[3]
NZ_SAMPLE['BIN5']=nz[4]/normlize[4]
NZ_SAMPLE['BIN6']=nz[5]/normlize[5]
NZ_SAMPLE['BIN7']=nz[6]/normlize[6]
NZ_SAMPLE['BIN8']=nz[7]/normlize[7]
NZ_SAMPLE['BIN9']=nz[8]/normlize[8]
NZ_SAMPLE['BIN10']=nz[9]/normlize[9]
NZ_SAMPLE['BIN11']=nz[10]/normlize[10]
NZ_SAMPLE['BIN12']=nz[11]/normlize[11]
NZ_SAMPLE['BIN13']=nz[12]/normlize[12]
NZ_SAMPLE['BIN14']=nz[13]/normlize[13]
NZ_SAMPLE['BIN15']=nz[14]/normlize[14]
NZ_SAMPLE['BIN16']=nz[15]/normlize[15]
NZ_SAMPLE['BIN17']=nz[16]/normlize[16]
NZ_SAMPLE['BIN18']=nz[17]/normlize[17]
NZ_SAMPLE['BIN19']=nz[18]/normlize[18]
NZ_SAMPLE['BIN20']=nz[19]/normlize[19]

print 'int n_1 / normlize = ',sum(NZ_SAMPLE['BIN1'])*step_z
print 'sum n_1..n_15 = ',sum(normlize) /norm_all

f=fio.FITS('modified_cfhtlens_heymans13.fits','rw')
f['NZ_SAMPLE'].write(NZ_SAMPLE,clobber=True)
f.close()

# to add a tomographic bin, use:
# f=fio.FITS('modified_cfhtlens_heymans13.fits','rw')
# f[-1].insert_column(name='BIN#',data=nz[#-1]/normlize[#-1])

f=open('nbar_i.txt','w')
print >>f, normlize[0]/norm_all
print >>f, normlize[1]/norm_all
print >>f, normlize[2]/norm_all
print >>f, normlize[3]/norm_all
print >>f, normlize[4]/norm_all
print >>f, normlize[5]/norm_all
print >>f, normlize[6]/norm_all
print >>f, normlize[7]/norm_all
print >>f, normlize[8]/norm_all
print >>f, normlize[9]/norm_all
print >>f, normlize[10]/norm_all
print >>f, normlize[11]/norm_all
print >>f, normlize[12]/norm_all
print >>f, normlize[13]/norm_all
print >>f, normlize[14]/norm_all
print >>f, normlize[15]/norm_all
print >>f, normlize[16]/norm_all
print >>f, normlize[17]/norm_all
print >>f, normlize[18]/norm_all
print >>f, normlize[19]/norm_all
f.close()

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt
from matplotlib.pyplot import cm
from matplotlib.pylab import legend

fig=plt.figure()
colors=cm.rainbow(np.linspace(0,1,Nbin))
for i,c in zip(range(Nbin),colors):
	plt.plot(NZ_SAMPLE['Z_MID'],nz[i]/norm_all,color=c,label=str(i+1))
plt.plot(NZ_SAMPLE['Z_MID'][NZ_SAMPLE['Z_MID']<=zmax],n_all(NZ_SAMPLE['Z_MID'][NZ_SAMPLE['Z_MID']<=zmax])/norm_all,color='k',label='all')

plt.xlabel('z')
plt.ylabel('n(z)')
legend(loc='upper right',ncol=2)
plt.savefig('nz.png')


print '~~~~~~~ Finished calculating n_i(z) ~~~~~~~~~~~'
