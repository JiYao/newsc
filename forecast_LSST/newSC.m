% new SC method, need to run FisherMatrixSIS_tomo.m to read data

Aij=zeros(Nbin,Nbin,length(slt)); % Aij is A^Ig_ij
GI=zeros(Nbin,Nbin,length(slt));

for i=1:length(slt)
  tmp=Ig./gg;
  Aij(:,:,i)=diag(diag(tmp(:,:,i)));
  GI(:,:,i)=IG(:,:,i)';
end;

for ll=1:length(slt)
  for i=1:Nbin
    for j=i+1:Nbin
      Aij(i,j,ll)=-sqrt(Aij(i,i,ll)*Aij(j,j,ll));
      % Aij(i,j,ll)=mean([Aij(i,i,ll) Aij(j,j,ll)]);
      Aij(j,i,ll)=Aij(i,j,ll);
    end;
  end;
end;

eff_II=Aij.^2.*gg./II;
eff_GI=Aij.*Gg./GI;

%% effeciency of II
figure;
eff1=reshape(eff_II(3,4,:),[1 27]);
eff2=reshape(eff_II(13,15,:),[1 27]);
eff3=reshape(eff_II(1,20,:),[1 27]);
plot(l,eff1,'LineWidth',3); lgdII{1}=['( i, j ) = ( 3, 4 )'];
hold on;
plot(l,eff2,'LineWidth',3); lgdII{2}=['( i, j ) = ( 13, 15 )'];
plot(l,eff3,'LineWidth',3); lgdII{3}=['( i, j ) = ( 1, 20 )'];
hold off;
axis([20 5000 0.95 1.15]);
set(gca,'xscale','log','Fontsize',30);
set(gcf,'Position',[0 0 1000 800]);
xlabel('$\ell$','Interpreter','LaTex','Fontsize',30);
ylabel(['efficiency of C^{II}_{ij}'],'Fontsize',30);
legend(lgdII,'Fontsize',30);

%% calculate efficiency of SC08
eff_IG=IG_SC./IG
eff08_26=reshape(eff_IG(2,6,:),[1 27]);
eff08_35=reshape(eff_IG(3,5,:),[1 27]);
%% effeciency of GI
figure;
eff1=reshape(eff_GI(4,4,:),[1 27]);
eff2=reshape(eff_GI(3,5,:),[1 27]);
eff3=reshape(eff_GI(5,3,:),[1 27]);
eff4=reshape(eff_GI(6,2,:),[1 27]);
plot(l,eff1,'LineWidth',3); lgdGI{1}=['SC17 ( i, j ) = ( 4, 4 )'];
hold on;
plot(l,eff2,'LineWidth',3); lgdGI{2}=['SC17 ( i, j ) = ( 3, 5 )'];
plot(l,eff3,'LineWidth',3); lgdGI{3}=['SC17 ( i, j ) = ( 5, 3 )'];
plot(l,eff4,'LineWidth',3); lgdGI{4}=['SC17 ( i, j ) = ( 6, 2 )'];
plot(l,eff08_35,'--','LineWidth',3); lgdGI{5}=['SC08 ( i, j ) = ( 5, 3 )'];
plot(l,eff08_26,'--','LineWidth',3); lgdGI{6}=['SC08 ( i, j ) = ( 6, 2 )'];
hold off;
axis([20 5000 0.96 1.01]);
set(gca,'xscale','log','Fontsize',30);
set(gcf,'Position',[0 0 1000 800]);
xlabel('$\ell$','Interpreter','LaTex','Fontsize',30);
ylabel(['efficiency of C^{GI}_{ij}'],'Fontsize',30);
legend(lgdGI,'Fontsize',22);

%% error analysis II

D_gg = zeros(Nbin,Nbin,Nl);
for ll=1:Nl
  D_gg(:,:,ll) = sqrt(2/(2*l(ll)+1)/0.2/l(ll)) * ( gg(:,:,ll) + diag(ggN) );
end;

DC_Ig_ii = zeros(Nbin,Nl);
for i=1:Nbin
  DC_Ig_ii(i,:) = sqrt( (1/2./(2*l+1)./(0.2*l))' .* ( reshape(gg(i,i,:).*GG(i,i,:),[1 Nl]) + (1+1./(3*(1-Q(i,:)).^2)) .* (reshape(gg(i,i,:),[1 Nl]).*GGN(i)+ggN(i).*reshape(GG(i,i,:)+II(i,i,:),[1 Nl])) + ggN(i)*GGN(i)*(1+1./(1-Q(i)).^2) ) );
end;

DII_II2 = zeros(Nbin,Nbin,Nl); % ( DeltaII_ij / II_ij )^2
DII_GG = zeros(Nbin,Nbin,Nl); % DeltaII_ij / GG_ij
DGG_GG = zeros(Nbin,Nbin,Nl); % DeltaGG_ij / GG_ij
for ll=1:Nl
  for i=1:Nbin
    for j=1:Nbin
      DII_II2(i,j,ll) = (DC_Ig_ii(i,ll)/Ig(i,i,ll))^2 + (D_gg(i,i,ll)/gg(i,i,ll))^2 + (DC_Ig_ii(j,ll)/Ig(j,j,ll))^2 + (D_gg(j,j,ll)/gg(j,j,ll))^2 + (D_gg(i,j,ll)/gg(i,j,ll))^2 ;
      DII_GG(i,j,ll) = sqrt(DII_II2(i,j,ll)) * II(i,j,ll) / GG(i,j,ll);
      DGG_GG(i,j,ll) = sqrt( ( (GG(i,j,ll)+(i==j)*GGN(i)).^2+(GG(i,i,ll)+GGN(i)).*(GG(j,j,ll)+GGN(j)) ) ./ (2*l(ll)*0.2*l(ll)*GG(i,j,ll).^2) );
    end;
  end;
end;

['SC II introduced error max = ' num2str(max(max(max( DII_GG./DGG_GG ))))]
['SC II introduced error mean = ' num2str(mean(mean(mean( DII_GG./DGG_GG ))))]

%% error analysis GI

DC_GgIg_ij = zeros(Nbin,Nbin,Nl);
for ll=1:Nl
  DC_GgIg_ij(:,:,ll) = sqrt(2/(2*l(ll)+1)/0.2/l(ll)) * abs( Gg(:,:,ll) + Ig(:,:,ll) );
end;

DC_Gg_ij2 = zeros(Nbin,Nbin,Nl);
DGI_GI = zeros(Nbin,Nbin,Nl);
DGI_GG = zeros(Nbin,Nbin,Nl);
for ll=1:Nl
  for i=1:Nbin
    for j=1:Nbin
      DC_Gg_ij2(i,j,ll) = DC_GgIg_ij(i,j,ll)^2 + 1/2 * ( (DC_Ig_ii(i,ll)/Ig(i,i,ll))^2 + (D_gg(i,i,ll)/gg(i,i,ll))^2 + (DC_Ig_ii(j,ll)/Ig(j,j,ll))^2 + (D_gg(j,j,ll)/gg(j,j,ll))^2 ) * (Aij(i,j,ll)*gg(i,j,ll))^2;
      DGI_GI(i,j,ll) = 1/2 * ( (DC_Ig_ii(i,ll)/Ig(i,i,ll))^2 + (D_gg(i,i,ll)/gg(i,i,ll))^2 + (DC_Ig_ii(j,ll)/Ig(j,j,ll))^2 + (D_gg(j,j,ll)/gg(j,j,ll))^2 ) + DC_Gg_ij2(i,j,ll)/Gg(i,j,ll)^2;
      DGI_GG(i,j,ll) = DGI_GI(i,j,ll) * IG(j,i,ll) / GG(i,j,ll);
    end;
  end;
end;

['SC GI introduced error max = ' num2str(min(min(min( DGI_GG./DGG_GG ))))]
['SC GI introduced error mean = ' num2str(mean(mean(mean( DGI_GG./DGG_GG ))))]

%% fast forecast

%% Error plot

figure;
example{1}=[4 4];
example{2}=[4 5];
example{3}=[5 4];
example{4}=[3 6];

for qual=1:4
  t=example{qual};
  i=t(1);
  j=t(2);
  subplot('Position',[(1-mod(qual,2))*0.5+0.07,(qual>2)*0.5+0.07,0.4,0.4]);

  hold on;

  errorbar(l,reshape(my_II(i,j,:),[1 27]),reshape(DII_GG(i,j,:).*GG(i,j,:),[1 27]),'o');
  lgdstr=['C^{II}_{',num2str(i),',',num2str(j),'}'];
  lgderr{1}=lgdstr;

  errorbar(l, abs(reshape(my_GI(i,j,:),[1 27])) , abs( reshape(DGI_GG(i,j,:).*GG(i,j,:),[1 27]) ) ,'x');
  lgdstr=['|C^{GI}_{',num2str(i),',',num2str(j),'}|'];
  lgderr{2}=lgdstr;

  errorbar(l,reshape(GG(i,j,:),[1 27]), reshape(DGG_GG(i,j,:).*GG(i,j,:),[1 27]) ,'s');
  lgdstr=['C^{GG}_{',num2str(i),',',num2str(j),'}'];
  lgderr{3}=lgdstr;

  Ymin=min( [abs(reshape(my_GI(i,j,:),[1 27])) reshape(my_II(i,j,:),[1 27])] ) /2;
  Ymax=max(reshape(GG(i,j,:),[1 27]))*2;
  Tmin=ceil(log10(Ymin));
  Tmax=floor(log10(Ymax));

  hold off;
  set(gca,'xscale','log','yscale','log','Fontsize',30);
  axis( [ 20 5000 Ymin Ymax] );
  yticks([ 10^Tmin 10^Tmax]);
  legend(lgderr);
end;
set(gcf,'Position',[0 0 1200 1000]);

print('errorplot','-dpng');


%% plot Q(z)
figure;
hold on;
plot( min( Q(1:Nbin-1,:)' ) );
lgdQ{1}=['min(Q)'];
plot( max( Q(1:Nbin-1,:)' ) );
lgdQ{2}=['max(Q)'];
hold off;
xlabel(['bin number i'],'FontSize',20);
ylabel(['Q_i'],'FontSize',20);
legend(lgdQ,'FontSize',20,'Location','northwest');
set(gca,'Fontsize',20);
print('Qz','-dpng');
