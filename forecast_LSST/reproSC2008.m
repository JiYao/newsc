% run after FisherMatrixSIS_tomo

i1=3
j1=4
i2=1
j2=7
figure;
Font=20;

subplot(2,2,1);
epsilon1=(IG(i1,j1,:)-IG_SC(i1,j1,:))./IG_SC(i1,j1,:);
epsilon1=reshape(epsilon1,[1 27]);
epsilon2=(IG(i2,j2,:)-IG_SC(i2,j2,:))./IG_SC(i2,j2,:);
epsilon2=reshape(epsilon2,[1 27]);
plot(l,epsilon1);
hold on;
plot(l,epsilon2);
hold off;
xlabel('$\ell$','Interpreter','LaTex','FontSize',Font);
set(gca,'xscale','log','FontSize',Font);
ylabel('$\epsilon_{ij}$','Interpreter','LaTex','FontSize',Font);
xlim(gca,[20 5000]);

subplot(2,2,2);
plot([l(1) l(27)],[Wij(i1,j1) Wij(i1,j1)]); lgd{1}=['i=',num2str(i1),', j=',num2str(j1)];
hold on;
plot([l(1) l(27)],[Wij(i2,j2) Wij(i2,j2)]); lgd{2}=['i=',num2str(i2),', j=',num2str(j2)];
hold off;
xlabel('$\ell$','Interpreter','LaTex','FontSize',Font);
set(gca,'xscale','log','FontSize',Font);
ylabel('$W_{ij}$','Interpreter','LaTex','FontSize',Font);
ylim(gca,[0.5e-5 5.5e-5]);
xlim(gca,[20 5000]);
legend(lgd,'FontSize',Font);

subplot(2,2,3);
plot([l(1) l(27)],[Delta_i(i1) Delta_i(i1)]);
hold on;
plot([l(1) l(27)],[Delta_i(i2) Delta_i(i2)]);
hold off;
xlabel('$\ell$','Interpreter','LaTex','FontSize',Font);
set(gca,'xscale','log','FontSize',Font);
ylabel('$\Delta_i$','Interpreter','LaTex','FontSize',Font);
ylim(gca,[950 1200]);
xlim(gca,[20 5000]);

subplot(2,2,4);
plot(l,Q(i1,:));
hold on;
plot(l,Q(i2,:));
hold off;
xlabel('$\ell$','Interpreter','LaTex','FontSize',Font);
set(gca,'xscale','log','FontSize',Font);
ylabel('$Q_i$','Interpreter','LaTex','FontSize',Font);
xlim(gca,[20 5000]);
