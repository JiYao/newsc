% i=num_para-1;
% j=num_para;
i=1;
j=2;
% IA1=-1.5;
% IA2=1.2;
IA1=p_model(i);
IA2=p_model(j);
% IA3=0.22;
% IA4=0.85;
range=4.5;

figure;
set(gcf,'Position',[0,0,1000,1000]);
% p1=linspace( (p_SC(indx(i))+IA1)/2-range*sqrt(Cov_SC(indx(i),indx(i))), (p_SC(indx(i))+IA1)/2+range*sqrt(Cov_SC(indx(i),indx(i))), 500 );
% p2=linspace( (p_SC(indx(j))+IA2)/2-range*sqrt(Cov_SC(indx(j),indx(j))), (p_SC(indx(j))+IA2)/2+range*sqrt(Cov_SC(indx(j),indx(j))), 500 );
p1=linspace( p_SC(indx(i))-0.04-range*sqrt(Cov_SC(indx(i),indx(i))), p_SC(indx(i))-0.04+range*sqrt(Cov_SC(indx(i),indx(i))), 500 );
p2=linspace( p_SC(indx(j))+0.04-range*sqrt(Cov_SC(indx(j),indx(j))), p_SC(indx(j))+0.01+range*sqrt(Cov_SC(indx(j),indx(j))), 500 );
[X,Y]=meshgrid(p1,p2);

rho=Cov_nosys(indx(i),indx(j))/sqrt(Cov_nosys(indx(i),indx(i)))/sqrt(Cov_nosys(indx(j),indx(j)));
chi2=( (X-p(indx(i))).^2./Cov_nosys(indx(i),indx(i)) + (Y-p(indx(j))).^2./Cov_nosys(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/sqrt(Cov_nosys(indx(i),indx(i))).*(Y-p(indx(j)))/sqrt(Cov_nosys(indx(j),indx(j))) )/(1-rho^2);
contour(X,Y,chi2,[2.3,2.3],'k-','LineWidth',2);
lgd{1}=['no sys'];

hold on;

rho=Cov_nosys(indx(i),indx(j))/sqrt(Cov_nosys(indx(i),indx(i)))/sqrt(Cov_nosys(indx(j),indx(j)));
chi2=( (X-IA1).^2./Cov_nosys(indx(i),indx(i)) + (Y-IA2).^2./Cov_nosys(indx(j),indx(j)) -2*rho*(X-IA1)/sqrt(Cov_nosys(indx(i),indx(i))).*(Y-IA2)/sqrt(Cov_nosys(indx(j),indx(j))) )/(1-rho^2);
contour(X,Y,chi2,[2.3,2.3],'r-','LineWidth',2);
lgd{2}=['Full IA contamination'];

% rho=Cov_nosys(indx(i),indx(j))/sqrt(Cov_nosys(indx(i),indx(i)))/sqrt(Cov_nosys(indx(j),indx(j)));
% chi2=( (X-IA3).^2./Cov_nosys(indx(i),indx(i)) + (Y-IA4).^2./Cov_nosys(indx(j),indx(j)) -2*rho*(X-IA3)/sqrt(Cov_nosys(indx(i),indx(i))).*(Y-IA4)/sqrt(Cov_nosys(indx(j),indx(j))) )/(1-rho^2);
% contour(X,Y,chi2,[2.3,2.3],'r--','LineWidth',2);
% lgd{3}=['Full IA contamination, estimated'];

rho=Cov_SC(indx(i),indx(j))/sqrt(Cov_SC(indx(i),indx(i)))/sqrt(Cov_SC(indx(j),indx(j)));
chi2=( (X-p_SC(indx(i))).^2./Cov_SC(indx(i),indx(i)) + (Y-p_SC(indx(j))).^2./Cov_SC(indx(j),indx(j)) -2*rho*(X-p_SC(indx(i)))/sqrt(Cov_SC(indx(i),indx(i))).*(Y-p_SC(indx(j)))/sqrt(Cov_SC(indx(j),indx(j))) )/(1-rho^2);
contour(X,Y,chi2,[2.3,2.3],'b--','LineWidth',2);
lgd{3}=['With SC, marg photo-z'];

quiver(p(indx(i))+0.005,p(indx(j))+0.005,IA1-p(indx(i)),IA2-p(indx(j)),0, 'MaxHeadSize',0.2,'LineWidth',2,'Color','Red');
lgd{4}=['Shift of full IA contamination'];
quiver(IA1-0.005,IA2-0.005,p_SC(indx(i))-IA1,p_SC(indx(j))-IA2,0, 'MaxHeadSize',0.2,'LineWidth',2,'Color','Blue');
lgd{5}=['Shift back by applying SC'];


plot( p(indx(i)), p(indx(j)) ,'kx','LineWidth',2);
plot( p_SC(indx(i)), p_SC(indx(j)) ,'bx','LineWidth',2);
plot( IA1, IA2 ,'rx','LineWidth',2);
% plot( IA3, IA4 ,'rx','LineWidth',2);

hold off;

legend(lgd,'FontSize',30);
xlabel(names{indx(i)},'FontSize',20);
ylabel(names{indx(j)},'FontSize',20);
set(gca,'fontsize',20);
