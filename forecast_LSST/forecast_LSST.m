clc;clear;%close all;

% take C^(1)=GG spectrum, and C^GG=C^(1)-C^IG. IG is anti-correlation, and
% recall we normally ignore the - sign in spectrum calculation. Thus the
% actual C^GG we want is calculated by GG+IG spectrum

%% load fiducial spectrums

p_all=[0.315 0.211995/0.315 0.829 0.9603 0 0.049   -1.0 0   0 0.025   1.0 0];
% parameters: Omega_m Omega_m*h sigma8 xn alpha_s Omega_b w_0 w_a Delta_z sigma_z A0 Power_z

trick = 1; % fraction of residual IG

prior_pz1=0.005;
prior_pz2=0.006;

partial=0.0015;
dalphas=0.1;
dw0=0.01;
dwa=0.01;
domegab=0.01;

dA=0.02;
dPower_z=0.01;

dDz=0.005;
dsigz=0.005;

dp=partial*p_all;
dp(7)=dw0;
dp(8)=dwa;
dp(9)=dDz;
dp(10)=dsigz;
dp(11)=dA;
dp(12)=dPower_z;

% LSST
fsky=0.436;
gamrms=0.26;
ng=26;

% % WFIRST
% fsky=2200/41253;
% gamrms=0.26;
% ng=45;

% % Euclid
% fsky=15000/41253;
% gamrms=0.26;
% ng=20;

filename=fullfile('Spectrums','fid','shear_cl','ell.txt');
l=dlmread(filename,' ',1,0);
slt=find(20<=l & l<=5000);
l=l(slt);
Nl=length(l);

load('nbar_i.txt');
GGN=gamrms^2./(ng*(60*180/pi)^2 *nbar_i);% *4*pi*fsky);
ggN=1./(ng*(60*180/pi)^2 *nbar_i);% *4*pi*fsky);

Nbin=20;
Npara=8;
NIApara=2;
F_nosys=zeros(Npara,Npara);
F_SC=zeros(Npara,Npara);
F_approx=zeros(Npara,Npara);
F_model=zeros(Npara,Npara);
F_marg=zeros(Npara+NIApara,Npara+NIApara);
F_sys=zeros(Npara,Npara);

F_prior=zeros(Npara,Npara);
F_prior(7,7)=1/prior_pz1^2;
F_prior(8,8)=1/prior_pz2^2;
F_prior_marg=zeros(Npara+NIApara,Npara+NIApara);
F_prior_marg(7,7)=1/prior_pz1^2;
F_prior_marg(8,8)=1/prior_pz2^2;

A_SC=zeros(Npara,1); % source of shift in cosmology
A_model=zeros(Npara,1);
A_approx=zeros(Npara,1);

F_new=zeros(Npara,Npara);
A_new=zeros(Npara,1);

filename=fullfile('Spectrums','fid','SCterms','Delta_i.txt');
Delta_i=dlmread(filename,' ',1,0);

filename=fullfile('Spectrums','fid','SCterms','Wij.txt');
Wij=dlmread(filename,' ',1,0);

for alpha=1:Npara % 6 cosmo para
	for beta=1:Npara

		alpha1=alpha;
		beta1=beta;
		if alpha>4 alpha1=alpha+2; end; % Dark Energy parameter
		if beta>4 beta1=beta+2; end;
		%if alpha>Npara alpha1=alpha+4; end;
		%if beta> Npara beta1=beta+4; end;

		C_nosys=zeros(Nbin,Nbin,Nl); % Nbin photo-z bins
		C_SC=zeros(Nbin,Nbin,Nl);
		C_marg=zeros(Nbin,Nbin,Nl);

		dPda_nosys=zeros(Nbin,Nbin,Nl);
		dPdb_nosys=zeros(Nbin,Nbin,Nl);

		dPda_SC=zeros(Nbin,Nbin,Nl);
		dPdb_SC=zeros(Nbin,Nbin,Nl);

		dPda_marg=zeros(Nbin,Nbin,Nl);
		dPdb_marg=zeros(Nbin,Nbin,Nl);

		gg=zeros(Nbin,Nbin,Nl);
		GG=zeros(Nbin,Nbin,Nl);
		IG=zeros(Nbin,Nbin,Nl);
		II=zeros(Nbin,Nbin,Nl);
		Ig=zeros(Nbin,Nbin,Nl);
		Gg=zeros(Nbin,Nbin,Nl);
		% Wij=zeros(Nbin,Nbin,Nl);
		% Delta_inv=zeros(Nbin,Nl);
		D_C_IG_ij=zeros(Nbin,Nbin,Nl);
		IG_SC=zeros(Nbin,Nbin,Nl);
		residual=zeros(Nbin,Nbin,Nl);

		Aij=zeros(Nbin,Nbin,length(slt)); % Aij is A^Ig_ij
		GI=zeros(Nbin,Nbin,length(slt));

		for i=1:Nbin

			t=['bin_',int2str(i),'_',int2str(i),'.txt'];
			% filename=fullfile('Spectrums','fid','galaxy_cl',t);
			% P=dlmread(filename,' ',1,0);
			% gg(i,:)=P(slt);

			% filename=fullfile('Spectrums','fid','shear_cl_ii',t);
			% P=dlmread(filename,' ',1,0);
			% II(i,:)=P(slt);

			% filename=fullfile('SCterms',int2str((i+2)*100+i+2),'C_Ig.dat');
			% P=load(filename);
			% P=flipud(P(:,4));
			% Delta_inv(i,:)=P(slt);

			t=['Q',int2str(i),'.dat'];
			filename=fullfile('Qi',t);
			P=load(filename);
			P=flipud(P(:,2));
			Q(i,:)=P(slt);

			t=['bin_',int2str(i),'.txt'];
			filename=fullfile('Spectrums','fid','nz_sample',t);
			nz_i=dlmread(filename,' ',1,0);
			nz_i=nz_i(2:length(nz_i)-1);

			filename=fullfile('Spectrums','fid','bias_field','b_g.txt');
			b_g=dlmread(filename,' ',1,0);
			b_g=b_g(1,:);

			b(i)=b_g*nz_i(1:length(b_g))*0.05; % 0.05 is step length in the n(z) histogram
			%b(i)=1;

			for j=i:Nbin

				t=['bin_',int2str(j),'_',int2str(i),'.txt'];
				filename=fullfile('Spectrums','fid','shear_cl',t);
				P=dlmread(filename,' ',1,0);
				GG(i,j,:)=P(slt);
				GG(j,i,:)=P(slt);

				filename=fullfile('Spectrums','fid','shear_cl_gi',t);
				P=dlmread(filename,' ',1,0);
				IG(i,j,:)=P(slt);
				% IG(j,i,:)=P(slt);

				filename=fullfile('Spectrums','fid','shear_cl_ii',t);
				P=dlmread(filename,' ',1,0);
				II(i,j,:)=P(slt);
				II(j,i,:)=P(slt);

				filename=fullfile('Spectrums','fid','galaxy_intrinsic_cl',t);
				P=dlmread(filename,' ',1,0);
				Ig(i,j,:)=P(slt);
				Ig(j,i,:)=P(slt);

				filename=fullfile('Spectrums','fid','galaxy_shear_cl',t);
				P=dlmread(filename,' ',1,0);
				Gg(i,j,:)=P(slt);
				% Gg(j,i,:)=P(slt);

				filename=fullfile('Spectrums','fid','galaxy_cl',t);
				P=dlmread(filename,' ',1,0);
				gg(i,j,:)=P(slt);
				gg(j,i,:)=P(slt);

				t=['bin_',int2str(i),'_',int2str(j),'.txt'];
				filename=fullfile('Spectrums','fid','shear_cl_gi',t);
				P=dlmread(filename,' ',1,0);
				IG(j,i,:)=P(slt);

				filename=fullfile('Spectrums','fid','galaxy_shear_cl',t);
				P=dlmread(filename,' ',1,0);
				Gg(j,i,:)=P(slt);

				% filename=fullfile('SCterms',int2str((i+2)*100+j+2),'C_IG.dat');
				% P=load(filename);
				% P=flipud(P(:,3));
				% Wij(i,j,:)=P(slt);
				% Wij(j,i,:)=P(slt);
			end;
		end;

		for i=1:length(slt)
		  tmp=Ig./gg;
		  Aij(:,:,i)=diag(diag(tmp(:,:,i)));
		  GI(:,:,i)=IG(:,:,i)';
		end;

		for ll=1:length(slt)
		  for i=1:Nbin
		    for j=i+1:Nbin
		      Aij(i,j,ll)=-sqrt(Aij(i,i,ll)*Aij(j,j,ll));
		      % Aij(i,j,ll)=mean([Aij(i,i,ll) Aij(j,j,ll)]);
		      Aij(j,i,ll)=Aij(i,j,ll);
		    end;
		  end;
		end;
		my_II=Aij.^2.*gg;
		my_GI=Aij.*Gg;

		%% select only auto- and adjacent- bins
		for ll=1:length(slt)
			for i=1:Nbin
				for j=i+1:Nbin
					if j>i+1
						my_II(i,j,ll)=0;
						my_II(j,i,ll)=0;
						my_GI(i,j,ll)=0;
						my_GI(j,i,ll)=0;
					end;
				end;
			end;
		end;

		for i=1:Nbin
			for j=i:Nbin
				% ------------------ no systematics ------------------
				% ---- C matrix -------

				%C_nosys(i,j,:)=sqrt( (GG(i,i,:)+GGN(i)).*(GG(j,j,:)+GGN(j)) + (GG(i,j,:)+(i==j).*GGN(i)).^2 );
				C_nosys(i,j,:)=sqrt( 2./ (2*reshape(l,[1 1 Nl])*0.2.*reshape(l,[1 1 Nl])*fsky) .* (GG(i,j,:)+(i==j).*GGN(i)).^2 );
				C_nosys(j,i,:)=C_nosys(i,j,:);

				% ---- dP/dp_alpha -----
				t=['bin_',int2str(j),'_',int2str(i),'.txt'];
				t1=[int2str(alpha1),'p'];
				filename=fullfile('Spectrums',t1,'shear_cl',t);
				GGp=dlmread(filename,' ',1,0);

				t1=[int2str(alpha1),'m'];
				filename=fullfile('Spectrums',t1,'shear_cl',t);
				GGm=dlmread(filename,' ',1,0);

				dPda_nosys(i,j,:)=(GGp(slt)-GGm(slt))./(2*dp(alpha1));
				dPda_nosys(j,i,:)=dPda_nosys(i,j,:);

				% ----- dP/dp_beta ----
				t1=[int2str(beta1),'p'];
				filename=fullfile('Spectrums',t1,'shear_cl',t);
				GGp=dlmread(filename,' ',1,0);

				t1=[int2str(beta1),'m'];
				filename=fullfile('Spectrums',t1,'shear_cl',t);
				GGm=dlmread(filename,' ',1,0);

				dPdb_nosys(i,j,:)=(GGp(slt)-GGm(slt))./(2*dp(beta1));
				dPdb_nosys(j,i,:)=dPdb_nosys(i,j,:);

			end;
		end;

		NN=Nbin^2;
		CC_nosys=zeros(NN,NN,Nl);

		for s1=1:Nbin
		for s2=1:Nbin
		for s3=1:Nbin
		for s4=1:Nbin
			CC_nosys((s1-1)*Nbin+s2,(s3-1)*Nbin+s4,:) = (GG(s1,s3,:)+(s1==s3)*GGN(s1)).*(GG(s2,s4,:)+(s2==s4)*GGN(s2)) + (GG(s1,s4,:)+(s1==s4).*GGN(s1)).*(GG(s2,s3,:)+(s2==s3)*GGN(s2));
		end;
		end;
		end;
		end;

		% ---------------- Self Calibration ---------------------

		for i=1:Nbin
			for j=i:Nbin

				% ----- C matrix ------
				D0 = sqrt( reshape(( GG(i,j,:).^2+(GG(i,i,:)+GGN(i)).*(GG(j,j,:)+GGN(j)) ),[1,Nl]) ./ (2*l'*0.2.*l'*fsky) );
				D1 = Wij(i,j) * Delta_i(i) / b(i) .* sqrt( 1./((2*l'+1)*0.2.*l'*fsky) .* ( reshape(gg(i,i,:).*GG(i,i,:),[1,Nl]) + (1+1./(3*(1-Q(i,:)).^2)).*(reshape(gg(i,i,:),[1 Nl])*GGN(i)+ggN(i)*(reshape(GG(i,i,:)+II(i,i,:),[1,Nl]))) + ggN(i)*GGN(i)*(1+1./(1-Q(i,:)).^2 ) ) );
				D2 = Wij(i,j) * Delta_i(i) / b(i) .* reshape(Ig(i,i,:),[1,Nl]) .* (1/2 * sqrt(2./((2*l'+1)*0.2.*l'*fsky)) .* (1+ggN(i)./reshape(gg(i,i,:),[1 Nl])) );

				D_C_IG_ij(i,j,:)=sqrt( D1.^2 + D2.^2 );
				% D_C_IG_ij(j,i,:)=(1-(i==j))* D_C_IG_ij(i,j,:);
				D_C_IG_ij(j,i,:) = D_C_IG_ij(i,j,:);

				IG_SC(i,j,:)=Wij(i,j)*Delta_i(i)/b(i).*reshape(Ig(i,i,:),[1,Nl]);
				%IG_SC(i,j,:)=IG(i,j,:); % test: clean all IG
				% IG_SC(j,i,:)=(1-(i==j))* IG_SC(i,j,:);
				% IG_SC(j,i,:) = IG_SC(i,j,:);
				IG_SC(i,j,:)=Wij(i,j)*Delta_i(i)/b(i).*reshape(Ig(i,i,:),[1,Nl]);
				%IG_SC(j,i,:)=(1-(i==j))*IG_SC(i,j,:) - (i==j)*II(i,i,:);
				% IG_SC(j,i,:)=(1-(i==j))*IG_SC(i,j,:) + (i==j)*IG(i,j,:);

			end;
		end;

		residual=IG-IG_SC;
		%error_SC=(D_C_IG_ij.^2+residual.^2).^0.5;
		error_SC=D_C_IG_ij;

		IG_comb=IG_SC;
		for ll=1:Nl
			for i=1:Nbin
				for j=1:Nbin
					if j<=i+1
						IG_comb(i,j,ll)=my_GI(j,i,ll);
					end;
				end;
			end;
		end;

		NN=Nbin^2;
		FF1=zeros(NN,NN,Nl);
		FF2=zeros(NN,NN,Nl);
		CC_SC=zeros(NN,NN,Nl);
		CC_approx=CC_SC;
		test=CC_SC;
		C_SC=zeros(Nbin,Nbin,Nl);
		cutauto=[];
		for ll=1:Nl
			Cinv_nosys=inv(C_nosys(:,:,ll));
			Cinv_error=inv(error_SC(:,:,ll));
			for s1=1:Nbin
			for s2=1:Nbin
			for s3=1:Nbin
			for s4=1:Nbin
				FF1((s1-1)*Nbin+s2,(s3-1)*Nbin+s4,ll) = Cinv_nosys(s1,s4)*Cinv_nosys(s2,s3);
				FF2((s1-1)*Nbin+s2,(s3-1)*Nbin+s4,ll) = Cinv_error(s1,s4)*Cinv_error(s2,s3);

				% CC1 = 1/(2*l(ll)+1)/0.2/l(ll)/fsky * ( (GG(s1,s3,ll) + (s1==s3)*GGN(s1)) * (GG(s2,s4,ll) + (s2==s4)*GGN(s2)) + (GG(s1,s4,ll) + (s1==s4)*GGN(s1)) * (GG(s2,s3,ll) + (s2==s3)*GGN(s2)) );
				%
				% CC2_1 = 1/(2*l(ll)+1)/0.2/l(ll)/fsky * ( IG(s3,s1,ll) * (GG(s2,s3,ll)+(s2==s3)*GGN(s2)) + IG(s3,s2,ll) * (GG(s1,s3,ll)+(s1==s3)*GGN(s1)) );
				% % CC2_2 = -1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s3,s4)*Delta_i(s3)/b(s3) * ( (s1~=s3)*Gg(s1,s3,ll) * (GG(s2,s3,ll)+IG(s3,s2,ll)+(s2==s3)*GGN(s2)) + (s2~=s3)*Gg(s2,s3,ll) * (GG(s1,s3,ll)+IG(s1,s3,ll)+(s1==s3)*GGN(s1)) );
				% CC2_2 = -1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s3,s4)*Delta_i(s3)/b(s3) * ( Gg(s1,s3,ll) * (GG(s2,s3,ll)+IG(s3,s2,ll)+(s2==s3)*GGN(s2)) + Gg(s2,s3,ll) * (GG(s1,s3,ll)+IG(s1,s3,ll)+(s1==s3)*GGN(s1)) );
				%
				% CC3_1 = 1/(2*l(ll)+1)/0.2/l(ll)/fsky * ( IG(s1,s3,ll) * (GG(s4,s1,ll)+(s4==s1)*GGN(s4)) + IG(s1,s4,ll) * (GG(s3,s1,ll)+(s3==s1)*GGN(s3)) );
				% % CC3_2 = -1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s1,s2)*Delta_i(s1)/b(s1) * ( (s3~=s1)*Gg(s3,s1,ll) * (GG(s4,s1,ll)+IG(s4,s1,ll)+(s4==s1)*GGN(s4)) + (s4~=s1)*Gg(s4,s1,ll) * (GG(s3,s1,ll)+IG(s3,s1,ll)+(s3==s1)*GGN(s3)) );
				% CC3_2 = -1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s1,s2)*Delta_i(s1)/b(s1) * ( Gg(s3,s1,ll) * (GG(s4,s1,ll)+IG(s4,s1,ll)+(s4==s1)*GGN(s4)) + Gg(s4,s1,ll) * (GG(s3,s1,ll)+IG(s3,s1,ll)+(s3==s1)*GGN(s3)) );
				%
				% CC4_1 = 1/(2*l(ll)+1)/0.2/l(ll)/fsky * ( II(s1,s3,ll) * (GG(s2,s4,ll)+(s2==s4)*GGN(s2)) + IG(s1,s4,ll) * IG(s2,s3,ll) );
				% % CC4_2 = -1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s3,s4)*Delta_i(s3)/b(s3) * ( Ig(s1,s3,ll) * (GG(s2,s3,ll)+IG(s3,s2,ll)+(s2==s3)*GGN(s2)) + (s2~=s3)*Gg(s2,s3,ll) * (IG(s1,s3,ll)+II(s1,s3,ll)) );
				% CC4_2 = -1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s3,s4)*Delta_i(s3)/b(s3) * ( Ig(s1,s3,ll) * (GG(s2,s3,ll)+IG(s3,s2,ll)+(s2==s3)*GGN(s2)) + Gg(s2,s3,ll) * (IG(s1,s3,ll)+II(s1,s3,ll)) );
				% % CC4_3 = -1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s1,s2)*Delta_i(s1)/b(s1) * ( Ig(s3,s1,ll) * (GG(s4,s1,ll)+IG(s1,s4,ll)+(s1==s4)*GGN(s1)) + (s4~=s1)*Gg(s4,s1,ll) * (IG(s3,s1,ll)+II(s1,s3,ll)) );
				% CC4_3 = -1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s1,s2)*Delta_i(s1)/b(s1) * ( Ig(s3,s1,ll) * (GG(s4,s1,ll)+IG(s1,s4,ll)+(s1==s4)*GGN(s1)) + Gg(s4,s1,ll) * (IG(s3,s1,ll)+II(s1,s3,ll)) );
				% % CC4_4 = 1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s1,s2)*Delta_i(s1)/b(s1) * Wij(s3,s4)*Delta_i(s3)/b(s3) * ( gg(s1,s3,ll)*(GG(s1,s3,ll)+IG(s1,s3,ll)+IG(s3,s1,ll)+II(s1,s3,ll)) + ( gg(s1,s3,ll)*(s1==s3)*GGN(s1) + (s1==s3)*ggN(s1)*(GG(s1,s3,ll)+IG(s1,s3,ll)+IG(s3,s1,ll)+II(s1,s3,ll)) )*(1+1/3/(1-Q(s1,ll))^2) + (s1==s3)*ggN(s1)*GGN(s1)*(1+1/(1-Q(s1,ll))^2) + ( (s3~=s1)*Gg(s3,s1,ll)+Ig(s3,s1,ll) ) * ( (s1~=s3)*Gg(s1,s3,ll)+Ig(s1,s3,ll) ) );
				% CC4_4 = 1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s1,s2)*Delta_i(s1)/b(s1) * Wij(s3,s4)*Delta_i(s3)/b(s3) * ( gg(s1,s3,ll)*(GG(s1,s3,ll)+IG(s1,s3,ll)+IG(s3,s1,ll)+II(s1,s3,ll)) + ( gg(s1,s3,ll)*(s1==s3)*GGN(s1) + (s1==s3)*ggN(s1)*(GG(s1,s3,ll)+IG(s1,s3,ll)+IG(s3,s1,ll)+II(s1,s3,ll)) )*(1+1/3/(1-Q(s1,ll))^2) + (s1==s3)*ggN(s1)*GGN(s1)*(1+1/(1-Q(s1,ll))^2) + ( Gg(s3,s1,ll)+Ig(s3,s1,ll) ) * ( Gg(s1,s3,ll)+Ig(s1,s3,ll) ) );

				% CC_SC((s1-1)*Nbin+s2,(s3-1)*Nbin+s4,ll) = CC1+CC2_1+CC2_2+CC3_1+CC3_2+CC4_1+CC4_2+CC4_3+CC4_4;
				% [CC2_1+CC2_2+CC3_1+CC3_2+CC4_1+CC4_2+CC4_3 CC4_4]/CC1
				% CC_SC((s1-1)*Nbin+s2,(s3-1)*Nbin+s4,ll) = CC1+CC4_4;

				% if abs(CC2_1+CC2_2+CC3_1+CC3_2+CC4_1+CC4_2+CC4_3)>CC4_4 & abs(CC2_1+CC2_2+CC3_1+CC3_2+CC4_1+CC4_2+CC4_3)>0.1*CC1
				% 	[CC2_1+CC2_2+CC3_1+CC3_2+CC4_1+CC4_2+CC4_3 CC2_1 CC2_2 CC3_1 CC3_2 CC4_1 CC4_2 CC4_3 CC4_4]/CC1
				% end

				CC1 = 1/(2*l(ll)+1)/0.2/l(ll)/fsky * ( (GG(s1,s3,ll)+IG(s1,s3,ll)+IG(s3,s1,ll)+II(s1,s3,ll)+(s1==s3)*GGN(s1)) * (GG(s2,s4,ll)+IG(s2,s4,ll)+IG(s4,s2,ll)+II(s2,s4,ll)+(s2==s4)*GGN(s2)) + (GG(s1,s4,ll)+IG(s1,s4,ll)+IG(s4,s1,ll)+II(s1,s4,ll)+(s1==s4)*GGN(s1)) * (GG(s2,s3,ll)+IG(s2,s3,ll)+IG(s3,s2,ll)+II(s2,s3,ll)+(s2==s3)*GGN(s2)) );
				CC2 = -1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s3,s4)*Delta_i(s3)/b(s3) * ( (Gg(s1,s3,ll)+Ig(s1,s3,ll))*(GG(s2,s3,ll)+IG(s2,s3,ll)+IG(s3,s2,ll)+II(s2,s3,ll)+(s2==s3)*GGN(s2)) + (Gg(s2,s3,ll)+Ig(s2,s3,ll))*(GG(s1,s3,ll)+IG(s1,s3,ll)+IG(s3,s1,ll)+II(s1,s3,ll)+(s1==s3)*GGN(s1) ));
				CC3 = -1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s1,s2)*Delta_i(s1)/b(s1) * ( (Gg(s3,s1,ll)+Ig(s3,s1,ll))*(GG(s4,s1,ll)+IG(s4,s1,ll)+IG(s1,s4,ll)+II(s4,s1,ll)+(s4==s1)*GGN(s4)) + (Gg(s4,s1,ll)+Ig(s4,s1,ll))*(GG(s3,s1,ll)+IG(s1,s3,ll)+IG(s3,s1,ll)+II(s3,s1,ll)+(s1==s3)*GGN(s1) ));
				CC4 = 1/(2*l(ll)+1)/0.2/l(ll)/fsky * Wij(s1,s2)*Delta_i(s1)/b(s1) * Wij(s3,s4)*Delta_i(s3)/b(s3) * ( gg(s1,s3,ll)*(GG(s1,s3,ll)+IG(s1,s3,ll)+IG(s3,s1,ll)+II(s1,s3,ll)) + ( gg(s1,s3,ll)*(s1==s3)*GGN(s1) + (s1==s3)*ggN(s1)*(GG(s1,s3,ll)+IG(s1,s3,ll)+IG(s3,s1,ll)+II(s1,s3,ll)) )*(1+1/3/(1-Q(s1,ll))^2) + (s1==s3)*ggN(s1)*GGN(s1)*(1+1/(1-Q(s1,ll))^2) + ( Gg(s3,s1,ll)+Ig(s3,s1,ll) ) * ( Gg(s1,s3,ll)+Ig(s1,s3,ll) ) );
				CC_SC((s1-1)*Nbin+s2,(s3-1)*Nbin+s4,ll) = CC1+CC2+CC3+CC4;
				% CC_approx((s1-1)*Nbin+s2,(s3-1)*Nbin+s4,ll) = CC1;
				CC_approx((s1-1)*Nbin+s2,(s3-1)*Nbin+s4,ll) = 1/(2*l(ll)+1)/0.2/l(ll)/fsky * ( (GG(s1,s3,ll)+(s1==s3)*GGN(s1)) * (GG(s2,s4,ll)+(s2==s4)*GGN(s2)) + (GG(s1,s4,ll)+(s1==s4)*GGN(s1)) * (GG(s2,s3,ll)+(s2==s3)*GGN(s2)) );
				% if abs(CC2+CC3)>CC4 & abs(CC2+CC3)>0.5*CC1
				% 	[CC2+CC3 CC2 CC3 CC4]/CC1
				% end
				% if ll==1 & s1==1 & s2==2 & s3==3 & s4==4
				% 	[s1 s2 s3 s4]
				% 	[CC1 -CC2 -CC3 CC4]
				% end
				% if ll==1 & s1==3 & s2==4 & s3==1 & s4==2
				% 	[s1 s2 s3 s4]
				% 	[CC1 -CC2 -CC3 CC4]
				% end

			end;
			end;
			if (ll==1 & s1~=s2)
				cutauto=[cutauto (s1-1)*Nbin+s2];
			end;
			end;
			end;

			% Weikang's appriximation method
			% CC_SC(:,:,ll)=inv(FF1(:,:,ll)) + inv(FF2(:,:,ll));
			test(:,:,ll)=CC_SC(:,:,ll) ./ ( inv(FF2(:,:,ll)) + inv(FF1(:,:,ll)) );
		end;

		% cutauto=1:49;

		for i=1:Nbin
			for j=i:Nbin
				% ---------------- Marginalization -------------------
				% -------- C matrix --------
				t=['bin_',int2str(j),'_',int2str(i),'.txt'];
				%filename=fullfile('Spectrums','fid','shear_cl',t);
				%GG=dlmread(filename,' ',1,0);
				%filename=fullfile('Spectrums','fid','shear_cl_gi',t);
				%IG=dlmread(filename,' ',1,0);
				%filename=fullfile('Spectrums','fid','shear_cl_ii',t);
				%II=dlmread(filename,' ',1,0);
				C_marg(i,j,:)=GG(i,j,:)+IG(i,j,:)+IG(j,i,:)+(i==j)*GGN(i)+II(i,j,slt);
				%if i==j
				%	C_marg(i,j,:)=GG(slt)+IG(slt)+GGN(i);%+II(slt)+GGN(i);
				%end;
				C_marg(j,i,:)=C_marg(i,j,:);

				% ---- dP/dp_alpha -----
				t1=[int2str(alpha1),'p'];
				filename=fullfile('Spectrums',t1,'shear_cl',t);
				GGp=dlmread(filename,' ',1,0);
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				IGp=dlmread(filename,' ',1,0);

				filename=fullfile('Spectrums',t1,'shear_cl_ii',t);
				IIp=dlmread(filename,' ',1,0);
				t=['bin_',int2str(i),'_',int2str(j),'.txt'];
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				GIp=dlmread(filename,' ',1,0);
				t=['bin_',int2str(j),'_',int2str(i),'.txt'];

				t1=[int2str(alpha1),'m'];
				filename=fullfile('Spectrums',t1,'shear_cl',t);
				GGm=dlmread(filename,' ',1,0);
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				IGm=dlmread(filename,' ',1,0);

				filename=fullfile('Spectrums',t1,'shear_cl_ii',t);
				IIm=dlmread(filename,' ',1,0);
				t=['bin_',int2str(i),'_',int2str(j),'.txt'];
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				GIm=dlmread(filename,' ',1,0);
				t=['bin_',int2str(j),'_',int2str(i),'.txt'];

				dPda_marg(i,j,:)=(GGp(slt)+IGp(slt)+GIp(slt)+IIp(slt)-GGm(slt)-IGm(slt)-GIm(slt)-IIm(slt))./(2*dp(alpha1));
				dPda_marg(j,i,:)=dPda_marg(i,j,:);

				% ----- dP/dp_beta ----
				t1=[int2str(beta1),'p'];
				filename=fullfile('Spectrums',t1,'shear_cl',t);
				GGp=dlmread(filename,' ',1,0);
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				IGp=dlmread(filename,' ',1,0);

				filename=fullfile('Spectrums',t1,'shear_cl_ii',t);
				IIp=dlmread(filename,' ',1,0);
				t=['bin_',int2str(i),'_',int2str(j),'.txt'];
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				GIp=dlmread(filename,' ',1,0);
				t=['bin_',int2str(j),'_',int2str(i),'.txt'];

				t1=[int2str(beta1),'m'];
				filename=fullfile('Spectrums',t1,'shear_cl',t);
				GGm=dlmread(filename,' ',1,0);
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				IGm=dlmread(filename,' ',1,0);

				filename=fullfile('Spectrums',t1,'shear_cl_ii',t);
				IIm=dlmread(filename,' ',1,0);
				t=['bin_',int2str(i),'_',int2str(j),'.txt'];
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				GIm=dlmread(filename,' ',1,0);
				t=['bin_',int2str(j),'_',int2str(i),'.txt'];

				dPdb_marg(i,j,:)=(GGp(slt)+IGp(slt)+GIp(slt)+IIp(slt)-GGm(slt)-IGm(slt)-GIm(slt)-IIm(slt))./(2*dp(beta1));
				dPdb_marg(j,i,:)=dPdb_marg(i,j,:);

			end;
		end;

		% cut off auto-spectra
		dPda_noauto=dPda_nosys;
		dPdb_noauto=dPdb_nosys;
		% for i=1:Nl
		% 	for j=1:Nbin
		% 		dPda_noauto(j,j,i)=0;
		% 		dPdb_noauto(j,j,i)=0;
		% 		%CC_nosys((j-1)*Nbin+j,(j-1)*Nbin+j,i)=0;
		% 	end;
		% end;

		for i=1:Nl

			tmp1 = reshape(dPda_noauto(:,:,i)',[1,NN]);
			tmp2 = reshape(dPdb_noauto(:,:,i)',[NN,1]);
			%F_nosys(alpha,beta)=F_nosys(alpha,beta) + trace( inv(C_nosys(:,:,i))*dPda_nosys(:,:,i)*inv(C_nosys(:,:,i))*dPdb_nosys(:,:,i) );
			F_nosys(alpha,beta)=F_nosys(alpha,beta) + (2*l(i)+1)*0.2*l(i)*fsky* reshape(dPda_nosys(:,:,i)',[1,NN]) * pinv(CC_nosys(:,:,i)) * reshape(dPdb_nosys(:,:,i)',[NN,1]);
			% F_nosys(alpha,beta)=F_nosys(alpha,beta) + (2*l(i)+1)*0.2*l(i)*fsky* tmp1(cutauto) * pinv(CC_nosys(cutauto,cutauto,i)) * tmp2(cutauto);
			%F_SC(alpha,beta)=F_SC(alpha,beta) + (2*l(i)+1)*0.2*l(i)*fsky*trace( inv(C_SC(:,:,i))*dPda_nosys(:,:,i)*inv(C_SC(:,:,i))*dPdb_nosys(:,:,i) );
			% F_SC(alpha,beta)=F_SC(alpha,beta) + reshape(dPda_nosys(:,:,i)',[1,NN]) * pinv(CC_SC(:,:,i)) * reshape(dPdb_nosys(:,:,i)',[NN,1]);
			F_SC(alpha,beta)=F_SC(alpha,beta) + tmp1(cutauto) * pinv(CC_SC(cutauto,cutauto,i)) * tmp2(cutauto);
			F_approx(alpha,beta)=F_approx(alpha,beta) + tmp1(cutauto) * pinv(CC_approx(cutauto,cutauto,i)) * tmp2(cutauto);
			F_model(alpha,beta)=F_model(alpha,beta) + tmp1 * pinv(CC_approx(:,:,i)) * tmp2;
			% F_SC(alpha,beta)=F_SC(alpha,beta) + tmp1 * pinv(CC_SC(:,:,i)) * tmp2;
			F_marg(alpha,beta)=F_marg(alpha,beta) + (2*l(i)+1)/2*0.2*l(i)*fsky*trace( inv(C_marg(:,:,i))*dPda_marg(:,:,i)*inv(C_marg(:,:,i))*dPdb_marg(:,:,i) );
			%F_marg(alpha,beta)=F_marg(alpha,beta) + (2*l(i)+1)/2*0.2*l(i)*fsky*trace( inv(C_nosys(:,:,i))*dPda_marg(:,:,i)*inv(C_nosys(:,:,i))*dPdb_marg(:,:,i) );
			%F_marg(alpha,beta)=F_marg(alpha,beta) + (2*l(i)+1)*0.2*l(i)*fsky* reshape(dPda_marg(:,:,i)',[1,NN]) * inv(CC_marg) * reshape(dPdb_marg(:,:,i)',[NN,1]);
			F_sys(alpha,beta)=F_sys(alpha,beta) + (l(i)+1/2)*0.2*l(i)*fsky*trace( inv(C_marg(:,:,i))*dPda_nosys(:,:,i)*inv(C_marg(:,:,i))*dPdb_nosys(:,:,i) );

			F_new(alpha,beta)=F_new(alpha,beta) + tmp1 * pinv(CC_approx(:,:,i)) * tmp2;

			% if alpha==2 & beta==2
			% 	['cutauto']
			% 	tmp1(cutauto) * pinv(CC_SC(cutauto,cutauto,i)) * tmp2(cutauto)
			% 	['full']
			% 	tmp1 * pinv(CC_approx(:,:,i)) * tmp2
			% end;

		end;
	end;

	for i=1:Nl
		% A_SC(alpha)=A_SC(alpha) + reshape( ( GG(:,:,i) - ( GG(:,:,i)+IG(:,:,i)-IG_SC(:,:,i) ) )' ,[1,NN]) * pinv(CC_SC(:,:,i)) * reshape(dPda_nosys(:,:,i)',[NN,1]);
		% tmp1 = reshape( (-IG(:,:,i)+IG_SC(:,:,i)-II(:,:,i)*0 )' ,[1,NN]);
		% tmp1 = reshape( (-(IG(:,:,i)+IG(:,:,i)'-diag(diag(IG(:,:,i))))+(IG_SC(:,:,i)+IG_SC(:,:,i)'-diag(diag(IG_SC(:,:,i))))-II(:,:,i)*0 )' ,[1,NN]);
		tmp1 = reshape( (-(IG(:,:,i)+IG(:,:,i)')+(IG_SC(:,:,i)+IG_SC(:,:,i)')*1-II(:,:,i)*1 )' ,[1,NN]);
		% tmp1 = reshape( (-IG(:,:,i)*1 )' ,[1,NN]); % test residual 20%
		tmp2 = reshape(dPda_noauto(:,:,i)',[NN,1]);
		A_SC(alpha)=A_SC(alpha) + tmp1(cutauto) * pinv(CC_SC(cutauto,cutauto,i)) * tmp2(cutauto);
		% A_SC(alpha)=A_SC(alpha) + tmp1(cutauto) * pinv(CC_approx(cutauto,cutauto,i)) * tmp2(cutauto);
		A_approx(alpha)=A_approx(alpha) + tmp1(cutauto) * pinv(CC_approx(cutauto,cutauto,i)) * tmp2(cutauto);
		% A_SC(alpha)=A_SC(alpha) + tmp1 * pinv(CC_SC(:,:,i)) * tmp2;
		% A_model(alpha) = A_model(alpha) + reshape( -IG(:,:,i)' ,[1,NN]) * pinv(CC_SC(:,:,i)) * reshape(dPda_nosys(:,:,i)',[NN,1]);

		tmp1 = reshape( (-(IG(:,:,i)+IG(:,:,i)')-II(:,:,i)*1 )' ,[1,NN]);
		A_model(alpha) = A_model(alpha) + tmp1 * pinv(CC_approx(:,:,i)) * tmp2;

		% tmp1 = reshape( (-IG(:,:,i)+IG_comb(:,:,i)-II(:,:,i)+my_II(:,:,i) )' ,[1,NN]);
		% tmp1 = reshape( (-(IG(:,:,i)+IG(:,:,i)'-diag(diag(IG(:,:,i))))+(IG_comb(:,:,i)+IG_comb(:,:,i)'-diag(diag(IG_comb(:,:,i))))-II(:,:,i)+my_II(:,:,i) )' ,[1,NN]);
		tmp1 = reshape( (-(IG(:,:,i)+IG(:,:,i)')+(IG_comb(:,:,i)+IG_comb(:,:,i)')-II(:,:,i)+my_II(:,:,i) )' ,[1,NN]);
		A_new(alpha)=A_new(alpha) + tmp1 * pinv(CC_approx(:,:,i)) * tmp2;
	end;

end;

for alpha=Npara+1:Npara+NIApara
	for beta=1:alpha %Npara+NIApara

		alpha1=alpha;
		beta1=beta;
		if alpha>4 alpha1=alpha+2; end; % Dark Energy parameter
		if beta>4 beta1=beta+2; end;
		%if alpha>Npara alpha1=alpha+4; end;
		%if beta> Npara beta1=beta+4; end;

		C_marg=zeros(Nbin,Nbin,Nl);
		dPda_marg=zeros(Nbin,Nbin,Nl);
		dPdb_marg=zeros(Nbin,Nbin,Nl);

		%GG=zeros(Nbin,Nbin,Nl);
		%IG=zeros(Nbin,Nbin,Nl);
		%for i=1:Nbin
		%	for j=i:Nbin
		%
		%		t=['bin_',int2str(j),'_',int2str(i),'.txt'];
		%		filename=fullfile('Spectrums','fid','shear_cl',t);
		%		P=dlmread(filename,' ',1,0);
		%		GG(i,j,:)=P(slt);
		%		GG(j,i,:)=P(slt);

		%		filename=fullfile('Spectrums','fid','shear_cl_gi',t);
		%		P=dlmread(filename,' ',1,0);
		%		IG(i,j,:)=P(slt);
		%		IG(j,i,:)=P(slt);

		%	end;
		%end;

		%for ll=1:Nl
		%for s1=1:Nbin
		%for s2=1:Nbin
		%for s3=s1:s1%1:Nbin
		%for s4=s2:s2%1:Nbin
		%	CC1=( GG(s1,s3,ll)+IG(s1,s3,ll)+(s1==s3)*GGN(s1) ).*( GG(s2,s4,ll)+IG(s2,s4,ll)+(s2==s4)*GGN(s2) ) + ( GG(s1,s4,ll)+IG(s1,s4,ll)+(s1==s4)*GGN(s1) ).*( GG(s2,s3,ll)+IG(s2,s3,ll)+(s2==s3)*GGN(s2) );
		%	C_marg(s1,s2,ll)=sqrt(CC1);
		%end;
		%end;
		%end;
		%end;
		%end;

		for i=1:Nbin
			for j=i:Nbin
				% ---------------- Marginalization -------------------
				% -------- C matrix --------
				t=['bin_',int2str(j),'_',int2str(i),'.txt'];
				%filename=fullfile('Spectrums','fid','shear_cl',t);
				%GG=dlmread(filename,' ',1,0);
				%filename=fullfile('Spectrums','fid','shear_cl_gi',t);
				%IG=dlmread(filename,' ',1,0);
				%filename=fullfile('Spectrums','fid','shear_cl_ii',t);
				%II=dlmread(filename,' ',1,0);
					C_marg(i,j,:)=GG(i,j,:)+IG(i,j,:)+IG(j,i,:)+(i==j)*GGN(i)+II(i,j,slt);
				%if i==j
				%	C_marg(i,j,:)=GG(slt)+IG(slt)+GGN(i);
				%end;
				C_marg(j,i,:)=C_marg(i,j,:);

				% ---- dP/dp_alpha -----
				t1=[int2str(alpha1),'p'];
				filename=fullfile('Spectrums',t1,'shear_cl',t);
				GGp=dlmread(filename,' ',1,0);
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				IGp=dlmread(filename,' ',1,0);

				filename=fullfile('Spectrums',t1,'shear_cl_ii',t);
				IIp=dlmread(filename,' ',1,0);
				t=['bin_',int2str(i),'_',int2str(j),'.txt'];
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				GIp=dlmread(filename,' ',1,0);
				t=['bin_',int2str(j),'_',int2str(i),'.txt'];

				t1=[int2str(alpha1),'m'];
				filename=fullfile('Spectrums',t1,'shear_cl',t);
				GGm=dlmread(filename,' ',1,0);
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				IGm=dlmread(filename,' ',1,0);

				filename=fullfile('Spectrums',t1,'shear_cl_ii',t);
				IIm=dlmread(filename,' ',1,0);
				t=['bin_',int2str(i),'_',int2str(j),'.txt'];
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				GIm=dlmread(filename,' ',1,0);
				t=['bin_',int2str(j),'_',int2str(i),'.txt'];

				dPda_marg(i,j,:)=(GGp(slt)+IGp(slt)+GIp(slt)+IIp(slt)-GGm(slt)-IGm(slt)-GIm(slt)-IIm(slt))./(2*dp(alpha1));
				dPda_marg(j,i,:)=dPda_marg(i,j,:);

				% ----- dP/dp_beta ----
				t1=[int2str(beta1),'p'];
				filename=fullfile('Spectrums',t1,'shear_cl',t);
				GGp=dlmread(filename,' ',1,0);
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				IGp=dlmread(filename,' ',1,0);

				filename=fullfile('Spectrums',t1,'shear_cl_ii',t);
				IIp=dlmread(filename,' ',1,0);
				t=['bin_',int2str(i),'_',int2str(j),'.txt'];
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				GIp=dlmread(filename,' ',1,0);
				t=['bin_',int2str(j),'_',int2str(i),'.txt'];

				t1=[int2str(beta1),'m'];
				filename=fullfile('Spectrums',t1,'shear_cl',t);
				GGm=dlmread(filename,' ',1,0);
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				IGm=dlmread(filename,' ',1,0);

				filename=fullfile('Spectrums',t1,'shear_cl_ii',t);
				IIm=dlmread(filename,' ',1,0);
				t=['bin_',int2str(i),'_',int2str(j),'.txt'];
				filename=fullfile('Spectrums',t1,'shear_cl_gi',t);
				GIm=dlmread(filename,' ',1,0);
				t=['bin_',int2str(j),'_',int2str(i),'.txt'];

				dPdb_marg(i,j,:)=(GGp(slt)+IGp(slt)+GIp(slt)+IIp(slt)-GGm(slt)-IGm(slt)-GIm(slt)-IIm(slt))./(2*dp(beta1));
				dPdb_marg(j,i,:)=dPdb_marg(i,j,:);
			end;
		end;
		for i=1:Nl
			%CC_marg=zeros(NN,NN);
			%for s1=1:Nbin
			%for s2=1:Nbin
			%for s3=1:Nbin
			%for s4=1:Nbin
			%	CC_marg((s1-1)*Nbin+s2,(s3-1)*Nbin+s4)=C_marg(s1,s3)*C_marg(s2,s4)+C_marg(s1,s4)*C_marg(s2,s3);
			%end;
			%end;
			%end;
			%end;
			F_marg(alpha,beta)=F_marg(alpha,beta) + (2*l(i)+1)/2*0.2*l(i)*fsky*trace( inv(C_marg(:,:,i))*dPda_marg(:,:,i)*inv(C_marg(:,:,i))*dPdb_marg(:,:,i) );
			%F_marg(alpha,beta)=F_marg(alpha,beta) + (2*l(i)+1)/2*0.2*l(i)*fsky*trace( inv(C_nosys(:,:,i))*dPda_marg(:,:,i)*inv(C_nosys(:,:,i))*dPdb_marg(:,:,i) );
			%F_marg(alpha,beta)=F_marg(alpha,beta) + (2*l(i)+1)*0.2*l(i)*fsky* reshape(dPda_marg(:,:,i)',[1,NN]) * inv(CC_marg) * reshape(dPdb_marg(:,:,i)',[NN,1]);
			F_marg(beta,alpha)=F_marg(alpha,beta);
		end;
	end;

	%{
	for i=1:Nl
		A_model(alpha)=A_model(alpha) + (2*l(i)+1)*0.2*l(i)*fsky*trace( inv(C_nosys(:,:,i))*((-IG(:,:,i)-IG(:,:,i)'-II(:,:,i))*trick)*inv(C_nosys(:,:,i))*dPda_nosys(:,:,i) );
	end;
	%}
end;

F_nosys=F_nosys([1:Npara-2],[1:Npara-2]);
F_SC=F_SC([1:Npara],[1:Npara]);
% F_SC=F_approx([1:Npara],[1:Npara]);
F_sys=F_marg([1:Npara],[1:Npara]);
%F_marg=F_marg([1:6,9:10],[1:6,9:10]);

%% contour with fiducial

IP=[1:4,7:10];
p=p_all(IP);
indx=[1 3 2 5 6];%[1 3 2 5 6]; % plot index
names={'\Omega_m','\Omega_m*h','\sigma_8','n_s','w_0','w_a','\Delta_z','\sigma_z'};
num_para=length(indx);
range=3; % +/- 4 sigma
figure;
ps=300; % plot-sizefigure;
set(gcf,'Position',[0,0,ps*num_para,ps*num_para]);

%{
p(2)=p(2)/p(1); % \Omega_m *h --> h
M=eye(length(IP));
M(1,2)=0; M(2,1)=p(2); M(2,2)=p(1);
F_nosys=M'*F_nosys*M;
F_SC=M'*F_SC*M;
F_sys=M'*F_sys*M;
M=eye(length(IP)+NIApara);
M(1,2)=0; M(2,1)=p(2); M(2,2)=p(1);
F_marg=M'*F_marg*M;
%}
names{2}='h_0';

A_SC=A_SC(1:Npara);
% A_model=A_model(1:Npara-2);

% DP_SC=-inv(F_nosys)*A_SC;
DP_SC=-inv(F_SC)*A_SC;
p_SC=p;%+DP_SC';
DP_model=-inv(F_model)*A_model;
p_model=p;%+DP_model';
DP_new=-inv(F_new)*A_new;
DP_approx=-inv(F_approx)*A_approx;

Cov_nosys=inv(F_nosys);
Cov_SC=inv(F_SC);
% Cov_SC=inv(F_SC(1:6,1:6));
Cov_marg=inv(F_marg);
% Cov_marg=inv(F_marg([1:8],[1:8]));
%Cov_nomarg=inv(F_marg([1:6],[1:6]));
%Cov_marg_pz=inv(F_marg([1:8],[1:8]));
%Cov_marg_IA=inv(F_marg([1:6,9:10],[1:6,9:10]));
Cov_approx=inv(F_approx);
Cov_new=inv(F_new);

Cov_SC=inv(F_SC+F_prior);
Cov_marg=inv(F_marg+F_prior_marg);
Cov_approx=inv(F_approx+F_prior);
Cov_new=inv(F_new+F_prior);

for i=1:num_para-1
	for j=i+1:num_para

		subplot('Position',[1/num_para*(i-0.5),1/num_para*(num_para+0.5-j),1/num_para,1/num_para]);
		p1=linspace( p(indx(i))-range*sqrt(Cov_nosys(indx(i),indx(i))), p(indx(i))+range*sqrt(Cov_nosys(indx(i),indx(i))), 500 );
		p2=linspace( p(indx(j))-range*sqrt(Cov_nosys(indx(j),indx(j))), p(indx(j))+range*sqrt(Cov_nosys(indx(j),indx(j))), 500 );
		[X,Y]=meshgrid(p1,p2);

		rho=Cov_nosys(indx(i),indx(j))/sqrt(Cov_nosys(indx(i),indx(i)))/sqrt(Cov_nosys(indx(j),indx(j)));
		chi2=( (X-p(indx(i))).^2./Cov_nosys(indx(i),indx(i)) + (Y-p(indx(j))).^2./Cov_nosys(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/sqrt(Cov_nosys(indx(i),indx(i))).*(Y-p(indx(j)))/sqrt(Cov_nosys(indx(j),indx(j))) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'k-','LineWidth',2);
		lgd{1}=['no sys'];

		hold on;
		rho=Cov_SC(indx(i),indx(j))/sqrt(Cov_SC(indx(i),indx(i)))/sqrt(Cov_SC(indx(j),indx(j)));
		chi2=( (X-p_SC(indx(i))).^2./Cov_SC(indx(i),indx(i)) + (Y-p_SC(indx(j))).^2./Cov_SC(indx(j),indx(j)) -2*rho*(X-p_SC(indx(i)))/sqrt(Cov_SC(indx(i),indx(i))).*(Y-p_SC(indx(j)))/sqrt(Cov_SC(indx(j),indx(j))) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'b--','LineWidth',2);
		lgd{2}=['With SC, marg photo-z'];

		rho=Cov_marg(indx(i),indx(j))/sqrt(Cov_marg(indx(i),indx(i)))/sqrt(Cov_marg(indx(j),indx(j)));
		chi2=( (X-p(indx(i))).^2./Cov_marg(indx(i),indx(i)) + (Y-p(indx(j))).^2./Cov_marg(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/sqrt(Cov_marg(indx(i),indx(i))).*(Y-p(indx(j)))/sqrt(Cov_marg(indx(j),indx(j))) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'r--','LineWidth',2);
		lgd{3}=['IA model, marg photo-z & IA'];

		% %{
		% rho=Cov_nosys(indx(i),indx(j))/sqrt(Cov_nosys(indx(i),indx(i)))/sqrt(Cov_nosys(indx(j),indx(j)));
		% chi2=( (X-p_model(indx(i))).^2./Cov_nosys(indx(i),indx(i)) + (Y-p_model(indx(j))).^2./Cov_nosys(indx(j),indx(j)) -2*rho*(X-p_model(indx(i)))/sqrt(Cov_nosys(indx(i),indx(i))).*(Y-p_model(indx(j)))/sqrt(Cov_nosys(indx(j),indx(j))) )/(1-rho^2);
		% contour(X,Y,chi2,[2.3,2.3],'g--');
		% lgd{4}=['impact of IG'];
		%
		% rho=Cov_nomarg(indx(i),indx(j))/sqrt(Cov_nomarg(indx(i),indx(i)))/sqrt(Cov_nomarg(indx(j),indx(j)));
		% chi2=( (X-p(indx(i))).^2./Cov_nomarg(indx(i),indx(i)) + (Y-p(indx(j))).^2./Cov_nomarg(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/sqrt(Cov_nomarg(indx(i),indx(i))).*(Y-p(indx(j)))/sqrt(Cov_nomarg(indx(j),indx(j))) )/(1-rho^2);
		% contour(X,Y,chi2,[2.3,2.3],'k');
		% lgd{4}=['no marg: C=C^{GG}+C^{IG}+N'];
		%
		% rho=Cov_marg_pz(indx(i),indx(j))/sqrt(Cov_marg_pz(indx(i),indx(i)))/sqrt(Cov_marg_pz(indx(j),indx(j)));
		% chi2=( (X-p_model(indx(i))).^2./Cov_n(indx(i),indx(i)) + (Y-p_model(indx(j))).^2./Cov_marg_pz(indx(j),indx(j)) -2*rho*(X-p_model(indx(i)))/sqrt(Cov_marg_pz(indx(i),indx(i))).*(Y-p_model(indx(j)))/sqrt(Cov_marg_pz(indx(j),indx(j))) )/(1-rho^2);
		% contour(X,Y,chi2,[2.3,2.3],'g--');
		% lgd{4}=['marg photo-z: C=C^{GG}+C^{IG}+N'];
		%
		% rho=Cov_marg_IA(indx(i),indx(j))/sqrt(Cov_marg_IA(indx(i),indx(i)))/sqrt(Cov_marg_IA(indx(j),indx(j)));
		% chi2=( (X-p(indx(i))).^2./Cov_marg_IA(indx(i),indx(i)) + (Y-p(indx(j))).^2./Cov_marg_IA(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/sqrt(Cov_marg_IA(indx(i),indx(i))).*(Y-p(indx(j)))/sqrt(Cov_marg_IA(indx(j),indx(j))) )/(1-rho^2);
		% contour(X,Y,chi2,[2.3,2.3],'r');
		% lgd{6}=['marg IA: C=C^{GG}+C^{IG}+N'];
		% %}

		plot( p(indx(i)), p(indx(j)) ,'kx','LineWidth',2);

		hold off;

		if j==num_para xlabel(names{indx(i)},'FontSize',20); end;
		if i==1 ylabel(names{indx(j)},'FontSize',20); end;
	end;
end;

legend(lgd,'Position',[0.7,0.7,0.01,0.01],'FontSize',30);
saveas(gca,'contour','png');

%% --------------plot contours with the shift -------------------

IP=[1:4,7:10];
p=p_all(IP);

p_SC=p+DP_SC';
p_model=p+DP_model';
p_new=p+DP_new';
p_approx=p+DP_approx';

range=2.2; % +/- 4 sigma
figure;
ps=300; % plot-sizefigure;
set(gcf,'Position',[0,0,ps*num_para,ps*num_para]);

lgd(:)=[];

for i=1:num_para-1
	for j=i+1:num_para

		subplot('Position',[1/num_para*(i-0.5),1/num_para*(num_para+0.5-j),1/num_para,1/num_para]);
		p1=linspace( p_SC(indx(i))-DP_SC(indx(i))/2-range*sqrt(Cov_SC(indx(i),indx(i))), p_SC(indx(i))+range*sqrt(Cov_SC(indx(i),indx(i))), 500 );
		p2=linspace( p_SC(indx(j))-DP_SC(indx(j))/2-range*sqrt(Cov_SC(indx(j),indx(j))), p_SC(indx(j))+range*sqrt(Cov_SC(indx(j),indx(j))), 500 );
		[X,Y]=meshgrid(p1,p2);

		rho=Cov_nosys(indx(i),indx(j))/sqrt(Cov_nosys(indx(i),indx(i)))/sqrt(Cov_nosys(indx(j),indx(j)));
		chi2=( (X-p(indx(i))).^2./Cov_nosys(indx(i),indx(i)) + (Y-p(indx(j))).^2./Cov_nosys(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/sqrt(Cov_nosys(indx(i),indx(i))).*(Y-p(indx(j)))/sqrt(Cov_nosys(indx(j),indx(j))) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'k-','LineWidth',2);
		lgd{1}=['no sys'];

		hold on;
		rho=Cov_SC(indx(i),indx(j))/sqrt(Cov_SC(indx(i),indx(i)))/sqrt(Cov_SC(indx(j),indx(j)));
		chi2=( (X-p_SC(indx(i))).^2./Cov_SC(indx(i),indx(i)) + (Y-p_SC(indx(j))).^2./Cov_SC(indx(j),indx(j)) -2*rho*(X-p_SC(indx(i)))/sqrt(Cov_SC(indx(i),indx(i))).*(Y-p_SC(indx(j)))/sqrt(Cov_SC(indx(j),indx(j))) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'b--','LineWidth',2);
		lgd{2}=['With SC, marg photo-z'];

		% rho=Cov_approx(indx(i),indx(j))/sqrt(Cov_approx(indx(i),indx(i)))/sqrt(Cov_approx(indx(j),indx(j)));
		% chi2=( (X-p_approx(indx(i))).^2./Cov_approx(indx(i),indx(i)) + (Y-p_approx(indx(j))).^2./Cov_approx(indx(j),indx(j)) -2*rho*(X-p_approx(indx(i)))/sqrt(Cov_approx(indx(i),indx(i))).*(Y-p_approx(indx(j)))/sqrt(Cov_approx(indx(j),indx(j))) )/(1-rho^2);
		% contour(X,Y,chi2,[2.3,2.3],'r--','LineWidth',2);
		% lgd{3}=['With SC, with approx, marg photo-z'];

		% rho=Cov_new(indx(i),indx(j))/sqrt(Cov_new(indx(i),indx(i)))/sqrt(Cov_new(indx(j),indx(j)));
		% chi2=( (X-p_new(indx(i))).^2./Cov_new(indx(i),indx(i)) + (Y-p_new(indx(j))).^2./Cov_new(indx(j),indx(j)) -2*rho*(X-p_new(indx(i)))/sqrt(Cov_new(indx(i),indx(i))).*(Y-p_new(indx(j)))/sqrt(Cov_new(indx(j),indx(j))) )/(1-rho^2);
		% contour(X,Y,chi2,[2.3,2.3],'g--','LineWidth',2);
		% lgd{3}=['With combined SC (08+17), marg photo-z'];

		% rho=Cov_marg(indx(i),indx(j))/sqrt(Cov_marg(indx(i),indx(i)))/sqrt(Cov_marg(indx(j),indx(j)));
		% chi2=( (X-p(indx(i))).^2./Cov_marg(indx(i),indx(i)) + (Y-p(indx(j))).^2./Cov_marg(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/sqrt(Cov_marg(indx(i),indx(i))).*(Y-p(indx(j)))/sqrt(Cov_marg(indx(j),indx(j))) )/(1-rho^2);
		% contour(X,Y,chi2,[2.3,2.3],'r--','LineWidth',2);
		% lgd{3}=['IA model, marg photo-z & IA'];

    % rho=Cov_nosys(indx(i),indx(j))/sqrt(Cov_nosys(indx(i),indx(i)))/sqrt(Cov_nosys(indx(j),indx(j)));
		% chi2=( (X-p_model(indx(i))).^2./Cov_nosys(indx(i),indx(i)) + (Y-p_model(indx(j))).^2./Cov_nosys(indx(j),indx(j)) -2*rho*(X-p_model(indx(i)))/sqrt(Cov_nosys(indx(i),indx(i))).*(Y-p_model(indx(j)))/sqrt(Cov_nosys(indx(j),indx(j))) )/(1-rho^2);
		% contour(X,Y,chi2,[2.3,2.3],'g--','LineWidth',2);
		% lgd{4}=['impact of IG'];

    % %{
		% rho=Cov_nomarg(indx(i),indx(j))/sqrt(Cov_nomarg(indx(i),indx(i)))/sqrt(Cov_nomarg(indx(j),indx(j)));
		% chi2=( (X-p(indx(i))).^2./Cov_nomarg(indx(i),indx(i)) + (Y-p(indx(j))).^2./Cov_nomarg(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/sqrt(Cov_nomarg(indx(i),indx(i))).*(Y-p(indx(j)))/sqrt(Cov_nomarg(indx(j),indx(j))) )/(1-rho^2);
		% contour(X,Y,chi2,[2.3,2.3],'k');
		% lgd{4}=['no marg: C=C^{GG}+C^{IG}+N'];
		%
		% rho=Cov_marg_pz(indx(i),indx(j))/sqrt(Cov_marg_pz(indx(i),indx(i)))/sqrt(Cov_marg_pz(indx(j),indx(j)));
		% chi2=( (X-p_model(indx(i))).^2./Cov_n(indx(i),indx(i)) + (Y-p_model(indx(j))).^2./Cov_marg_pz(indx(j),indx(j)) -2*rho*(X-p_model(indx(i)))/sqrt(Cov_marg_pz(indx(i),indx(i))).*(Y-p_model(indx(j)))/sqrt(Cov_marg_pz(indx(j),indx(j))) )/(1-rho^2);
		% contour(X,Y,chi2,[2.3,2.3],'g--');
		% lgd{4}=['marg photo-z: C=C^{GG}+C^{IG}+N'];
		%
		% rho=Cov_marg_IA(indx(i),indx(j))/sqrt(Cov_marg_IA(indx(i),indx(i)))/sqrt(Cov_marg_IA(indx(j),indx(j)));
		% chi2=( (X-p(indx(i))).^2./Cov_marg_IA(indx(i),indx(i)) + (Y-p(indx(j))).^2./Cov_marg_IA(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/sqrt(Cov_marg_IA(indx(i),indx(i))).*(Y-p(indx(j)))/sqrt(Cov_marg_IA(indx(j),indx(j))) )/(1-rho^2);
		% contour(X,Y,chi2,[2.3,2.3],'r');
		% lgd{6}=['marg IA: C=C^{GG}+C^{IG}+N'];
		% %}

		plot( p(indx(i)), p(indx(j)) ,'kx','LineWidth',2);
		plot( p_SC(indx(i)), p_SC(indx(j)) ,'bx','LineWidth',2);
		% plot( p_approx(indx(i)), p_approx(indx(j)) ,'rx','LineWidth',2);
		% plot( p_new(indx(i)), p_new(indx(j)) ,'gx','LineWidth',2);

		hold off;

		if j==num_para xlabel(names{indx(i)},'FontSize',20); end;
		if i==1 ylabel(names{indx(j)},'FontSize',20); end;
	end;
end;

legend(lgd,'Position',[0.7,0.7,0.01,0.01],'FontSize',30);
saveas(gca,'contour_shift','png');

%% --------------plot contours + shift with new SC -------------------

IP=[1:4,7:10];
p=p_all(IP);

p_SC=p+DP_SC';
p_model=p(1:8)+DP_model';
p_new=p+DP_new';
p_approx=p+DP_approx';

range=3; % +/- 4 sigma
figure;
ps=300; % plot-sizefigure;
set(gcf,'Position',[0,0,ps*num_para,ps*num_para]);

lgd(:)=[];

p_center=p_new;

for i=1:num_para-1
	for j=i+1:num_para

		subplot('Position',[1/num_para*(i-0.5),1/num_para*(num_para+0.5-j),1/num_para,1/num_para]);
		p1=linspace( p_center(indx(i))-range*sqrt(Cov_SC(indx(i),indx(i))), p_center(indx(i))+range*sqrt(Cov_SC(indx(i),indx(i))), 500 );
		p2=linspace( p_center(indx(j))-range*sqrt(Cov_SC(indx(j),indx(j))), p_center(indx(j))+range*sqrt(Cov_SC(indx(j),indx(j))), 500 );
		[X,Y]=meshgrid(p1,p2);

		rho=Cov_nosys(indx(i),indx(j))/sqrt(Cov_nosys(indx(i),indx(i)))/sqrt(Cov_nosys(indx(j),indx(j)));
		chi2=( (X-p(indx(i))).^2./Cov_nosys(indx(i),indx(i)) + (Y-p(indx(j))).^2./Cov_nosys(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/sqrt(Cov_nosys(indx(i),indx(i))).*(Y-p(indx(j)))/sqrt(Cov_nosys(indx(j),indx(j))) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'k-','LineWidth',2);
		lgd{1}=['no sys'];

		hold on;
		rho=Cov_approx(indx(i),indx(j))/sqrt(Cov_approx(indx(i),indx(i)))/sqrt(Cov_approx(indx(j),indx(j)));
		chi2=( (X-p_approx(indx(i))).^2./Cov_approx(indx(i),indx(i)) + (Y-p_approx(indx(j))).^2./Cov_approx(indx(j),indx(j)) -2*rho*(X-p_approx(indx(i)))/sqrt(Cov_approx(indx(i),indx(i))).*(Y-p_approx(indx(j)))/sqrt(Cov_approx(indx(j),indx(j))) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'b--','LineWidth',2);
		lgd{2}=['With SC08, no II, approx cov, marg photo-z'];

		rho=Cov_new(indx(i),indx(j))/sqrt(Cov_new(indx(i),indx(i)))/sqrt(Cov_new(indx(j),indx(j)));
		chi2=( (X-p_new(indx(i))).^2./Cov_new(indx(i),indx(i)) + (Y-p_new(indx(j))).^2./Cov_new(indx(j),indx(j)) -2*rho*(X-p_new(indx(i)))/sqrt(Cov_new(indx(i),indx(i))).*(Y-p_new(indx(j)))/sqrt(Cov_new(indx(j),indx(j))) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'r-.','LineWidth',2);
		lgd{3}=['With combined SC08 + SC17, approx cov, marg photo-z'];

		plot( p(indx(i)), p(indx(j)) ,'kx','LineWidth',2);
		plot( p_approx(indx(i)), p_approx(indx(j)) ,'bx','LineWidth',2);
		plot( p_new(indx(i)), p_new(indx(j)) ,'rx','LineWidth',2);

		hold off;

		if j==num_para xlabel(names{indx(i)},'FontSize',20); end;
		if i==1 ylabel(names{indx(j)},'FontSize',20); end;
	end;
end;

legend(lgd,'Position',[0.7,0.7,0.01,0.01],'FontSize',30);
saveas(gca,'shift_combined_SC','png');



%% ------------- plot contours of IA parameters -------------------
%{
figure;
ps=500; % plot-sizefigure;
set(gcf,'Position',[0,0,ps*5,ps*3]);
range=2;

names={'\Omega_m','h_0','\sigma_8','n_s','w_0','w_a','A_0','\eta'};
p=[0.315 0.211995/0.315 0.829 0.9603 -1.0 0 1.0 0];

for i=1:4
	for j=7:8

		subplot('Position',[1/5*(i-0.5),1/3*(j-6-0.5),1/5,1/3]);
		p1=linspace( p(i)-range*sqrt(Cov_marg(i,i)), p(i)+range*sqrt(Cov_marg(i,i)), ps );
		p2=linspace( p(j)-range*sqrt(Cov_marg(j,j)), p(j)+range*sqrt(Cov_marg(j,j)), ps );
		[X,Y]=meshgrid(p1,p2);

		rho=Cov_marg(i,j)/sqrt(Cov_marg(i,i))/sqrt(Cov_marg(j,j));
		chi2=( (X-p(i)).^2./Cov_marg(i,i) + (Y-p(j)).^2./Cov_marg(j,j) -2*rho*(X-p(i))/sqrt(Cov_marg(i,i)).*(Y-p(j))/sqrt(Cov_marg(j,j)) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'r--');
		IAlgd=['IA Marginalization: C=C^{GG}+C^{IG}+N'];

		hold off;

		axis([p(i)-4*sqrt(Cov_marg(i,i)), p(i)+4*sqrt(Cov_marg(i,i)), p(j)-4*sqrt(Cov_marg(j,j)), p(j)+4*sqrt(Cov_marg(j,j)) ]);

		if j==7 xlabel(names{i},'FontSize',20); end;
		if i==1 ylabel(names{j},'FontSize',20); end;

	end;
end;

legend(IAlgd);
saveas(gca,'IAcontour1','png');

figure;
ps=500; % plot-sizefigure;
set(gcf,'Position',[0,0,ps*4,ps*3]);
range=2;

for i=5:6
	for j=7:8

		subplot('Position',[1/4*(i-4-0.5),1/3*(j-6-0.5),1/4,1/3]);
		p1=linspace( p(i)-range*sqrt(Cov_marg(i,i)), p(i)+range*sqrt(Cov_marg(i,i)), ps );
		p2=linspace( p(j)-range*sqrt(Cov_marg(j,j)), p(j)+range*sqrt(Cov_marg(j,j)), ps );
		[X,Y]=meshgrid(p1,p2);

		rho=Cov_marg(i,j)/sqrt(Cov_marg(i,i))/sqrt(Cov_marg(j,j));
		chi2=( (X-p(i)).^2./Cov_marg(i,i) + (Y-p(j)).^2./Cov_marg(j,j) -2*rho*(X-p(i))/sqrt(Cov_marg(i,i)).*(Y-p(j))/sqrt(Cov_marg(j,j)) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'r--');
		IAlgd=['IA Marginalization: C=C^{GG}+C^{IG}+N'];

		hold off;

		axis([p(i)-4*sqrt(Cov_marg(i,i)), p(i)+4*sqrt(Cov_marg(i,i)), p(j)-4*sqrt(Cov_marg(j,j)), p(j)+4*sqrt(Cov_marg(j,j)) ]);

		if j==7 xlabel(names{i},'FontSize',20); end;
		if i==5 ylabel(names{j},'FontSize',20); end;

	end;
end;

i=7;j=8;
subplot('Position',[1/4*2.5,1/3*(j-6-0.5),1/4,1/3]);
p1=linspace( p(i)-range*sqrt(Cov_marg(i,i)), p(i)+range*sqrt(Cov_marg(i,i)), ps );
p2=linspace( p(j)-range*sqrt(Cov_marg(j,j)), p(j)+range*sqrt(Cov_marg(j,j)), ps );
[X,Y]=meshgrid(p1,p2);

rho=Cov_marg(i,j)/sqrt(Cov_marg(i,i))/sqrt(Cov_marg(j,j));
chi2=( (X-p(i)).^2./Cov_marg(i,i) + (Y-p(j)).^2./Cov_marg(j,j) -2*rho*(X-p(i))/sqrt(Cov_marg(i,i)).*(Y-p(j))/sqrt(Cov_marg(j,j)) )/(1-rho^2);
contour(X,Y,chi2,[2.3,2.3],'r--');
IAlgd=['IA Marginalization: C=C^{GG}+C^{IG}+N'];

hold off;

axis([p(i)-4*sqrt(Cov_marg(i,i)), p(i)+4*sqrt(Cov_marg(i,i)), p(j)-4*sqrt(Cov_marg(j,j)), p(j)+4*sqrt(Cov_marg(j,j)) ]);

xlabel(names{i},'FontSize',20);
if i==1 ylabel(names{j},'FontSize',20); end;


legend(IAlgd);
saveas(gca,'IAcontour2','png');
%}

%% --------------- plot SC parameters -----------------
%{
epsilon=IG./IG_SC-1;

i1=3; j1=4;
i2=1; j2=7;

figure;
subplot(2,2,1);
plot(l,reshape(epsilon(i1,j1,:),[1,Nl]),'LineWidth',2);
hold on;
plot(l,reshape(epsilon(i2,j2,:),[1,Nl]),'LineWidth',2);
hold off;
set(gca,'xscale','log','xtick',[10 10^2 10^3 10^4]);
axis([10 10^4 min(min(epsilon(i1,j1,:)),min(epsilon(i2,j2,:)))*0.9 max(max(epsilon(i1,j1,:)),max(epsilon(i2,j2,:)))*1.1]);
xlabel('$\ell$','Interpreter','LaTex','FontSize',15);
ylabel('$\epsilon_{ij}$','Interpreter','LaTex','FontSize',15);

subplot(2,2,2);
plot([min(l) max(l)],[Wij(i1,j1) Wij(i1,j1)],'LineWidth',2);
SClgd{1}=['i = ',int2str(i1),' , j = ',int2str(j1)];
hold on;
plot([min(l) max(l)],[Wij(i2,j2) Wij(i2,j2)],'LineWidth',2);
SClgd{2}=['i = ',int2str(i2),' , j = ',int2str(j2)];
hold off;
set(gca,'xscale','log','xtick',[10 10^2 10^3 10^4]);
axis([10 10^4 min(Wij(i1,j1),Wij(i2,j2))*0.9 max(Wij(i1,j1),Wij(i2,j2))*1.1]);
legend(SClgd,'Position',[0.5,0.5,0.01,0.01],'FontSize',15);
xlabel('$\ell$','Interpreter','LaTex','FontSize',15);
ylabel('$W_{ij}$','Interpreter','LaTex','FontSize',15);

subplot(2,2,3);
plot([min(l) max(l)],[Delta_i(i1) Delta_i(i1)],'LineWidth',2);
hold on;
plot([min(l) max(l)],[Delta_i(i2) Delta_i(i2)],'LineWidth',2);
hold off;
set(gca,'xscale','log','xtick',[10 10^2 10^3 10^4]);
axis([10 10^4 min(Delta_i(i1),Delta_i(i2))*0.9 max(Delta_i(i1),Delta_i(i2))*1.1]);
xlabel('$\ell$','Interpreter','LaTex','FontSize',15);
ylabel('$\Delta_i^{-1}$','Interpreter','LaTex','FontSize',15);

subplot(2,2,4);
plot(l,Q(i1,:),'LineWidth',2);
hold on;
plot(l,Q(i2,:),'LineWidth',2);
hold off;
set(gca,'xscale','log','xtick',[10 10^2 10^3 10^4]);
axis([10 10^4 min(min(Q(i1,:)),min(Q(i2,:)))*0.9 max(max(Q(i1,:)),max(Q(i2,:)))*1.1]);
xlabel('$\ell$','Interpreter','LaTex','FontSize',15);
ylabel('$Q_i$','Interpreter','LaTex','FontSize',15);

saveas(gca,'SCterms','png');
%}
