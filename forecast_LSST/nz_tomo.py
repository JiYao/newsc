import fitsio as fio
import numpy as np

import ConfigParser
config= ConfigParser.RawConfigParser()
config.read(r'photoz_para.ini')

sigma_z=float(config.get('photo-z parameters','sigma_z'))
Delta_z=float(config.get('photo-z parameters','Delta_z'))

NZ_SAMPLE=fio.FITS('modified_cfhtlens_heymans13.fits')['NZ_SAMPLE'].read()

zmin = 0.4

def pdf(zph,z):
	z0=0.3
	return 1./2./z0*(z/z0)**2*np.exp(-z/z0) * 1./np.sqrt(2.*np.pi)/sigma_z/(1+z)*np.exp(-(z-zph-Delta_z)**2/2./(sigma_z*(1+z))**2)

def n_all(z):
	z0=0.3
	return 1./2./z0*(z/z0)**2*np.exp(-z/z0)

Nbin=10

step_z=NZ_SAMPLE['Z_MID'][2]-NZ_SAMPLE['Z_MID'][1]
normlize=np.zeros(Nbin)
nz=np.zeros((Nbin,len(NZ_SAMPLE['Z_MID'])))

for i in range(len(NZ_SAMPLE['Z_MID'])):
	z=NZ_SAMPLE['Z_MID'][i]
	step_zph=0.01
	for zph in [zmin+0.01*j+0.005 for j in range(220)]:
		bin_num=(zph-zmin)/0.2
		if (bin_num>=0 and bin_num<Nbin):
			bin_num=int(bin_num)
			nz[bin_num][i]=nz[bin_num][i]+pdf(zph,z)*step_zph

for i in range(Nbin):
	normlize[i]=sum(nz[i])*step_z

NZ_SAMPLE['BIN1']=nz[0]/normlize[0]
NZ_SAMPLE['BIN2']=nz[1]/normlize[1]
NZ_SAMPLE['BIN3']=nz[2]/normlize[2]
NZ_SAMPLE['BIN4']=nz[3]/normlize[3]
NZ_SAMPLE['BIN5']=nz[4]/normlize[4]
NZ_SAMPLE['BIN6']=nz[5]/normlize[5]
NZ_SAMPLE['BIN7']=nz[6]/normlize[6]
NZ_SAMPLE['BIN8']=nz[7]/normlize[7]
NZ_SAMPLE['BIN9']=nz[8]/normlize[8]
NZ_SAMPLE['BIN10']=nz[9]/normlize[9]

print 'int n_1 / normlize = ',sum(NZ_SAMPLE['BIN1'])*step_z
print 'sum n_1..n_10 = ',sum(normlize)

f=fio.FITS('modified_cfhtlens_heymans13.fits','rw')
f['NZ_SAMPLE'].write(NZ_SAMPLE,clobber=True)
f.close()

# to add a tomographic bin, use:
# f=fio.FITS('modified_cfhtlens_heymans13.fits','rw')
# f[-1].insert_column(name='BIN#',data=nz[#-1]/normlize[#-1])

f=open('nbar_i.txt','w')
print >>f, normlize[0]
print >>f, normlize[1]
print >>f, normlize[2]
print >>f, normlize[3]
print >>f, normlize[4]
print >>f, normlize[5]
print >>f, normlize[6]
print >>f, normlize[7]
print >>f, normlize[8]
print >>f, normlize[9]
f.close()

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt
from matplotlib.pyplot import cm
from matplotlib.pylab import legend

fig=plt.figure()
colors=cm.rainbow(np.linspace(0,1,Nbin))
for i,c in zip(range(Nbin),colors):
	plt.plot(NZ_SAMPLE['Z_MID'],nz[i-1],color=c,label=str(i+1))
plt.plot(NZ_SAMPLE['Z_MID'],n_all(NZ_SAMPLE['Z_MID']),color='k',label='all')

plt.xlabel('z')
plt.ylabel('n(z)')
legend(loc='upper right')
plt.savefig('nz.png')


print '~~~~~~~ Finished calculating n_i(z) ~~~~~~~~~~~'
