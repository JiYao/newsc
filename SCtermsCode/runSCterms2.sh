#!/bin/bash

# SRUN OPTIONS
# NOTE: #SBATCH is a command, not a comment
# ----------------------------------------------------------------------
# NECESSARY PARAMETERS

# The number of tasks
#SBATCH -n 1

# ----------------------------------------------------------------------
# OPTIONAL PARAMETERS

# The number of cores per task (used for multi-threaded application, default=1, 
# prefer a power of 6, no more than the max number of cores per node)
# Change this when using openMP in camb for CosmoMC.  Only advantageous when calculating 
# CMB and MPK
#SBATCH -c 6
##SBATCH -c 8
# The number of tasks per node (n * c)
# Can uncomment this to force x number of tasks per node.
##SBATCH --tasks-per-node=1

# The number of nodes
# SLURM issues a warning if you specify more nodes than tasks (N > n). 
# It's better to let slurm calculate it for you.
# IMPORTANT: MPI between nodes will slow down the program because of the 
# heavy I/O. It's recommended to more cores and less nodes for our jobs. 
# Can change below to run MPI jobs across multiple nodes
#SBATCH -N 1
##SBATCH -N 4

# Use --exclusive to get the whole nodes exclusively for this job
##SBATCH --exclusive

# Request a sepcific list of hosts (two different formats)
##SBATCH -w f7
##SBATCH -w ./hostfile
## Request a specific list of hosts NOT be included
#SBATCH --exclude=cosmo,f1
# Setting the name of the error-file to 'job.myjobid.err' and 
# the output-file to 'job.myjobid.out'
# The file paths below are relative to the directory from which you submitted
# Change to your preferences.
#SBATCH --error=%J.err --output=%J.out

#Print  detailed  event  logging to error file
#SBATCH -v

#Give your job a name, so you can more easily identify which job is which
#SBATCH -J TomoC2
# ------------------------------
# VERY OPTIONAL PARAMETERS

# Account name (project ID) to run under
##SBATCH -A <account>

# The maximum allowed run time (D-HH:MM:)
##SBATCH --time=15-00:00:00

# If this job needs 4GB of memory per mpi-task (=mpi ranks, =cores)
##SBATCH --mem-per-cpu=4000

# ----------------------------------------------------------------------
# MPI SET-UP FOR SLURM

# Different types of MPI may result in unique initiation procedures.
# IMPORTANT for mpich2: user assumes the system administrator already link
# your program with SLURM's implementation of the PMI library
# If SLURM is not configured with MpiDefault=pmi2
# then the srun command MUST BE invoked with the option --mpi=pmi2.
# Reference: http://wiki.mpich.org/mpich/index.php/Frequently_Asked_Questions#Q:_How_do_I_use_MPICH_with_slurm.3F
# Reference: http://slurm.schedmd.com/mpi_guide.html#mpich2
# ---------------------------------------------------------------------



# Run the MPI application

#uncomment the line below if you want to run with openMP as advised above.  
#change number of threads to match -c line above.
#export OMP_NUM_THREADS=6

echo "Starting at `date`"
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"

# Changing to director where my application is since I submit from scripts

##rm generateinputfiles_spec.out
##ifort generateinputfiles_spec.f -o generateinputfiles_spec.out

##rm spec_tomo.out
##ifort spec_tomo.f -o spec_tomo.out

##rm tomo_input.out
##ifort tomo_input.f -o tomo_input.out
##./tomo_input.out < generateinputfiles_spec.in

# ------------------------------------------------------------


# ------------------------------------------------------------

mkdir ./SCterms/304/
cd SCterms/304/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_304.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/305/
cd SCterms/305/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_305.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/306/
cd SCterms/306/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_306.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..


# ------------------------------------------------------------

mkdir ./SCterms/307/
cd SCterms/307/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_307.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/307/
cd SCterms/307/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_307.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/308/
cd SCterms/308/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_308.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/309/
cd SCterms/309/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_309.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/405/
cd SCterms/405/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_405.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/406/
cd SCterms/406/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_406.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/407/
cd SCterms/407/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_407.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/408/
cd SCterms/408/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_408.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/409/
cd SCterms/409/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_409.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/506/
cd SCterms/506/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_506.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/507/
cd SCterms/507/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_507.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/508/
cd SCterms/508/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_508.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/509/
cd SCterms/509/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_509.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/607/
cd SCterms/607/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_607.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..


# ------------------------------------------------------------

mkdir ./SCterms/608/
cd SCterms/608/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_608.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/609/
cd SCterms/609/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_609.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/708/
cd SCterms/708/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_708.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/709/
cd SCterms/709/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_709.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./SCterms/809/
cd SCterms/809/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_809.in

#./spec_tomo.out < C_IG.in > C_IG.dat

# IG spec
#./spec_tomo.out < IG_1p.in > IG_1p.dat
#./spec_tomo.out < IG_1m.in > IG_1m.dat

#./spec_tomo.out < IG_2p.in > IG_2p.dat
#./spec_tomo.out < IG_2m.in > IG_2m.dat

#./spec_tomo.out < IG_3p.in > IG_3p.dat
#./spec_tomo.out < IG_3m.in > IG_3m.dat

#./spec_tomo.out < IG_4p.in > IG_4p.dat
#./spec_tomo.out < IG_4m.in > IG_4m.dat

#./spec_tomo.out < IG_7p.in > IG_7p.dat
#./spec_tomo.out < IG_7m.in > IG_7m.dat

#./spec_tomo.out < IG_8p.in > IG_8p.dat
#./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_9p.dat
./spec_tomo.out < IG_pz1m.in > IG_9m.dat

./spec_tomo.out < IG_pz2p.in > IG_10p.dat
./spec_tomo.out < IG_pz2m.in > IG_10m.dat

cd ../..


echo "Program finished with exit code $? at: `date`"

# ---------------------------------------------------
# Reference: http://www.hpc2n.umu.se/batchsystem/examples_scripts,
#            http://www.hpc2n.umu.se/slurm-submit-file-design
#            https://computing.llnl.gov/tutorials/linux_clusters/man/srun.txt
# ---------------------------------------------------
