       program generateinputfiles
c 
c  Automatize the creation of the 11 files for the 
c  the fiducial model and the files with p_i=p_i +/- epsilon
c
        implicit double precision (a-h,o-z)
        parameter (partial=0.15d0,dalphas=0.1d0,dwa=0.3d0,dDz=0.02d0)
		parameter (ilin=1,domegab=0.01,dpzrms=0.02d0)
		! partial derivative fraction & uncertainty of Delta_z
		parameter (IAtype=22) ! 21: power law correlation, 22: amplitude
c
	write(*,*)'Enter the parameters for the fiducial model'
        write(*,*)'omegam,omegah,sigma8,n,alphas,omegab,
     2  wQ0,ibn,wQ1 (0 for Mo etal)'
      read(*,*)omegam,omegah,sigma8,xn,alphas,omegab,wQ0,ibn,wQ1

	write(*,*)'Enter redshift to be used: '
        read(*,*) redshift
		
	write(*,*)'Enter density of galaxies: '
		read(*,*) ng
	write(*,*)'nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins'
		read(*,*) nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(*,*) 'power law: Agp alphagp, IA amplitude Amp0'
		read(*,*) Agp,alphagp,Amp0

!	write(*,*)'Enter the percentage for small variations in
!	2 parameters'
!	write(*,*)'This will be used to generate the input files that'
!	write(*,*)'will be used to calculated numerical derivatives: '
!        read(*,*) ppercent
c
c  C^GG_ii
c
        open(unit=9,file='C_GG.in',status='replace')
	write(9,*)3
      write(9,*)omegam,omegah,sigma8,xn,alphas,omegab,wQ0,ibn,wQ1
	write(9,*)ilin
	write(9,*)redshift,ng
	write(9,*)1
	write(9,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins !
	write(9,*)Agp,alphagp,Amp0
c
c  C^GG_ij
c
        open(unit=901,file='C_GGij.in',status='replace')
        write(901,*)3
      write(901,*)omegam,omegah,sigma8,xn,alphas,omegab,wQ0,ibn,wQ1
        write(901,*)ilin
        write(901,*)redshift,ng
        write(901,*)1
        write(901,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins !
		write(901,*)Agp,alphagp,Amp0
c
c  C^GG_jj
c
        open(unit=902,file='C_GGjj.in',status='replace')
        write(902,*)3
      write(902,*)omegam,omegah,sigma8,xn,alphas,omegab,wQ0,ibn,wQ1
        write(902,*)ilin
        write(902,*)redshift,ng
        write(902,*)1
        write(902,*)nbin2,nbin2,z0,Dz,pzrms,zmin,zmax,nbins !
		write(902,*)Agp,alphagp,Amp0

c
c  C^IG_ij
c
        open(unit=10,file='C_IG.in',status='replace')
        rewind 10
	write(10,*)3
        write(10,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(10,*)ilin
	write(10,*)redshift,ng
	write(10,*)2
	write(10,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(10,*)Agp,alphagp,Amp0
c
c  C^IG_ii
c
        open(unit=1001,file='C_IGii.in',status='replace')
        rewind 1001
        write(1001,*)3
        write(1001,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(1001,*)ilin
        write(1001,*)redshift,ng
        write(1001,*)2
        write(1001,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
		write(1001,*)Agp,alphagp,Amp0
c
c  C^IG_jj
c
        open(unit=1002,file='C_IGjj.in',status='replace')
        rewind 1002
        write(1002,*)3
        write(1002,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(1002,*)ilin
        write(1002,*)redshift,ng
        write(1002,*)2
        write(1002,*)nbin2,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
		write(1002,*)Agp,alphagp,Amp0
c 
c   C^II_ii
c
        open(unit=11,file='C_II.in',status='replace')
        rewind 11
	write(11,*)3
        write(11,*)omegam,omegah,sigma8,xn,
     2   alphas,omegab,wQ0,ibn,wQ1
	write(11,*)ilin
	write(11,*)redshift,ng
	write(11,*)3
	write(11,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(11,*)Agp,alphagp,Amp0
!
c   C^II_jj
c
        open(unit=1101,file='C_IIjj.in',status='replace')
        rewind 1101
        write(1101,*)3
        write(1101,*)omegam,omegah,sigma8,xn,
     2   alphas,omegab,wQ0,ibn,wQ1
        write(1101,*)ilin
        write(1101,*)redshift,ng
        write(1101,*)3
        write(1101,*)nbin2,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
		write(1101,*)Agp,alphagp,Amp0
c
c	C^gg_ii
c
        open(unit=15,file='C_gg.in',status='replace')
        rewind 15
	write(15,*)3
        write(15,*)omegam,omegah,sigma8,xn,
     2  alphas,omegab,wQ0,ibn,wQ1
	write(15,*)ilin
	write(15,*)redshift,ng
	write(15,*)4
	write(15,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(15,*)Agp,alphagp,Amp0
c
!	C^Gg_ii
!
        open(unit=16,file='C_Gg.in',status='replace')
        rewind 16
	write(16,*)3
        write(16,*)omegam,omegah,sigma8,xn,
     2  alphas,omegab,wQ0,ibn,wQ1
	write(16,*)ilin
	write(16,*)redshift,ng
	write(16,*)5
	write(16,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(16,*)Agp,alphagp,Amp0
c
!	C^Ig_ii
!
        open(unit=17,file='C_Ig.in',status='replace')
        rewind 17
	write(17,*)3
        write(17,*)omegam,omegah,sigma8,xn,
     2  alphas,omegab,wQ0,ibn,wQ1
	write(17,*)ilin
	write(17,*)redshift,ng
	write(17,*)6
	write(17,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(17,*)Agp,alphagp,Amp0
c
!	C^GG,N_ii
!
        open(unit=18,file='C_GGN.in',status='replace')
        rewind 18
	write(18,*)3
        write(18,*)omegam,omegah,sigma8,xn,
     2  alphas,omegab,wQ0,ibn,wQ1
	write(18,*)ilin
	write(18,*)redshift,ng
	write(18,*)7
	write(18,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(18,*)Agp,alphagp,Amp0
c
!       C^GG,N_jj
!
        open(unit=1801,file='C_GGNjj.in',status='replace')
        rewind 1801
        write(1801,*)3
        write(1801,*)omegam,omegah,sigma8,xn,
     2  alphas,omegab,wQ0,ibn,wQ1
        write(1801,*)ilin
        write(1801,*)redshift,ng
        write(1801,*)7
        write(1801,*)nbin2,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
		write(1801,*)Agp,alphagp,Amp0
c
!	C^gg,N_ii
!
        open(unit=19,file='C_ggN.in',status='replace')
        rewind 19
	write(19,*)3
        write(19,*)omegam,omegah,sigma8,xn,
     2  alphas,omegab,wQ0,ibn,wQ1
	write(19,*)ilin
	write(19,*)redshift,ng
	write(19,*)8
	write(19,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(19,*)Agp,alphagp,Amp0

!	
!   now starts files for partial derivatives
!
! ------------- IG spectrum -------------------------------------
c  IG spec 1p
c
        open(unit=21,file='IG_1p.in',status='replace')
        rewind 21
	write(21,*)3
        write(21,*)omegam*(1+partial),omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(21,*)ilin
	write(21,*)redshift,ng
	write(21,*)2
	write(21,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(21,*)Agp,alphagp,Amp0
c
c  IG spec 1m
c
        open(unit=22,file='IG_1m.in',status='replace')
        rewind 22
	write(22,*)3
        write(22,*)omegam*(1-partial),omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(22,*)ilin
	write(22,*)redshift,ng
	write(22,*)2
	write(22,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(22,*)Agp,alphagp,Amp0
c
c  IG spec 2p
c
        open(unit=23,file='IG_2p.in',status='replace')
        rewind 23
	write(23,*)3
        write(23,*)omegam,omegah*(1+partial),sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(23,*)ilin
	write(23,*)redshift,ng
	write(23,*)2
	write(23,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(23,*)Agp,alphagp,Amp0
c
c  IG spec 2m
c
        open(unit=24,file='IG_2m.in',status='replace')
        rewind 24
	write(24,*)3
        write(24,*)omegam,omegah*(1-partial),sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(24,*)ilin
	write(24,*)redshift,ng
	write(24,*)2
	write(24,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(24,*)Agp,alphagp,Amp0
c
c  IG spec 3p
c
        open(unit=25,file='IG_3p.in',status='replace')
        rewind 25
	write(25,*)3
        write(25,*)omegam,omegah,sigma8*(1+partial),
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(25,*)ilin
	write(25,*)redshift,ng
	write(25,*)2
	write(25,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(25,*)Agp,alphagp,Amp0
c
c  IG spec 3m
c
        open(unit=26,file='IG_3m.in',status='replace')
        rewind 26
	write(26,*)3
        write(26,*)omegam,omegah,sigma8*(1-partial),
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(26,*)ilin
	write(26,*)redshift,ng
	write(26,*)2
	write(26,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(26,*)Agp,alphagp,Amp0
c
c  IG spec 4p
c
        open(unit=27,file='IG_4p.in',status='replace')
        rewind 27
	write(27,*)3
        write(27,*)omegam,omegah,sigma8,
     2  xn*(1+partial),alphas,omegab,wQ0,ibn,wQ1
        write(27,*)ilin
	write(27,*)redshift,ng
	write(27,*)2
	write(27,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(27,*)Agp,alphagp,Amp0
c
c  IG spec 4m
c
        open(unit=28,file='IG_4m.in',status='replace')
        rewind 28
	write(28,*)3
        write(28,*)omegam,omegah,sigma8,
     2  xn*(1-partial),alphas,omegab,wQ0,ibn,wQ1
        write(28,*)ilin
	write(28,*)redshift,ng
	write(28,*)2
	write(28,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(28,*)Agp,alphagp,Amp0
c
c  IG spec 5p
c
        open(unit=29,file='IG_5p.in',status='replace')
        rewind 29
	write(29,*)3
        write(29,*)omegam,omegah,sigma8,
     2  xn,alphas+dalphas,omegab,wQ0,ibn,wQ1
        write(29,*)ilin
	write(29,*)redshift,ng
	write(29,*)2
	write(29,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(29,*)Agp,alphagp,Amp0
c
c  IG spec 5m
c
        open(unit=30,file='IG_5m.in',status='replace')
        rewind 30
	write(30,*)3
        write(30,*)omegam,omegah,sigma8,
     2  xn,alphas-dalphas,omegab,wQ0,ibn,wQ1
        write(30,*)ilin
	write(30,*)redshift,ng
	write(30,*)2
	write(30,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(30,*)Agp,alphagp,Amp0
c
c  IG spec 6p
c
        open(unit=31,file='IG_6p.in',status='replace')
        rewind 31
	write(31,*)3
        write(31,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab+domegab,wQ0,ibn,wQ1
        write(31,*)ilin
	write(31,*)redshift,ng
	write(31,*)2
	write(31,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(31,*)Agp,alphagp,Amp0
c
c  IG spec 6m
c
        open(unit=32,file='IG_6m.in',status='replace')
        rewind 32
	write(32,*)3
        write(32,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab-domegab,wQ0,ibn,wQ1
        write(32,*)ilin
	write(32,*)redshift,ng
	write(32,*)2
	write(32,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(32,*)Agp,alphagp,Amp0
c
c  IG spec 7p
c
        open(unit=33,file='IG_7p.in',status='replace')
        rewind 33
	write(33,*)3
        write(33,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0*(1+partial),ibn,wQ1
        write(33,*)ilin
	write(33,*)redshift,ng
	write(33,*)2
	write(33,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(33,*)Agp,alphagp,Amp0
c
c  IG spec 7m
c
        open(unit=34,file='IG_7m.in',status='replace')
        rewind 34
	write(34,*)3
        write(34,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0*(1-partial),ibn,wQ1
        write(34,*)ilin
	write(34,*)redshift,ng
	write(34,*)2
	write(34,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(34,*)Agp,alphagp,Amp0
c
c  IG spec 8p
c
        open(unit=35,file='IG_8p.in',status='replace')
        rewind 35
	write(35,*)3
        write(35,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1+dwa
        write(35,*)ilin
	write(35,*)redshift,ng
	write(35,*)2
	write(35,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(35,*)Agp,alphagp,Amp0
c
c  IG spec 8m
c
        open(unit=36,file='IG_8m.in',status='replace')
        rewind 36
	write(36,*)3
        write(36,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1-dwa
        write(36,*)ilin
	write(36,*)redshift,ng
	write(36,*)2
	write(36,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(36,*)Agp,alphagp,Amp0

c  photo-z parameters
c
c  IG spec photo-z parameter 1p
c
        open(unit=41,file='IG_pz1p.in',status='replace')
        rewind 41
	write(41,*)3
        write(41,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(41,*)ilin
	write(41,*)redshift,ng
	write(41,*)2
	write(41,*)nbin1,nbin2,z0,Dz+dDz,pzrms,zmin,zmax,nbins
	write(41,*)Agp,alphagp,Amp0
c
c  IG spec photo-z parameter 1m
c
        open(unit=42,file='IG_pz1m.in',status='replace')
        rewind 42
	write(42,*)3
        write(42,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(42,*)ilin
	write(42,*)redshift,ng
	write(42,*)2
	write(42,*)nbin1,nbin2,z0,Dz-dDz,pzrms,zmin,zmax,nbins
	write(42,*)Agp,alphagp,Amp0
c
c  IG spec photo-z parameter 2p
c
        open(unit=43,file='IG_pz2p.in',status='replace')
        rewind 43
	write(43,*)3
        write(43,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(43,*)ilin
	write(43,*)redshift,ng
	write(43,*)2
	write(43,*)nbin1,nbin2,z0,Dz,pzrms+dpzrms,zmin,zmax,nbins
	write(43,*)Agp,alphagp,Amp0
c
c  IG spec photo-z parameter 2m
c
        open(unit=44,file='IG_pz2m.in',status='replace')
        rewind 44
	write(44,*)3
        write(44,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(44,*)ilin
	write(44,*)redshift,ng
	write(44,*)2
	write(44,*)nbin1,nbin2,z0,Dz,pzrms-dpzrms,zmin,zmax,nbins
	write(44,*)Agp,alphagp,Amp0
c  IA parameters
c
c  IG IA parameter 1p
c
        open(unit=45,file='IG_IA1p.in',status='replace')
        rewind 45
	write(45,*)3
        write(45,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(45,*)ilin
	write(45,*)redshift,ng
	write(45,*)IAtype
	write(45,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(45,*)Agp*(1+partial),alphagp,Amp0*(1+partial)
c
c  IG IA parameter 1m
c
        open(unit=46,file='IG_IA1m.in',status='replace')
        rewind 46
	write(46,*)3
        write(46,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(46,*)ilin
	write(46,*)redshift,ng
	write(46,*)IAtype
	write(46,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(46,*)Agp*(1-partial),alphagp,Amp0*(1-partial)
c
c  IG IA parameter 2p
c
        open(unit=47,file='IG_IA2p.in',status='replace')
        rewind 47
	write(47,*)3
        write(47,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(47,*)ilin
	write(47,*)redshift,ng
	write(47,*)IAtype
	write(47,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(47,*)Agp,alphagp*(1+partial)
c
c  IG IA parameter 2m
c
        open(unit=48,file='IG_IA2m.in',status='replace')
        rewind 48
	write(48,*)3
        write(48,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(48,*)ilin
	write(48,*)redshift,ng
	write(48,*)IAtype
	write(48,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(48,*)Agp,alphagp*(1-partial)
	
!  ----------------- Ig spectrum --------------------------------------
c  Ig spec 1p
c
        open(unit=51,file='Ig_1p.in',status='replace')
        rewind 51
	write(51,*)3
        write(51,*)omegam*(1+partial),omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(51,*)ilin
	write(51,*)redshift,ng
	write(51,*)6
	write(51,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(51,*)Agp,alphagp,Amp0
c
c  Ig spec 1m
c
        open(unit=52,file='Ig_1m.in',status='replace')
        rewind 52
	write(52,*)3
        write(52,*)omegam*(1-partial),omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(52,*)ilin
	write(52,*)redshift,ng
	write(52,*)6
	write(52,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(52,*)Agp,alphagp,Amp0
c
c  Ig spec 2p
c
        open(unit=53,file='Ig_2p.in',status='replace')
        rewind 53
	write(53,*)3
        write(53,*)omegam,omegah*(1+partial),sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(53,*)ilin
	write(53,*)redshift,ng
	write(53,*)6
	write(53,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(53,*)Agp,alphagp,Amp0
c
c  Ig spec 2m
c
        open(unit=54,file='Ig_2m.in',status='replace')
        rewind 54
	write(54,*)3
        write(54,*)omegam,omegah*(1-partial),sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(54,*)ilin
	write(54,*)redshift,ng
	write(54,*)6
	write(54,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(54,*)Agp,alphagp,Amp0
c
c  Ig spec 3p
c
        open(unit=55,file='Ig_3p.in',status='replace')
        rewind 55
	write(55,*)3
        write(55,*)omegam,omegah,sigma8*(1+partial),
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(55,*)ilin
	write(55,*)redshift,ng
	write(55,*)6
	write(55,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(55,*)Agp,alphagp,Amp0
c
c  Ig spec 3m
c
        open(unit=56,file='Ig_3m.in',status='replace')
        rewind 56
	write(56,*)3
        write(56,*)omegam,omegah,sigma8*(1-partial),
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(56,*)ilin
	write(56,*)redshift,ng
	write(56,*)6
	write(56,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(56,*)Agp,alphagp,Amp0
c
c  Ig spec 4p
c
        open(unit=57,file='Ig_4p.in',status='replace')
        rewind 57
	write(57,*)3
        write(57,*)omegam,omegah,sigma8,
     2  xn*(1+partial),alphas,omegab,wQ0,ibn,wQ1
        write(57,*)ilin
	write(57,*)redshift,ng
	write(57,*)6
	write(57,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(57,*)Agp,alphagp,Amp0
c
c  Ig spec 4m
c
        open(unit=58,file='Ig_4m.in',status='replace')
        rewind 58
	write(58,*)3
        write(58,*)omegam,omegah,sigma8,
     2  xn*(1-partial),alphas,omegab,wQ0,ibn,wQ1
        write(58,*)ilin
	write(58,*)redshift,ng
	write(58,*)6
	write(58,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(58,*)Agp,alphagp,Amp0
c
c  Ig spec 5p
c
        open(unit=59,file='Ig_5p.in',status='replace')
        rewind 59
	write(59,*)3
        write(59,*)omegam,omegah,sigma8,
     2  xn,alphas+dalphas,omegab,wQ0,ibn,wQ1
        write(59,*)ilin
	write(59,*)redshift,ng
	write(59,*)6
	write(59,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(59,*)Agp,alphagp,Amp0
c
c  Ig spec 5m
c
        open(unit=60,file='Ig_5m.in',status='replace')
        rewind 60
	write(60,*)3
        write(60,*)omegam,omegah,sigma8,
     2  xn,alphas-dalphas,omegab,wQ0,ibn,wQ1
        write(60,*)ilin
	write(60,*)redshift,ng
	write(60,*)6
	write(60,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(60,*)Agp,alphagp,Amp0
c
c  Ig spec 6p
c
        open(unit=61,file='Ig_6p.in',status='replace')
        rewind 61
	write(61,*)3
        write(61,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab+domegab,wQ0,ibn,wQ1
        write(61,*)ilin
	write(61,*)redshift,ng
	write(61,*)6
	write(61,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(61,*)Agp,alphagp,Amp0
c
c  Ig spec 6m
c
        open(unit=62,file='Ig_6m.in',status='replace')
        rewind 62
	write(62,*)3
        write(62,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab-domegab,wQ0,ibn,wQ1
        write(62,*)ilin
	write(62,*)redshift,ng
	write(62,*)6
	write(62,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(62,*)Agp,alphagp,Amp0
c
c  Ig spec 7p
c
        open(unit=63,file='Ig_7p.in',status='replace')
        rewind 63
	write(63,*)3
        write(63,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0*(1+partial),ibn,wQ1
        write(63,*)ilin
	write(63,*)redshift,ng
	write(63,*)6
	write(63,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(63,*)Agp,alphagp,Amp0
c
c  Ig spec 7m
c
        open(unit=64,file='Ig_7m.in',status='replace')
        rewind 64
	write(64,*)3
        write(64,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0*(1-partial),ibn,wQ1
        write(64,*)ilin
	write(64,*)redshift,ng
	write(64,*)6
	write(64,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(64,*)Agp,alphagp,Amp0
c
c  Ig spec 8p
c
        open(unit=65,file='Ig_8p.in',status='replace')
        rewind 65
	write(65,*)3
        write(65,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1+dwa
        write(65,*)ilin
	write(65,*)redshift,ng
	write(65,*)6
	write(65,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(65,*)Agp,alphagp,Amp0
c
c  Ig spec 8m
c
        open(unit=66,file='Ig_8m.in',status='replace')
        rewind 66
	write(66,*)3
        write(66,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1-dwa
        write(66,*)ilin
	write(66,*)redshift,ng
	write(66,*)6
	write(66,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(66,*)Agp,alphagp,Amp0

c  photo-z parameters
c
c  Ig spec photo-z parameter 1p
c
        open(unit=71,file='Ig_pz1p.in',status='replace')
        rewind 71
	write(71,*)3
        write(71,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(71,*)ilin
	write(71,*)redshift,ng
	write(71,*)6
	write(71,*)nbin1,nbin1,z0,Dz+dDz,pzrms,zmin,zmax,nbins
	write(71,*)Agp,alphagp,Amp0
c
c  Ig spec photo-z parameter 1m
c
        open(unit=72,file='Ig_pz1m.in',status='replace')
        rewind 72
	write(72,*)3
        write(72,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(72,*)ilin
	write(72,*)redshift,ng
	write(72,*)6
	write(72,*)nbin1,nbin1,z0,Dz-dDz,pzrms,zmin,zmax,nbins
	write(72,*)Agp,alphagp,Amp0
c
c  Ig spec photo-z parameter 2p
c
        open(unit=73,file='Ig_pz2p.in',status='replace')
        rewind 73
	write(73,*)3
        write(73,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(73,*)ilin
	write(73,*)redshift,ng
	write(73,*)6
	write(73,*)nbin1,nbin1,z0,Dz,pzrms+dpzrms,zmin,zmax,nbins
	write(73,*)Agp,alphagp,Amp0
c
c  Ig spec photo-z parameter 2m
c
        open(unit=74,file='Ig_pz2m.in',status='replace')
        rewind 74
	write(74,*)3
        write(74,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(74,*)ilin
	write(74,*)redshift,ng
	write(74,*)6
	write(74,*)nbin1,nbin1,z0,Dz,pzrms-dpzrms,zmin,zmax,nbins
	write(74,*)Agp,alphagp,Amp0


! --------------- GG spectrum ------------------------------
c
c  GG spec 1p
c
        open(unit=81,file='Gg_1p.in',status='replace')
        rewind 81
	write(81,*)3
        write(81,*)omegam*(1+partial),omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(81,*)ilin
	write(81,*)redshift,ng
	write(81,*)5
	write(81,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(81,*)Agp,alphagp,Amp0
c
c  GG spec 1m
c
        open(unit=82,file='Gg_1m.in',status='replace')
        rewind 82
	write(82,*)3
        write(82,*)omegam*(1-partial),omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(82,*)ilin
	write(82,*)redshift,ng
	write(82,*)5
	write(82,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(82,*)Agp,alphagp,Amp0
c
c  GG spec 2p
c
        open(unit=83,file='Gg_2p.in',status='replace')
        rewind 83
	write(83,*)3
        write(83,*)omegam,omegah*(1+partial),sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(83,*)ilin
	write(83,*)redshift,ng
	write(83,*)5
	write(83,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(83,*)Agp,alphagp,Amp0
c
c  GG spec 2m
c
        open(unit=84,file='Gg_2m.in',status='replace')
        rewind 84
	write(84,*)3
        write(84,*)omegam,omegah*(1-partial),sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(84,*)ilin
	write(84,*)redshift,ng
	write(84,*)5
	write(84,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(84,*)Agp,alphagp,Amp0
c
c  GG spec 3p
c
        open(unit=85,file='Gg_3p.in',status='replace')
        rewind 85
	write(85,*)3
        write(85,*)omegam,omegah,sigma8*(1+partial),
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(85,*)ilin
	write(85,*)redshift,ng
	write(85,*)5
	write(85,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(85,*)Agp,alphagp,Amp0
c
c  GG spec 3m
c
        open(unit=86,file='Gg_3m.in',status='replace')
        rewind 86
	write(86,*)3
        write(86,*)omegam,omegah,sigma8*(1-partial),
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(86,*)ilin
	write(86,*)redshift,ng
	write(86,*)5
	write(86,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(86,*)Agp,alphagp,Amp0
c
c  GG spec 4p
c
        open(unit=87,file='Gg_4p.in',status='replace')
        rewind 87
	write(87,*)3
        write(87,*)omegam,omegah,sigma8,
     2  xn*(1+partial),alphas,omegab,wQ0,ibn,wQ1
        write(87,*)ilin
	write(87,*)redshift,ng
	write(87,*)5
	write(87,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(87,*)Agp,alphagp,Amp0
c
c  GG spec 4m
c
        open(unit=88,file='Gg_4m.in',status='replace')
        rewind 88
	write(88,*)3
        write(88,*)omegam,omegah,sigma8,
     2  xn*(1-partial),alphas,omegab,wQ0,ibn,wQ1
        write(88,*)ilin
	write(88,*)redshift,ng
	write(88,*)5
	write(88,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(88,*)Agp,alphagp,Amp0
c
c  GG spec 5p
c
        open(unit=89,file='Gg_5p.in',status='replace')
        rewind 89
	write(89,*)3
        write(89,*)omegam,omegah,sigma8,
     2  xn,alphas+dalphas,omegab,wQ0,ibn,wQ1
        write(89,*)ilin
	write(89,*)redshift,ng
	write(89,*)5
	write(89,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(89,*)Agp,alphagp,Amp0
c
c  GG spec 5m
c
        open(unit=90,file='Gg_5m.in',status='replace')
        rewind 90
	write(90,*)3
        write(90,*)omegam,omegah,sigma8,
     2  xn,alphas-dalphas,omegab,wQ0,ibn,wQ1
        write(90,*)ilin
	write(90,*)redshift,ng
	write(90,*)5
	write(90,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(90,*)Agp,alphagp,Amp0
c
c  GG spec 6p
c
        open(unit=91,file='Gg_6p.in',status='replace')
        rewind 91
	write(91,*)3
        write(91,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab+domegab,wQ0,ibn,wQ1
        write(91,*)ilin
	write(91,*)redshift,ng
	write(91,*)5
	write(91,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(91,*)Agp,alphagp,Amp0
c
c  GG spec 6m
c
        open(unit=92,file='Gg_6m.in',status='replace')
        rewind 92
	write(92,*)3
        write(92,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab-domegab,wQ0,ibn,wQ1
        write(92,*)ilin
	write(92,*)redshift,ng
	write(92,*)5
	write(92,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(92,*)Agp,alphagp,Amp0
c
c  GG spec 7p
c
        open(unit=93,file='Gg_7p.in',status='replace')
        rewind 93
	write(93,*)3
        write(93,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0*(1+partial),ibn,wQ1
        write(93,*)ilin
	write(93,*)redshift,ng
	write(93,*)5
	write(93,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(93,*)Agp,alphagp,Amp0
c
c  GG spec 7m
c
        open(unit=94,file='Gg_7m.in',status='replace')
        rewind 94
	write(94,*)3
        write(94,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0*(1-partial),ibn,wQ1
        write(94,*)ilin
	write(94,*)redshift,ng
	write(94,*)5
	write(94,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(94,*)Agp,alphagp,Amp0
c
c  GG spec 8p
c
        open(unit=95,file='Gg_8p.in',status='replace')
        rewind 95
	write(95,*)3
        write(95,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1+dwa
        write(95,*)ilin
	write(95,*)redshift,ng
	write(95,*)5
	write(95,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(95,*)Agp,alphagp,Amp0
c
c  GG spec 8m
c
        open(unit=96,file='Gg_8m.in',status='replace')
        rewind 96
	write(96,*)3
        write(96,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1-dwa
        write(96,*)ilin
	write(96,*)redshift,ng
	write(96,*)5
	write(96,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(96,*)Agp,alphagp,Amp0

c  photo-z parameters
c
c  GG spec photo-z parameter 1p
c
        open(unit=101,file='Gg_pz1p.in',status='replace')
        rewind 101
	write(101,*)3
        write(101,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(101,*)ilin
	write(101,*)redshift,ng
	write(101,*)5
	write(101,*)nbin1,nbin2,z0,Dz+dDz,pzrms,zmin,zmax,nbins
	write(101,*)Agp,alphagp,Amp0
c
c  GG spec photo-z parameter 1m
c
        open(unit=102,file='Gg_pz1m.in',status='replace')
        rewind 102
	write(102,*)3
        write(102,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(102,*)ilin
	write(102,*)redshift,ng
	write(102,*)5
	write(102,*)nbin1,nbin2,z0,Dz-dDz,pzrms,zmin,zmax,nbins
	write(102,*)Agp,alphagp,Amp0
c
c  GG spec photo-z parameter 2p
c
        open(unit=103,file='Gg_pz2p.in',status='replace')
        rewind 103
	write(103,*)3
        write(103,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(103,*)ilin
	write(103,*)redshift,ng
	write(103,*)5
	write(103,*)nbin1,nbin2,z0,Dz,pzrms+dpzrms,zmin,zmax,nbins
	write(103,*)Agp,alphagp,Amp0
c
c  GG spec photo-z parameter 2m
c
        open(unit=104,file='Gg_pz2m.in',status='replace')
        rewind 104
	write(104,*)3
        write(104,*)omegam,omegah,sigma8,
     2  xn,alphas,omegab,wQ0,ibn,wQ1
        write(104,*)ilin
	write(104,*)redshift,ng
	write(104,*)5
	write(104,*)nbin1,nbin2,z0,Dz,pzrms-dpzrms,zmin,zmax,nbins
	write(104,*)Agp,alphagp,Amp0

	
	close(9)
        close(901)
        close(902)
	close(10)
        close(1001)
        close(1002)
	close(11)
	close(15)
	close(16)
	close(17)
	close(18)
        close(1801)
	close(19)
	
	close(21)
	close(22)
	close(23)
	close(24)
	close(25)
	close(26)
	close(27)
	close(28)
	close(29)
	close(30)
	close(31)
	close(32)
	close(33)
	close(34)
	close(35)
	close(36)
	
	close(41)
	close(42)
	close(43)
	close(44)
	
	close(45)
	close(46)
	close(47)
	close(48)
	
	close(51)
	close(52)
	close(53)
	close(54)
	close(55)
	close(56)
	close(57)
	close(58)
	close(59)
	close(60)
	close(61)
	close(62)
	close(63)
	close(64)
	close(65)
	close(66)
	
	close(71)
	close(72)
	close(73)
	close(74)
	
	close(81)
	close(82)
	close(83)
	close(84)
	close(85)
	close(86)
	close(87)
	close(88)
	close(89)
	close(90)
	close(91)
	close(92)
	close(93)
	close(94)
	close(95)
	close(96)
	
	close(101)
	close(102)
	close(103)
	close(104)
        stop
        end
