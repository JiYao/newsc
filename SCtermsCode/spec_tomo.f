        program dthcqd
!=================================================================
!
! fluctuations in angular separation due to the potential fluctuations
! from Limber's eqn. using nonlinear evolution. normalize to sigma8
! units are Mpc
!
! This version of the program was modified in 2003 to include:
!
! 1) A new non-linear mapping procedure from Smith & Peacock 2002
!
! 2) A curved power spectrum with a parameter alpha_s
!
! 3) A new procedure to include the source redshift distribution
!    with the z0 as a parameter. The distribution peaks at 2 z0
!
! Note:  The PSF calibration parameter Cs is added in the Fisher
!    matrix analysis program
!
!    Mustapha Ishak, 2003
!
!=================================================================
!
        implicit double precision (a-h,o-z)
        parameter (tol=1.0d-5,nx=1000,ntheta=31,delta=0.01)
        parameter (kmax=5000,ntau=50,nknl=21,nomega=5)
        parameter (partial=0.15d0,dalphas=0.1d0,dwa=0.3d0,dDz=0.01d0)
        dimension akk(kmax),tf(kmax)
        dimension atrg(ntau,5)
        dimension delta2nl(nknl,ntau),alnknl(nknl,ntau),d2nlp(nknl,ntau)
        dimension xtau(nx),xa(nx),xpr(nx),sigth(ntheta,nomega)
        common /idum/ ilin
        common /tfun/ akk,tf,k0
        double precision ng,tempz
		integer typ
		double precision omegab
		common omegab
        common /cosmos/ omegam,omegav,omegah,h0,xn,alphas,
     2                  z0,wQ0,wQ1,ispect,typ,ibn,alphan,ng
        double precision ko, k_, lnko , Qqz
        common /pivot/ ko, k_, lnko
        common /peacock/ delta10,xm,alnkc
        common /nlps/ delta2nl,alnknl,d2nlp
        common /cosmo2/ curv,tau0,r0,xnorm8
        common /par/ tau1,r1,theta,atrg
        common /temp/itheta
        common /sigk/ sigr0
        common /lens/ cpp,cppint
		common /IA/ Agp,alphagp,Amp0
		common /passing/ akp
        character*80 filename
                double precision Wij,Delta_inv,Q
                common /SC/ Wij,Delta_inv,Q

        double precision  rn,plin,pq,ph,pnl,rk
        double precision  rknl,rneff,rncur,d1,d2,sig
      double precision f1a,f2a,f3a,f4a,f1b,f2b,f3b,f4b,frac,f1,f2,f3,f4
        double precision  diff,xlogr1,xlogr2,rmid

        INTEGER nbad,nok,nvar,nbin,nbin1,nbin2
        parameter (nvar=2)
        double precision eps,h1,hmin,x1,x2,ystart(nvar)

        external odeint
        external rkck
        external rkqs
        external derivs

        external p2
        external psk
        external dnorm8
        external dnormk
        external fnl
        external fnl0
        external t2
        external func1
		external fP_dI ! P_delta,I

        ko=0.05
        lnko=log(ko)

        fourpi=4.0d0*3.1415926535898d0
        pi=3.1415926535898d0
!!        write(*,*)'BBKS(3) or POTENT(2) tf:'
        read(*,*)ispect
                open(unit=10,file='z.in',status='unknown')
                rewind 10
        if (ispect.eq.2) then
!!        write(*,*)'omegam,omegav,m,delta2,akc,ibn (0 for Mo etal)'
        read(*,*)omegam,omegav,xm,delta10,akc,ibn
        f2=exp(log(omegam)*1.2)
        delta10=delta10/f2
        alnkc=log(akc)
        else if (ispect.eq.3) then
!
!  alphas, z0 and wQ added to the list of input parameters
!
!!      write(*,*)'omegam,omegah,sigma8,n,alphas,omegab,w,dw,ibn'
      read(*,*)omegam,omegah,sigma8,xn,alphas,omegab,wQ0,ibn,wQ1
!!      write(*,*)omegam,omegah,sigma8,xn,alphas,z0,wQ0,wQ1,ibn
        omegav=1.-omegam
        endif
!!        write(*,*)'linear (0) or nonlinear (1) calculation:'
        read(*,*)ilin
!!        write(*,*)'redshift, density of galaxies'
        read(*,*)z,ng
!!        write(*,*)'Type of spectrum: (1=GG, 2=GI, 3=II,
!!     .               4=gg, 5=gG, 6=gI, 7=GG,N, 8=gg,N,
!!     .               21=IG_IA_PowerLaw)'
        read(*,*)typ
!!      write(*,*)'nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins'
      read(*,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
!!      write(*,*)nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
		read(*,*)Agp,alphagp,Amp0

        a1=1.0/(1.0+z)
        h0=omegah/omegam*100. !71.
        hc=2.998d5/h0
        if (ispect.eq.0) then
!!        write(*,*) 'Enter filename'
        read(*,'(a80)') filename
        open(unit=9,file=filename
     <,status='unknown')
        rewind 9
        i=1
30      read(9,*,end=20)akk(i),tf(i)
        i=i+1
        goto 30
20      continue
        k0=i-1
        endif
           l=1
! conformal time today
        a0=0.
        at=1.0
        tau0=hc*rombint(func1,a0,at,tol)
        tau1=hc*rombint(func1,a0,a1,tol)
!!        write(*,*)'distance ratio:',(1-tau1/tau0)
        curv=(omegam+omegav-1.)/hc/hc
        alnkmin=log(1.d-5)
        alnkmax=log(50.d0)
!  do the k integration using Romberg integrator
        if (ispect.eq.2) then
         alnk10=log(0.1)
! xn not expanded here : we are not using this section
         d10=fourpi*exp(alnk10*(xn+3.0))*t2(alnk10)
         xnorm8=delta10/d10
         sig8=sqrt(xnorm8*fourpi*rombint(dnorm8,alnkmin,alnkmax,tol))
!!         write(*,*)sig8
         goto 111
         do i=1,20
          alnk=alnkmin+i/20.*(alnkmax-alnkmin)
!!          write(10,*)exp(alnk),t2(alnk)
         enddo
111      continue
        else if (ispect.eq.3) then
! sigma8 linear normalization today
        sig0=fourpi*rombint(dnorm8,alnkmin,alnkmax,tol)
        xnorm8=sigma8*sigma8/sig0
       endif
! set up a power spectrum map for ntau points in time. Consider
! only k>kcrit for which linear Delta^2>0.1 today.
! find kcrit between kmin and kmax.
        alnklo=alnkmin
        alnkhi=alnkmax
        delta2crit=0.1
60      if (alnkhi-alnklo.gt.0.01) then
         alnk=(alnkhi+alnklo)/2.
         k_=exp(alnk)
         delta2=fourpi*xnorm8*(k_/ko)**
     2     (3+xn+0.5*alphas*(alnk-lnko))*t2(alnk)
         if (delta2.gt.delta2crit) then
          alnkhi=alnk
         else
          alnklo=alnk
         endif
         goto 60
        endif
        alnkcrit=alnk
!!        write(*,*)'k(delta2=',delta2crit,')=',exp(alnkcrit)
        alnk=log(0.1)
        k_=0.1
        delta2=fourpi*xnorm8*(k_/ko)**
     2       (3+xn+0.5*alphas*(alnk-lnko))*t2(alnk)
!!        write(*,*)'delta2(0.1)=',delta2
! set up (a,tau,r,g,n_eff) table for ntime points equally spaced
! in time; use interpolation between a and tau
        xaold=a1
        xtauold=tau1/hc
        do 15 i=1,nx
         xa(i)=xaold+(at-a1)/nx
         xtau(i)=xtauold+rombint(func1,xaold,xa(i),tol)
         xaold=xa(i)
         xtauold=xtau(i)
15      continue
        xaplo=1./func1(xa(1))
        xaphi=1./func1(1.0d0)
        call spline(xtau,xa,nx,xaplo,xaphi,xpr)
!
        do 40 i=1,ntau
         tau=tau1+i*(tau0-tau1)/ntau
         call splint(xtau,xa,xpr,nx,tau/hc,a) ! get a(tau/hc)
         r=tau0-tau
         if (curv.ne.0.0d0) then
         if (curv.gt.0.) then
          r=sin(sqrt(curv)*r)/sqrt(curv)
         else
          r=sinh(sqrt(-curv)*r)/sqrt(-curv)
         endif
         endif
         if (r.lt.0.0d0) r=0.0d0
!
!   here the growth factor with wQ=-1 (see Lahav et al 1991)

!        x=1+omegam*(1./a-1)+omegav*(a*a-1)
!        f=exp(0.6*log(omegam/a/x))
!        g=2.5*omegam/(x*f+1.5*omegam/a+1.-omegam-omegav)

!---------------------------------------------------------------
!
!       integration of the growth factor for other models
!

        eps=1.0d-7

!       write(*,*)'Enter ystart(1)(g), ystart(2)(dg/da)'
!       read (*,*) ystart(1),ystart(2)
!       write(*,*)'Enter h1 (stepsize), hmin (zero is ok)'
!       read (*,*) h1, hmin
        ystart(1)=1.
        ystart(2)=0.
        h1=0.001
        hmin=0.00001
        x1=0.001
        x2=a
!write(*,*)x1,x2,a

      call odeint(ystart,nvar,x1,x2,eps,h1,hmin,nok,nbad,derivs,
     *rkqs)
!write(*,*)'end of integration'
!write(33,*)a,ystart(1),g/a
        g=ystart(1)*a
!      write(*,*)omegav/omegam*a**3,ystart(1)

!-----------------------------------------------------------------
!
        if (ibn.eq.0) then
! Mo et al. procedure for varying n
! find k where sigmak=1
          if (i.eq.1) then
           alnkhi=alnkmax
          else
           alnkhi=alnkold
          endif
          alnklo=log(1.0d-2)
          sigmakcrit=1.0
 61       if (alnkhi-alnklo.gt.delta) then
           alnk=(alnkhi+alnklo)/2.
           sigr0=1.0/exp(alnk)
           sigmaka=xnorm8*fourpi*rombint(dnormk,alnkmin,alnkmax,tol)*a*a
           if (sigmaka.gt.sigmakcrit) then
            alnkhi=alnk
           else
            alnklo=alnk
           endif
           goto 61
          endif
         alnkold=alnk
! find n_eff
         alnklow=alnk-delta
         t2low=log(t2(alnklow))
         alnkhigh=alnk+delta
         t2high=log(t2(alnkhigh))
! xn here not expanded but this section is not used
         aneff=xn+(t2high-t2low)/2./delta
! compute Mo et al. correction
         dum1=(17.+aneff)/(10.+2.*aneff)
         dum2=(11.+aneff)/(10.+2.*aneff)
         gamma1=gammln(dum1)
         gamma2=gammln(dum2)
         bn=0.795*exp((5.+aneff)*(gamma2-gamma1))
         atrg(i,5)=bn
         endif
         atrg(i,1)=tau
         atrg(i,2)=a
         atrg(i,3)=r
         atrg(i,4)=g
        tempz=1/a-1
!!        write(10,*) tempz
!        write(9,'(6E13.5)')(atrg(i,j),j=1,4),aneff,exp(alnk)
40      continue
!!        write(*,*)'done !'
        close(10)
!
        dlnk=(alnkmax-alnkcrit)/(nknl-1.)
        g0=atrg(ntau,4)
         if (curv.eq.0.) then
          r1=tau0-tau1
         else if (curv.gt.0.) then
          r1=sin(sqrt(curv)*(tau0-tau1))/sqrt(curv)
         else
          r1=sinh(sqrt(-curv)*(tau0-tau1))/sqrt(-curv)
         endif
!
!=================================================================
!
        do 80 i=1,ntau

         g=atrg(i,4)
         a=atrg(i,2)
!
! nonlinear mapping
! from halofit program Smith and Peacock
! calculate nonlinear wavenumber (rknl), effective spectral index (rneff) and
! curvature (rncur) of the power spectrum at the desired redshift, using method
! described in Smith et al (2002).

!     write(*,*) 'computing effective spectral quantities:'

         xlogr1=-2.0
         xlogr2=3.5
 10      rmid=(xlogr2+xlogr1)/2.0
         rmid=10**rmid
         call wint(i,rmid,sig,d1,d2)
         diff=sig-1.0
         if (abs(diff).le.0.001) then
            rknl=1./rmid
            rneff=-3-d1
            rncur=-d2
         elseif (diff.gt.0.001) then
            xlogr1=log10(rmid)
            goto 10
         elseif (diff.lt.-0.001) then
            xlogr2=log10(rmid)
            goto 10
         endif

!      write(*,201) 'rknl [h/Mpc] =',rknl,'rneff=',rneff, 'rncur=',rncur
!201   format(a14,f12.6,2x,a6,f12.6,2x,a6,f12.6)
!
!
         if(ibn.eq.0) then
          bn=atrg(i,5)
         else
          bn=1.0d0
         endif
!
         do 70 j=1,nknl

          alnkl=alnkcrit+(j-1.)*dlnk
         k_=exp(alnkl)
          delta2=fourpi*xnorm8*(k_/ko)**(3+xn+0.5*alphas*
     2    (alnkl-lnko))*t2(alnkl)*g*g/g0/g0
          sav_alnkl=alnkl
!
! use Mo et al. if omegam=1 omegav=0, Smith and Peacock elsewhere
!
          if (omegam.eq.1.0.and.omegav.eq.0.0.and.ibn.eq.0) then
           delta2=delta2/bn
           delta2nl(j,i)=fnl0(delta2)*bn
          else
            rk=exp(alnkl)
            call halofit2(rk,rneff,rncur,rknl,delta2,pnl,pq,ph,a) !
            delta2nl(j,i)=pnl
         endif
          alnknl(j,i)=alnkl
!
! use log(delta2) to facilitate interpolation
          if (i.eq.ntau) then
!       write(11,'(4E14.6)')exp(alnknl(j,i)),delta2nl(j,i)
          endif
          delta2nl(j,i)=log(delta2nl(j,i))
70       continue
!
! try first derivative on low k end
!        d2nlplo=(3+xn)*fourpi*xnorm8*exp(alnkcrit*(2+xn))*t2(alnkcrit)
! natural spline bc on high and low k end;
        d2nlplo=1.d40
        d2nlphi=1.d40
         call spline(alnknl(1,i),delta2nl(1,i),nknl,d2nlplo,
     2  d2nlphi,d2nlp(1,i))
80       continue
!
!=================================================================
!
! k integration
        theta0=1./10000. !pi/180./60./60.0
        theta1=1./20. !pi/36.
        dlntheta=(log(theta1)-log(theta0))/(ntheta-1)
        dalnk=alnkmax-alnkmin
        alnk1=log(0.01)
        alnk2=log(10.0)
!         do ik=1,100
!          alnk=alnk1+(ik-1.0)/100.*(alnk2-alnk1)
!          dsig8=dnorm8(alnk)
!          write(10,*)exp(alnk),fourpi*dsig8
!         enddo
        do 130 itheta=1,ntheta
         theta=exp(log(theta0)+dlntheta*(itheta-1))
         xkappa=1.0/theta
	if (xkappa.gt.35000.) then
		goto 130
	endif
!         p=sqrt(fourpi*
!    2    rombint(p2,alnkmin,alnkmax,tol))
!         if (cppint.gt.0.) then
!          cppsqrt=sqrt(fourpi*cppint)
!         else
!          cppsqrt=sqrt(-fourpi*cppint)
!         endif
!         do ik=1,100
!          alnk=alnk1+(ik-1.0)/100.*(alnk2-alnk1)
!          dp2=fourpi*p2(alnk)
!          dcpp=fourpi*cpp
!           write(10+itheta,'(5E14.5)'),exp(alnk),dp2,dcpp
!         enddo
          if (typ.lt.7 .or. typ.gt.8) then
		   sigma1=psk(xkappa,nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,
     .      nbins)
!           sigma1=xkappa*xkappa*pi*psk(xkappa,nbin1,nbin2,z0,
!     .     Dz,pzrms,zmin,zmax,nbins)*2*pi/xkappa/xkappa
!           print *, 'sigma',psk(xkappa),sigma1
          else
           sigma1=psk(xkappa,nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,
     .      nbins)
!!           print *, 'tother'
          end if
!         write(8,'(4E14.5)')theta/theta0,p,cppsqrt,sigma1
!!          write(int(typ)+20,'(5E13.4)')xkappa,sigma1,Wij,Delta_inv,Q
          write(*,'(5E13.4)')xkappa,sigma1,Wij,Delta_inv,Q!*xkappa*xkappa/2.d0/pi
!         write(*,'(4E14.5)')theta/theta0,p,cppsqrt,sigma1
130      continue
        stop
        end

!%%  Smith and Peacock 2002 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! subroutine changed to fit within the lensing program (MI 2003)
!
! The subroutine wint, finds the effective spectral quantities
! rknl, rneff & rncur. This it does by calculating the radius of
! the Gaussian filter at which the variance is unity = rknl.
! rneff is defined as the first derivative of the variance, calculated
! at the nonlinear wavenumber and similarly the rncur is the second
! derivative at the nonlinear wavenumber.

      subroutine wint(ind,r,sig,d1,d2)
!       implicit none
        implicit double precision  (a-h,o-z)
        parameter (ntau=50,nknl=21,fourpi=4.0d0*3.1415926535898d0)
        dimension atrg(ntau,5)
		integer typ
        common /cosmos/ omegam,omegav,omegah,h0,xn,alphas,
     2                  z0,wQ0,wQ1,ispect,typ,ibn,alphan,ng
        common /cosmo2/ curv,tau0,r0,xnorm8
        double precision ko, k_, lnko
        common /pivot/ ko, k_, lnko
        common /par/ tau1,r1,theta,atrg
      double precision  sum1,sum2,sum3,t,y,x,w1,w2,w3
      double precision  rk, r, sig, d1,d2
!     double precision  om_m,om_v,h,p_index,gams,sig8,amp
      integer i,nint
!     common/cospar/om_m,om_v,om_b,h,p_index,gams,sig8,amp
      g0=atrg(ntau,4)
      tau=atrg(ind,1)
      a=atrg(ind,2)
      g=atrg(ind,4)
      nint=300
      sum1=0.d0
      sum2=0.d0
      sum3=0.d0
      do i=1,nint
        t=(float(i)-0.5)/float(nint)
        y=-1.d0+1.d0/t
        rk=y
!
!       d2=amp*amp*p_cdm(rk,gams,sig8)
!
        alnkl=log(rk)
        k_=rk
        d2=fourpi*xnorm8*(k_/ko)**(3+xn+0.5*alphas*
     2    (alnkl-lnko))*t2(alnkl)*g*g/g0/g0
         x=y*r
         w1=exp(-x*x)
         w2=2*x*x*w1
         w3=4*x*x*(1-x*x)*w1
         sum1=sum1+w1*d2/y/t/t
         sum2=sum2+w2*d2/y/t/t
         sum3=sum3+w3*d2/y/t/t
      enddo
      sum1=sum1/float(nint)
      sum2=sum2/float(nint)
      sum3=sum3/float(nint)
      sig=sqrt(sum1)
      d1=-sum2/sum1
      d2=-sum2*sum2/sum1/sum1 - sum3/sum1
      return
      end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! halo model nonlinear fitting formula as described in
! Appendix C of Smith et al. (2002)
! updated according to 1208.2701v2 by Ji Yao

      subroutine halofit2(rk,rn,rncur,rknl,plin,pnl,pq,ph,a_hf)

      implicit double precision  (a-h,o-z)
!      implicit none

      double precision  gam,a,amod,b,c,xmu,xnu,alpha,beta,f1,f2,f3,f4
      double precision  rk,rn,plin,pnl,pq,ph,a_hf
!      double precision  om_m,om_v,om_b,h,p_index,gams,sig8,amp
      double precision  rknl,y,rncur,p_cdm
      double precision  f1a,f2a,f3a,f4a,f1b,f2b,f3b,f4b,frac
	  double precision wQ,Qz
	  integer typ
        common /cosmos/ omegam,omegav,omegah,h0,xn,alphas,
     2                  z0,wQ0,wQ1,ispect,typ,ibn,alphan,ng
!     common/cospar/om_m,om_v,om_b,h,p_index,gams,sig8,amp
	  wQ=wQ0+wQ1*(1.-a_hf)
      Qz=a_hf**(-3.*(1.+wQ0+wQ1))*exp(-3.*wQ1*(1.-a_hf))
!      gam=0.86485+0.2989*rn+0.1631*rncur
	  gam=0.1971-0.0843*rn+0.8460*rncur !new
!      a=1.4861+1.83693*rn+1.67618*rn*rn+0.7940*rn*rn*rn+
!     &0.1670756*rn*rn*rn*rn-0.620695*rncur
      a=1.5222+2.8553*rn+2.3706*rn**2+0.9903*rn**3+
     &0.2250*rn**4-0.6038*rncur+0.1749*omegav*Qz*(1+wQ)
      a=10**a
!      b=10**(0.9463+0.9466*rn+0.3084*rn*rn-0.940*rncur)
      b=10**(-0.5642+0.5864*rn+0.5716*rn**2-1.5474*rncur+
     &0.2279*omegav*Qz*(1+wQ))
!      c=10**(-0.2807+0.6669*rn+0.3214*rn*rn-0.0793*rncur)
	  c=10**(0.3698+2.0404*rn+0.8161*rn**2+0.5869*rncur)
!      xmu=10**(-3.54419+0.19086*rn)
	  xmu=0
!      xnu=10**(0.95897+1.2857*rn)
	  xnu=10**(5.2105+3.6902*rn)
!      alpha=1.38848+0.3701*rn-0.1452*rn*rn
	  alpha=abs(6.0835+1.3373*rn-0.1959*rn**2-5.5274*rncur)
!      beta=0.8291+0.9854*rn+0.3400*rn**2
      beta=2.0379-0.7354*rn+0.3157*rn**2+1.2490*rn**3+
     &0.3980*rn**4-0.1682*rncur

      if(abs(1-omegam).gt.0.01) then ! omega evolution
         f1a=omegam**(-0.0732)
         f2a=omegam**(-0.1423)
         f3a=omegam**(0.0725)
         f1b=omegam**(-0.0307)
         f2b=omegam**(-0.0585)
         f3b=omegam**(0.0743)
         frac=omegav/(1.-omegam)
         f1=frac*f1b + (1-frac)*f1a
         f2=frac*f2b + (1-frac)*f2a
         f3=frac*f3b + (1-frac)*f3a
      else
         f1=1.0
         f2=1.
         f3=1.
      endif
	  f1=omegam**(-0.0307)
	  f2=omegam**(-0.0585)
	  f3=omegam**(0.0743)

      y=(rk/rknl)

      ph=a*y**(f1*3)/(1+b*y**(f2)+(f3*c*y)**(3-gam))
      ph=ph/(1+xmu*y**(-1)+xnu*y**(-2))
      pq=plin*(1+plin)**beta/(1+plin*alpha)*exp(-y/4.0-y**2/8.0)
      pnl=pq+ph
!	write(*,*)'halofit: ',pq,ph,pnl
      return
      end

!%%%%%this is the old mapping procedure PD96 %%%%%%%%%%%%%%%%%%

       function fnl(x,g,aneff)
        implicit double precision  (a-h,o-z)
! nonlinear mapping from Peacock and Dodds
        dum=log(1.0+aneff/3.0)
        a=0.482*exp(-0.947*dum)
        b=0.226*exp(-1.778*dum)
        alpha=3.310*exp(-0.244*dum)
        beta=0.862*exp(-0.287*dum)
        v=11.55*exp(-0.423*dum)
        dum1=1.+b*beta*x+exp(alpha*beta*log(a*x))
        dum2=1.+exp(beta*(alpha*log(a*x)+3*log(g)-
     2  log(v)-0.5*log(x)))
        fnl=x*exp(log(dum1/dum2)/beta)
       return
       end
!
!==========  Mo et al   ==========================================
!
       function fnl0(x)
        implicit double precision  (a-h,o-z)
! nonlinear mapping from Mo et al. for omega=0 case
	dum1=1+2*x*x-0.6*x*x*x-1.5*exp(3.5*log(x))+x*x*x*x
	dum2=1+0.0037*x*x*x
	fnl0=x*sqrt(dum1/dum2)
       return
       end
!


!===============================================================
!      Photo-z redshift distribution converted to true redshift for calculations.
!      - troxel

        function dndz(zw,z0,zmin,norm,zbin,Dz,pzrms)

        implicit none
        integer, parameter   :: dp=kind(0.d0),nn=75
        real (kind=dp)           :: pdf,zw,z0,dndz,z,zmin,norm
        real (kind=dp)           :: dum1(nn),zstep,pi,Dz,pzrms,zbin
        integer                     :: i
        if (zw.lt.zmin-3.0*pzrms*(1.+zmin+zbin/2.0)) then
          dndz=0
          return
        else if (zw.gt.zmin+zbin+3.0*pzrms*(1.+zmin+zbin/2.0)) then
          dndz=0
          return
        else
         pi=3.1415926535898d0
         zstep=zbin/nn
         do i=1,nn
          z=zmin+zstep*i
          pdf=exp(-(zw-z-Dz)**2.0/2.0/(pzrms*(1.+zw))**2)
     .        /sqrt(2.0*pi)/pzrms/(1.+zw)
          dum1(i)=pdf*zw*zw/2.0/z0/z0/z0*exp(-(zw/z0)**1.)/norm
         end do
         call integ(dum1,dndz,nn)
         dndz=dndz*zstep
        end if
        return
        end function
!===============================================================


!=================================================================
!
!      computes angular power spectrum as a function of kappa
!      do the time integration at a fixed number of points
!      using preevaluated nonlinear power spectrum; interpolate
!      to get the ps at a given k
!
!      The source redshift distribution is added here
!
       function psk(xkappa,nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins)
        implicit double precision  (a-h,o-z)
        parameter (ntau=50,nknl=21,fourpi=4.0d0*3.1415926535898d0)
        parameter (ntau2=50)
        parameter (tol=1.0d-5)
        dimension dum1(ntau),wz(ntau),wz2(ntau),dum2(ntau),dum3(ntau),
     2     dum4(ntau)
        dimension atrg(ntau,5)
        integer nbin1,nbin2,nbins
        double precision eta2pst,Wij,Delta_inv,psknum,Q
        dimension delta2nl(nknl,ntau),alnknl(nknl,ntau),d2nlp(nknl,ntau)
        double precision  nzw,zw,nzw2,Pz,ps,nz1,ng,wa,wb,Dz,pzrms,binsize
        double precision nza,nzb
		integer typ
        common /cosmos/ omegam,omegav,omegah,h0,xn,alphas,
     2                  z00,wQ0,wQ1,ispect,typ,ibn,alphan,ng
        common /idum/ ilin
        common /cosmo2/ curv,tau0,r0,xnorm8
        double precision ko, k_, lnko,nz,dzk,nnz,norma,normb
        common /pivot/ ko, k_, lnko
        common /par/ tau1,r1,theta,atrg
        common /nlps/ delta2nl,alnknl,d2nlp
        common /temp/itheta
        common /SC/ Wij,Delta_inv,Q
		common /IA/ Agp,alphagp,Amp0
		common /passing/ akp
        external func1
		external fP_dI ! P_delta,I
        external dndz
        external eta2p
!
       ko=0.05
       hc=2.998d5/h0
       pi=3.1415926535898d0
       g0=atrg(ntau,4)
       i1=0
       nnz=0

       binsize=(zmax-zmin)/nbins
       zstep=(zmax-zmin)/ntau
       do i=1,ntau
        zw=zmin+zstep*i
!        dum1(i)=zw*zw/2./z0/z0/z0*exp(-zw/z0)
		dum1(i)=dndz(zw,z0,((nbin1-1)*binsize),1.0d0,binsize,Dz,pzrms)
       end do
       call integ(dum1,norma,ntau)
       norma=norma*zstep
       do i=1,ntau
        zw=zmin+zstep*i
!        dum1(i)=zw*zw/2./z0/z0/z0*exp(-zw/z0)
		dum1(i)=dndz(zw,z0,((nbin2-1)*binsize),1.0d0,binsize,Dz,pzrms)
       end do
       call integ(dum1,normb,ntau)
       normb=normb*zstep


!
!  Big loop starts here: for integration over z
!
       do i=1,ntau-1
        tau=atrg(i,1)
        a=atrg(i,2)
        r=atrg(i,3)
        g=atrg(i,4)
        if(ibn.eq.0) then
          bn=atrg(i,5)
        else
          bn=1.d0
        endif
        ak=xkappa/r
        alnk=log(ak)
        k_=ak
!        print *, a

!
!  Redshift distribution and Tomography
!
!
        zw=0.0
        z1=1./a-1.
!        print *, z1,i
!        zmax=3.5
        wz(1)=0.0
        zstep=(zmax-z1)/(ntau-1)

        if (typ.eq.1 .or. typ.eq.2 .or. typ.eq.5 .or.typ.gt.8) then
         if (z1.le.(nbin1*binsize)) then
          do j=2,ntau
           if (z1.ge.((nbin1-1)*binsize)) then
            zstep=(zmax-z1)/(ntau-1)
            zw=z1+zstep*(j-1)
           else
            zstep=(zmax-zmin)/(ntau-1)
            zw=zmin+zstep*(j-1)
           end if
           aw=1.0/(1.0+zw)
           tauw=hc*rombint(func1,a0,aw,tol)
           rw=tau0-tauw
           nzw=dndz(zw,z0,((nbin1-1)*binsize),norma,binsize,Dz,pzrms)
           wz(j)=nzw*(1.-r/rw)*r
!!           if (1.eq.0) print *, rw,r,nzw ! Needed to prevent compiler bug. Never triggered.
          end do
        end if
        end if
!
        if (typ.lt.7.or.typ.gt.8) then
         if (z1.le.(nbin1*binsize)) then
          call integ(wz,wa,ntau)
          wa=wa*zstep
         else
          wa=0.
         end if
        end if

        if (typ.eq.1 .or. typ.eq.2 .or. typ.eq.5 .or.typ.gt.8) then
         if (z1.le.(nbin2*binsize)) then
          do j=2,ntau
           if (z1.ge.((nbin2-1)*binsize)) then
            zstep=(zmax-z1)/(ntau-1)
            zw=z1+zstep*(j-1)
           else
            zstep=(zmax-zmin)/(ntau-1)
            zw=zmin+zstep*(j-1)
           end if
           aw=1.0/(1.0+zw)
           tauw=hc*rombint(func1,a0,aw,tol)
           rw=tau0-tauw
           nzw=dndz(zw,z0,((nbin2-1)*binsize),normb,binsize,Dz,pzrms)
           wz(j)=nzw*(1.-r/rw)*r
!!           if (1.eq.0) print *, rw,r,nzw ! Needed to prevent compiler bug. Never triggered.
          end do
        end if
        end if
!
        if (typ.lt.7.or.typ.gt.8) then
         if (z1.le.(nbin2*binsize)) then
          call integ(wz,wb,ntau)
          wb=wb*zstep
         else
          wb=0.
         end if
        end if

        if (alnk.lt.alnknl(1,i)) then
         pprim=fourpi*xnorm8*(k_/ko)**(xn-1.+0.5*alphas
     .         *(alnk-lnko))*t2(alnk)*g*g/g0/g0/ko**4*2*pi**2!/r/r
        else
           if (ilin.eq.0) then
! linear theory next 2 lines
         pprim=fourpi*xnorm8*(k_/ko)**(xn-1.+0.5*alphas
     .         *(alnk-lnko))*t2(alnk)*g*g/g0/g0/ko**4*2*pi**2!/r/r
           else
! nonlinear theory next 4 lines
           call splint(alnknl(1,i),delta2nl(1,i),d2nlp(1,i),
     2     nknl,alnk,delta2)
!        write(20,*)exp(alnk), delta2
           pprim=exp(delta2)*(k_)**(-4.)*2*pi**2!/r/r
            endif
        endif
!		write(*,'(5E13.4)') r,pprim*ak,ak,1./a-1.

      fsky=0.436!0.25
      gamrms=0.26**2.!0.04
      scl=5.0d-14*(omegah/omegam)**(-2) !8.46406d-17 ! 5.0d-14 (bridle & king)
      rho=omegam*3.0/8.0*h0**2/pi/4.3d-9 !omegam*hc*hc*3./8./pi/4.3d-9 ! Mpc/M_s (km/s)**2
!       if (z1.gt.0.15.and.z1.lt.0.35) then
!         nz=(z1/z0)**(alphan-1)*exp(-z1*z1/z0/z0/2)/0.37449
!        else
!         nzw=0.0
!        end if
!      nz=z1*z1*0.501388543283575/z0/z0/z0*exp(-z1/z0)
!        nz=z1*z1/z0/z0/z0*exp(-z1/z0)/1.52379
!      nz=z1*z1*exp(-(1.4*z1/z0)**(1.5))/0.0303693
       nza=dndz(z1,z0,((nbin1-1)*binsize),norma,binsize,Dz,pzrms)
       nzb=dndz(z1,z0,((nbin2-1)*binsize),normb,binsize,Dz,pzrms)
!      print *, nz
!      dzchi=dsqrt(omegam/a/a/a+omegav+(1-omegam-omegav)/a/a)
!     .      /hc
      dzchi=1.0/a**2/func1(a)/hc
!      print *, dzchi
      wwa=3.0/2.0/hc/hc/a*omegam*wa
      wwb=3.0/2.0/hc/hc/a*omegam*wb
      ww2=3.0/2.0/hc/hc/a*omegam*r/a*(1.d0-r/tau0)
!      print *, 'ww2',w,r/a*(1.d0-r/tau0)
      wga=bn*nza*dzchi
!	  print *, 'bn=',bn,'  nza=',nza,'  dzchi=',dzchi
      wia=scl*rho*g0/g*nza*dzchi !scl*rho*g0/g/a*nza*dzchi
      wgb=bn*nzb*dzchi
      wib=scl*rho*g0/g*nzb*dzchi !scl*rho*g0/g/a*nzb*dzchi

!      print *, nz
!      nnz=nnz+nz!6.6d8
!      print *, nnz

!     Types: 1=GG, 2=GI, 3=II, 4=gg, 5=gG, 6=gI, 7=GG,N, 8=gg,N

      if (typ .eq. 1) then
        pref=wwa*wwb
      else if (typ .eq. 2) then
        pref=wwa*wib
		pref=wia*wwb*Amp0 ! ^IG_ij instead of ^GI_ij
      else if (typ .eq. 3) then
        pref=wia*wib*Amp0**2
      else if (typ .eq. 4) then
        pref=wga*wgb
      else if (typ .eq. 5) then
        pref=wwa*wgb
		pref=wga*wwb ! ^Gg_ij instead of ^gG_ij
      else if (typ .eq. 6) then
        pref=wia*wgb
		pref=wga*wib*Amp0 ! ^Ig_ij instead of ^gI_ij
      else if (typ .eq. 7) then
!        print *, ng, gamrms
        !psk=gamrms/(ng*(60*180/pi)**2*fourpi*fsky)
        pref=nza*dzchi
!        write(*,*)nza
        !return
      else if (typ .eq. 8) then
!        print *, a
        !psk=1/(ng*(180*60/pi)**2*fourpi*fsky)
        pref=nza*dzchi
        !return
      else if (typ .eq. 9) then
        pref=ww2a*wwb
      else if (typ .eq. 10) then
        pref=ww2a*wib
!        print *, ww2,wi
	  else if (typ .eq. 21) then ! IA power law correlation model IGij spec
	    pref=nza*wwb/r/r
	  else if (typ .eq. 22) then ! IA Amplitude
		pref=wia*wwb*Amp0
      end if
!      print *, pref,pprim,ak
!		print *, '~~~',Amp0,wia,wwb,pref

      if (typ .lt. 7 .or. ((typ.gt.8) .and. (typ .ne. 21))) then
        dum1(i)=pref*pprim*ak/r/r!pref*pprim*ak ! pprim linear/nonlinear matter spectra /r^2
        dum2(i)=nza*wwb*dzchi !wga*wwb ! Wij
        dum3(i)=wga*nza*dzchi ! Delta^-1
        eta2pst=eta2p(z1,zmin+binsize*(nbin1-1),1d0,Dz,pzrms,z0)
        dum4(i)=pref*pprim*ak*eta2pst/r/r !pref*pprim*ak*eta2pst ! for C_Gg numerator
!		print *, wga,wwb,nza,eta2pst
!        print *, dum1(i),dum4(i)
	  else if (typ .eq. 21) then
		akp=ak
		dum1(i)=rombint(fP_dI,0,60*omegam/omegah,tol)
!		print *, '---',ak,dum1(i)
		dum1(i)=dum1(i)*pref
      else
        dum1(i)=pref ! for integral to get n_i
!        write(*,*) pref
      end if
!      print *, z1
       end do
!
!   Big loop ends here
!
! integrate in equal time intervals

      dtau1=(tau0-tau1)/ntau

	  if (typ .lt. 7 .or. (typ.gt.8 .and. typ .ne. 21)) then
	   call integ(dum1,psk,ntau-1)
       call integ(dum2,Wij,ntau-1)
       call integ(dum3,Delta_inv,ntau-1)
       call integ(dum4,psknum,ntau-1)

       psk=psk*dtau1
       Wij=Wij*dtau1
       Delta_inv=Delta_inv*dtau1
       psknum=psknum*dtau1
        Q=psknum/psk
!        print *, Wij,Delta_inv,psknum,psk,Q

	   else if (typ .eq. 21) then
		call integ(dum1,psk,ntau-1)
		psk=psk*dtau1

       else if (typ .eq. 7) then
        call integ(dum1,pref,ntau-1)
        psk=gamrms/(ng*pref*dtau1*(60*180/pi)**2*fourpi*fsky)
       else if (typ .eq. 8) then
        call integ(dum1,pref,ntau-1)
        psk=1/(ng*pref*dtau1*(60*180/pi)**2*fourpi*fsky)
       endif

       return
       end


!
!=================================================================
!
!      computes GGI and GII as a function of xkappa by parameter
!      as fitted by Sebboloni et al.
!
!      The source redshift distribution is added here
!
       function bsk(xkappa)
        implicit double precision  (a-h,o-z)
        parameter (ntau=50,nknl=21,fourpi=4.0d0*3.1415926535898d0)
        parameter (ntau2=50)
        parameter (tol=1.0d-5)
        dimension dum1(ntau),wz(ntau),wz2(ntau)
        dimension atrg(ntau,5)
        dimension delta2nl(nknl,ntau),alnknl(nknl,ntau),d2nlp(nknl,ntau)
        double precision  nzw,zw,nzw2,Pz,ps,nz1
		integer typ
        common /cosmos/ omegam,omegav,omegah,h0,xn,alphas,
     2                  z0,wQ0,wQ1,ispect,typ,ibn,alphan,ng
        common /idum/ ilin
        common /cosmo2/ curv,tau0,r0,xnorm8
        double precision ko, k_, lnko,nz,dzk,nnz
        common /pivot/ ko, k_, lnko
        common /par/ tau1,r1,theta,atrg
        common /nlps/ delta2nl,alnknl,d2nlp
        common /temp/itheta
        external func1
!
       ko=0.05
       hc=2.998d5/h0
       pi=3.1415926535898d0
       g0=atrg(ntau,4)
       i1=0
       nnz=0
!
!  Big loop starts here: for integration over z
!
       do 10 i=1,ntau-1
        tau=atrg(i,1)
        a=atrg(i,2)
        r=atrg(i,3)
        g=atrg(i,4)
        if(ibn.eq.0) then
          bn=atrg(i,5)
        else
          bn=1.0d0
        endif
        ak=xkappa/r
        alnk=log(ak)
        k_=ak
!        print *, a

!
!  Redshift distribution and Tomography
!
!
        zw=0.0
        z1=1./a-1.
!        print *, z1,i
        zmax=5.0
        wz(1)=0.0
        zstep=(zmax-z1)/(ntau-1)

!
!  Small loop starts here: redshift distribution
!  and tomography
!
        do j=2,ntau
!        print *, z1, zw, j, i

        zw=z1+zstep*(j-1)
        aw=1.0/(1.0+zw)
        tauw=hc*rombint(func1,a0,aw,tol)
         if (curv.eq.0.) then
          rw=tau0-tauw
         else if (curv.gt.0.) then
          rw=sin(sqrt(curv)*(tau0-tauw))/sqrt(curv)
         else
          rw=sinh(sqrt(-curv)*(tau0-tauw))/sqrt(-curv)
         endif
!
!       nzw=zw*zw*0.5/z0/z0/z0*exp(-zw/z0)
!
! for better precision we replace the infinite
! upper boundary by zmax=5 for the normalization
!
!       nzw=zw*zw*4.01110834626860*exp(-zw/z0)
!  or simply
!

!         nzw=zw*zw*exp(-(1.4*zw/z0)**(1.5))/0.0303693
!       0.374502
!        if (zw.gt.0.15.and.zw.lt.0.35) then
         nzw=(zw/z0)**(alphan-1)*exp(-zw*zw/z0/z0/2)/0.370379*0.03
!        else
!         nzw=0.0
!        end if
!        nzw=zw*zw*0.501388543283575/z0/z0/z0*exp(-zw/z0)
!        print *, zw
!
        w1=sqrt(1.-curv*r*r)-sqrt(1.-curv*rw*rw)*r/rw
        wz(j)=nzw*w1
!
!   Small loop ends here
!
        enddo
!
          call integ(wz,w,ntau)
          w=w*zstep

        rstep=r/(ntau-1)
        do j=2,ntau

        rw=rstep*(j-1)
        aw=1.0/(1.0+zw)
        tauw=hc*rombint(func1,a0,aw,tol)
         if (curv.eq.0.) then
          rw=tau0-tauw
         else if (curv.gt.0.) then
          rw=sin(sqrt(curv)*(tau0-tauw))/sqrt(curv)
         else
          rw=sinh(sqrt(-curv)*(tau0-tauw))/sqrt(-curv)
         endif
!
!       nzw=zw*zw*0.5/z0/z0/z0*exp(-zw/z0)
!
! for better precision we replace the infinite
! upper boundary by zmax=5 for the normalization
!
!       nzw=zw*zw*4.01110834626860*exp(-zw/z0)
!  or simply
!

!         nzw=zw*zw*exp(-(1.4*zw/z0)**(1.5))/0.0303693
!       0.374502
!        if (zw.gt.0.15.and.zw.lt.0.35) then
         nzw=(zw/z0)**(alphan-1)*exp(-zw*zw/z0/z0/2)/0.370379*0.03
!        else
!         nzw=0.0
!        end if
!        nzw=zw*zw*0.501388543283575/z0/z0/z0*exp(-zw/z0)
!        print *, zw
!
        w1=sqrt(1.-curv*r*r)-sqrt(1.-curv*rw*rw)*r/rw
        wz(j)=nzw*w1
!
!   Small loop ends here
!
        enddo
!        end if
!
          call integ(wz,w,ntau)
          w=w*zstep


      fsky=0.436!0.25
      gamrms=0.26**2!0.04
      scl=3.1232799550643746d-20 ! 5.0d-14 (bridle & king)
      rho=omegam*hc*hc*3./8./pi/4.3d-6 ! Mpc/M_s (km/s)**2
!       if (z1.gt.0.15.and.z1.lt.0.35) then
         nz=(z1/z0)**(alphan-1)*exp(-z1*z1/z0/z0/2)/0.370379*0.03
!        else
!         nzw=0.0
!        end if
!      nz=z1*z1*0.501388543283575/z0/z0/z0*exp(-z1/z0)
!      nz=z1*z1*exp(-(1.4*z1/z0)**(1.5))/0.0303693
!      print *, nz
!      dzchi=dsqrt(omegam/a/a/a+omegav+(1-omegam-omegav)/a/a)
!     .      /hc
      dzchi=1./a**2/func1(a)
!      print *, dzchi
      ww=3./2./hc/hc/a*omegam*w
      wg=bn*nz*dzchi
      wi=scl*rho*g0/g/a*nz

!      print *, nz
      nnz=nnz+nz!6.6d8

!     Types: 1=GG, 2=GI, 3=II, 4=gg, 5=gG, 6=gI, 7=GG,N, 8=gg,N

      if (typ .eq. 1) then
        pref=ww*ww
      else if (typ .eq. 2) then
        pref=ww*wi
      else if (typ .eq. 3) then
        pref=wi*wi
      else if (typ .eq. 4) then
        pref=wg*wg
      else if (typ .eq. 5) then
        pref=ww*wg
      else if (typ .eq. 6) then
        pref=wi*wg
      else if (typ .eq. 7) then
        pref=fourpi*fsky*gamrms
      else if (typ .eq. 8) then
!        print *, a
        pref=fourpi*fsky
      end if

      if (typ .lt. 7) then
        dum1(i)=pref*pprim*ak
      end if
!      print *, z1
 10     continue
!
!   Big loop ends here
!
! integrate in equal time intervals
       if (typ.gt.6) then
         psk=pref/nnz/4.6656d8*fourpi
!         print *, pref, nnz*4.6656d8/fourpi
         return
       end if
       call integ(dum1,psk,ntau-1)
       dtau1=(tau0-tau1)/ntau
       psk=psk*dtau1
       end

!
!=================================================================
!
!      computes integrand of sigma as a function of theta
!      do the time integration at a fixed number of points
!      using preevaluated nonlinear power spectrum; interpolate
!      to get the ps at a given k
!
!
       function p2(alnk)
        implicit double precision  (a-h,o-z)
        parameter (ntau=50,nknl=21,fourpi=4.0d0*3.1415926535898d0)
        dimension dum1(ntau),dum2(ntau)
        dimension atrg(ntau,5)
        dimension delta2nl(nknl,ntau),alnknl(nknl,ntau),d2nlp(nknl,ntau)
		integer typ
        common /cosmos/ omegam,omegav,omegah,h0,xn,alphas,
     2                  z0,wQ0,wQ1,ispect,typ,ibn,alphan,ng
        double precision ko, k_, lnko
        common /pivot/ ko, k_, lnko
        common /idum/ ilin
        common /cosmo2/ curv,tau0,r0,xnorm8
        common /par/ tau1,r1,theta,atrg
        common /nlps/ delta2nl,alnknl,d2nlp
        common /lens/ cpp,cppint
        common /temp/itheta
!
       hc=2.998d5/h0
       ak=exp(alnk)
       g0=atrg(ntau,4)
       i1=0
       dtau1=(tau0-tau1)/ntau
       do 10 i=1,ntau-1
        tau=atrg(i,1)
        a=atrg(i,2)
        r=atrg(i,3)
        g=atrg(i,4)
        x=ak*r*theta
        k_=ak
        if (alnk.lt.alnknl(1,i)) then
           ps=fourpi*9./4.*xnorm8*
     2     (k_/ko)**(xn-1.+0.5*alphas*(alnk-lnko))
     3     *t2(alnk)*g*g/g0/g0/hc/hc/hc/hc/a/a*omegam*omegam
        else
           if (ilin.eq.0) then
! linear theory next 2 lines
           ps=fourpi*9./4.*xnorm8*
     2     (k_/ko)**(xn-1.+0.5*alphas*(alnk-lnko))
     3     *t2(alnk) *g*g/g0/g0/hc/hc/hc/hc/a/a*omegam*omegam
           else
! nonlinear theory next 4 lines
           call splint(alnknl(1,i),delta2nl(1,i),d2nlp(1,i),
     2     nknl,alnk,delta2)
           ps=9./4.*exp(delta2)*(k_)**(-4.)/hc/hc/hc/hc/a/a
     2     *omegam*omegam
            endif
        endif
        if (x.lt.0.1) then
         aj0=1.0
         aj1=1.0
        else
        aj0=bessj0(x)
        aj1=2.0*bessj1(x)/x
        endif
         w=sqrt(1.-curv*r*r)-sqrt(1.-curv*r1*r1)*r/r1
! for Cpp
         dum1(i)=ps*ak*ak*ak*w*w*r*r*aj0
! for p
         dum2(i)=ps*ak*ak*ak*w*w*r*r*aj1*aj1
10     continue
! integrate in equal time intervals
       call integ(dum1,cpp,ntau-1)
       call integ(dum2,p2,ntau-1)
       p2=p2*dtau1
       cpp=cpp*dtau1
       return
       end
!
!=================================================================
! comoving distance
       function func1(a)
        implicit double precision  (a-h,o-z)
		integer typ
        common /cosmos/ omegam,omegav,omegah,h0,xn,alphas,
     2                  z0,wQ0,wQ1,ispect,typ,ibn,alphan,ng
!      integrates the a(tau) relation
        parameter (tcmb=2.726,nnur=3)
        parameter (grhog=1.4952d-13*tcmb**4)
        parameter (grhor=3.3957d-14*tcmb**4)
        double precision Qz
        grhom=3.3379d-11*h0*h0
        aeq=(grhog+grhor*nnur)/grhom
!
!  Quintessence:
!  we simply add here the wQ dependence to integrate a(tau)
!  including including a cosmological constant case wQ=-1
!
      if (a .ne. 0) then
         wQ=wQ0+wQ1*(1.-a)
         Qz=a**(-3.*(1.+wQ0+wQ1))*exp(-3.*wQ1*(1.-a))
      endif

        func1=1./sqrt(a*omegam+aeq+a*a*(1.-omegam-omegav)+
     2  omegav*a**4*Qz)
!  see Weinberg Cosmology P 105-106
       return
       end

!
! ================================================================
!

       function fP_dI(rp)
        implicit double precision  (a-h,o-z)
		common /IA/ Agp,alphagp
		common /passing/ akp
        parameter (bg=1) ! linear bias
		parameter (pi=3.1415926535898d0)

        fP_dI=2*pi/bg*Agp*rp**alphagp*bessel_jn(2,akp*rp)*rp
       return
       end

!
!=================================================================
!
        function dnorm8(alnk)
        implicit double precision (a-h,o-z)
!  This function calculates the integrand for the normalization of the
!  power spectrum with Delta = 1 at r = 8 Mpc/h.
!
        integer typ
		common /cosmos/ omegam,omegav,omegah,h0,xn,alphas,
     2                  z0,wQ0,wQ1,ispect,typ,ibn,alphan,ng
        double precision ko, k_, lnko
        common /pivot/ ko, k_, lnko
!
        sigr0=8.0d0
        ak=exp(alnk)
        k_=ak
        p=(k_/ko)**(xn+3.+0.5*alphas*(alnk-lnko))
        if (ak.le.0.0d0) then
          dnorm8=0.0d0
          return
        end if
!  Window function for spherical tophat of radius 8 Mpc/h.
        x=ak*sigr0
        w=3.0*(sin(x)-x*cos(x))/(x*x*x)
        dnorm8=t2(alnk)*w*w*p
!        write(10,*)ak,dnorm8,w*w
        return
        end

        function dnormk(alnk)
        implicit double precision (a-h,o-z)
!  This function calculates the integrand for the normalization of the
!  power spectrum with Delta = 1 at r = kinv Mpc/h.
!
        integer typ
		common /cosmos/ omegam,omegav,omegah,h0,xn,alphas,
     2                  z0,wQ0,wQ1,ispect,typ,ibn,alphan,ng
        common /sigk/ sigr0
        double precision ko, k_, lnko
        common /pivot/ ko, k_, lnko
        ak=exp(alnk)
        k_=ak
        p=(k_/ko)**(xn+3.+0.5*alphas*(alnk-lnko))
        if (ak.le.0.0d0) then
          dnormk=0.0d0
          return
        end if
!  Window function for spherical tophat of radius kinv Mpc/h.
        x=ak*sigr0
        w=3.0*(sin(x)-x*cos(x))/(x*x*x)
        dnormk=t2(alnk)*w*w*p
!        write(10,*)ak,dnormk
        return
        end
!
!=================================================================
!
        function t2(alnk)
!  t evaluates the transfer function at wavenumber ak.
!
        implicit double precision (a-h,o-z)
		integer typ
		double precision omegab
		common omegab
        common /cosmos/ omegam,omegav,omegah,h0,xn,alphas,
     2                  z0,wQ0,wQ1,ispect,typ,ibn,alphan,ng
        parameter (kmax=5000)
        dimension akk(kmax),tf(kmax)
        common /peacock/ delta10,xm,alnkc
        common /tfun/ akk,tf,k0
        double precision ko, k_, lnko
        common /pivot/ ko, k_, lnko
        ak=exp(alnk)
        if (ispect.eq.1) then
! Transfer function for cold dark matter (from EBW).
        q=ak/omegah
        a1=6.4*q
        a2=3.0*q
        a3=1.7*q
        nu=1.13
        t=exp(-log(1.0+exp(log(a1+a2*sqrt(a2)+a3*a3))*nu)/nu)
        t2=t*t
        return
        endif
!
!	This is the part used in our work. MI.
!
        if (ispect.eq.3) then
!  Transfer function for cold dark matter (from BBKS).
        q=ak/omegah/exp(-omegab*(1+sqrt(2.*omegah/omegam)/omegam))
		! updated by Ji Yao according to ScalPy
        a1=2.34*q
        a2=3.89*q
        a3=16.1*q
        a4=5.46*q
        a5=6.71*q
        t=1.0+a2+a3*a3+a4*a4*a4+a5*a5*a5*a5
        t=log(1.0+a1)/a1/sqrt(sqrt(t))
        t2=t*t
        return
        endif

        if (ispect.eq.0) then
!       interpolate to get tf
        if (ak.lt.akk(1)) then
        t2=1.
        return
        endif
        imin=1
        imax=2
        do 10 i=1,k0-1
        if (ak.gt.akk(i)) imin=i
        if (ak.gt.akk(i)) imax=i+1
10      continue
        t2=exp(log(tf(imin))+(log(ak)-log(akk(imin)))/(log(akk(imax))
     <-log(akk(imin)))*(log(tf(imax))-log(tf(imin))))
        return
        endif
        if (ispect.eq.2) then
! a simple PS
         t2=1.0/(1.0+exp((-xm+xn+0.5*alphas*(alnk-lnko))*
     2   (alnk-alnkc)))
         return
         endif
         t2=1.
         return
        end
!
!=================================================================
!
        function rombint(f,a,b,tol)
!  Rombint returns the integral from a to b of f(x)dx using Romberg
!  The method converges provided that f(x) is continuous in (a,b).  The
!  f must be double precision and must be declared external in the calling
!  routine.  tol indicates the desired relative accuracy in the integral.
!
        implicit double precision (a-h,o-z)
        common /lens/ cpp,cppint
        parameter (maxiter=20,maxj=5)
        dimension g(maxj+1),c(maxj+1)
        external f
!
        h=0.5d0*(b-a)
        fa=f(a)
        ca=cpp
        fb=f(b)
        cb=cpp
        gmax=h*(fa+fb)
        cmax=h*(ca+cb)
        g(1)=gmax
        c(1)=cmax
        nint=1
        error=1.0d20
        i=0
10        i=i+1
          if (i.gt.maxiter.or.(i.gt.5.and.abs(error).lt.tol))
     2      go to 40
!  Calculate next trapezoidal rule approximation to integral.
          g0=0.0d0
          c0=0.0d0
            do 20 k=1,nint
            g0=g0+f(a+(k+k-1)*h)
            c0=c0+cpp
20        continue
          g0=0.5d0*g(1)+h*g0
          c0=0.5d0*c(1)+h*c0
          h=0.5d0*h
          nint=nint+nint
          jmax=min(i,maxj)
          fourj=1.0d0
            do 30 j=1,jmax
!  Use Richardson extrapolation.
            fourj=4.0d0*fourj
            g1=g0+(g0-g(j))/(fourj-1.0d0)
            c1=c0+(c0-c(j))/(fourj-1.0d0)
            g(j)=g0
            g0=g1
            c(j)=c0
            c0=c1
30        continue
          if (abs(g0).gt.tol) then
            error=1.0d0-gmax/g0
          else
            error=gmax
          end if
          gmax=g0
          g(jmax+1)=g0
          cmax=c0
          c(jmax+1)=c0
        go to 10
40      rombint=g0
        cppint=c0
        if (i.gt.maxiter.and.abs(error).gt.tol)
     2    write(*,*) 'Rombint failed to converge; integral, error=',
     3    rombint,error
        return
        end
!
!=================================================================
!
      SUBROUTINE spline(x,y,n,yp1,ypn,y2)
      INTEGER n,NMAX
      DOUBLE PRECISION yp1,ypn,x(n),y(n),y2(n)
      PARAMETER (NMAX=1000)
      INTEGER i,k
      DOUBLE PRECISION p,qn,sig,un,u(NMAX)
      if (yp1.gt..99d30) then
        y2(1)=0.d0
        u(1)=0.d0
      else
        y2(1)=-0.5d0
        u(1)=(3.d0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.d0
        y2(i)=(sig-1.d0)/p
        u(i)=(6.d0*((y(i+1)-y(i))/(x(i+
     *1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*
     *u(i-1))/p
11    continue
      if (ypn.gt..99d30) then
        qn=0.d0
        un=0.d0
      else
        qn=0.5d0
        un=(3.d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
!  (C) Copr. 1986-92 Numerical Recipes Software =$j*m,).
      END
!
!=================================================================
!
      SUBROUTINE splint(xa,ya,y2a,n,x,y)
      INTEGER n
      DOUBLE PRECISION x,y,xa(n),y2a(n),ya(n)
      INTEGER k,khi,klo
      DOUBLE PRECISION a,b,h
      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.d0) pause 'bad xa input in splint'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**
     *2)/6.d0
      return
!  (C) Copr. 1986-92 Numerical Recipes Software =$j*m,).
      END
!
!=================================================================
!
        subroutine integ(y,z,n)
!  integ integrates using a n-order polynomial
!
        implicit double precision (a-h,o-z)
        dimension y(n)
        z=(y(1)+y(n))*3./8.+(y(2)+y(n-1))*7./6.+(y(3)+y(n-2))*23./24.
        do 10 i=4,n-3
         z=z+y(i)
10      continue
        return
        end
!
!=================================================================
!
      FUNCTION bessj0(x)
      DOUBLE PRECISION bessj0,x
      DOUBLE PRECISION ax,xx,z
      DOUBLE PRECISION p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6,
     *s1,s2,s3,s4,s5,s6,y
      SAVE p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6,s1,s2,s3,s4,
     *s5,s6
      DATA p1,p2,p3,p4,p5/1.d0,-.1098628627d-2,.2734510407d-4,
     *-.2073370639d-5,.2093887211d-6/, q1,q2,q3,q4,q5/-.1562499995d-1,
     *.1430488765d-3,-.6911147651d-5,.7621095161d-6,-.934945152d-7/
      DATA r1,r2,r3,r4,r5,r6/57568490574.d0,-13362590354.d0,
     *651619640.7d0,-11214424.18d0,77392.33017d0,-184.9052456d0/,s1,s2,
     *s3,s4,s5,s6/57568490411.d0,1029532985.d0,9494680.718d0,
     *59272.64853d0,267.8532712d0,1.d0/
      if(abs(x).lt.8.d0)then
        y=x**2
        bessj0=(r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))/(s1+y*(s2+y*(s3+y*
     *(s4+y*(s5+y*s6)))))
      else
        ax=abs(x)
        z=8.d0/ax
        y=z**2
        xx=ax-.785398164d0
        bessj0=sqrt(.636619772d0/ax)*(cos(xx)*(p1+y*(p2+y*(p3+y*(p4+y*
     *p5))))-z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))
      endif
      return
      END
!  (C) Copr. 1986-92 Numerical Recipes Software =$j*m,).
!
!=================================================================
!
      FUNCTION GAMMLN(XX)
       implicit double precision (a-h,o-z)
      double precision COF(6),STP,HALF,ONE,FPF,X,TMP,SER
      DATA COF,STP/76.18009173D0,-86.50532033D0,24.01409822D0,
     *    -1.231739516D0,.120858003D-2,-.536382D-5,2.50662827465D0/
      DATA HALF,ONE,FPF/0.5D0,1.0D0,5.5D0/
      X=XX-ONE
      TMP=X+FPF
      TMP=(X+HALF)*LOG(TMP)-TMP
      SER=ONE
      DO 11 J=1,6
        X=X+ONE
        SER=SER+COF(J)/X
11    CONTINUE
      GAMMLN=TMP+LOG(STP*SER)
      RETURN
      END
!
!=================================================================
!
      FUNCTION bessj1(x)
      DOUBLE PRECISION bessj1,x
      DOUBLE PRECISION ax,xx,z
      DOUBLE PRECISION p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6,
     *s1,s2,s3,s4,s5,s6,y
      SAVE p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6,s1,s2,s3,s4,
     *s5,s6
      DATA r1,r2,r3,r4,r5,r6/72362614232.d0,-7895059235.d0,
     *242396853.1d0,-2972611.439d0,15704.48260d0,-30.16036606d0/,s1,s2,
     *s3,s4,s5,s6/144725228442.d0,2300535178.d0,18583304.74d0,
     *99447.43394d0,376.9991397d0,1.d0/
      DATA p1,p2,p3,p4,p5/1.d0,.183105d-2,-.3516396496d-4,
     *.2457520174d-5,-.240337019d-6/, q1,q2,q3,q4,q5/.04687499995d0,
     *-.2002690873d-3,.8449199096d-5,-.88228987d-6,.105787412d-6/
      if(abs(x).lt.8.d0)then
        y=x**2
        bessj1=x*(r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))/(s1+y*(s2+y*(s3+
     *y*(s4+y*(s5+y*s6)))))
      else
        ax=abs(x)
        z=8.d0/ax
        y=z**2
        xx=ax-2.356194491d0
        bessj1=sqrt(.636619772d0/ax)*(cos(xx)*(p1+y*(p2+y*(p3+y*(p4+y*
     *p5))))-z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))*sign(1.d0,x)
      endif
      return
      END
!  (C) Copr. 1986-92 Numerical Recipes Software =$j*m,).
!
!=================================================================
!
        SUBROUTINE derivs(x,y,dydx)

        implicit double precision (a-h,o-z)
        PARAMETER (NMAX=50)
        double precision x,dydx(NMAX),y(NMAX)
        common /cosmos6/ zEmax
		integer typ
        common /cosmos/ omegam,omegav,omegah,h0,xn,alphas,
     2                  z0,wQ0,wQ1,ispect,typ,ibn,alphan,ng
        double precision Qz,wQ
!
         wQ=wQ0+wQ1*(1.-x)
         Qz=x**(-3.*(1.+wQ0+wQ1))*exp(-3.*wQ1*(1.-x))

        X_a=omegam/(1.-omegam)/x**3./Qz

        q_a= ((7./2.)-(3./2.)*(wQ/(1.+X_a)))/x
        r_a= ((3./2.)*((1.-wQ)/(1.+X_a)))/x**2

        dydx(1)=y(2)
        dydx(2)=-q_a * y(2) - r_a * y(1)

!       write(*,*)wQ,x,X_a,q_a,r_a
        end

!-------------------------------------------------------------

      SUBROUTINE odeint(ystart,nvar,x1,x2,eps,h1,hmin,nok,nbad,derivs,
     *rkqs)
      INTEGER nbad,nok,nvar,KMAXX,MAXSTP,NMAX
      double precision eps,h1,hmin,x1,x2,ystart(nvar),TINY
      EXTERNAL derivs,rkqs
      PARAMETER (MAXSTP=10000,NMAX=50,KMAXX=200,TINY=1.e-30)
      INTEGER i,kmax,kount,nstp
      double precision dxsav,h,hdid,hnext,x,xsav
      double precision dydx(NMAX),xp(KMAXX),y(NMAX)
      double precision yp(NMAX,KMAXX),yscal(NMAX)
      COMMON /path/ kmax,kount,dxsav,xp,yp
      x=x1
      h=sign(h1,x2-x1)
      nok=0
      nbad=0
      kount=0
      do 11 i=1,nvar
        y(i)=ystart(i)
11    continue
      if (kmax.gt.0) xsav=x-2.*dxsav
      do 16 nstp=1,MAXSTP
        call derivs(x,y,dydx)
        do 12 i=1,nvar
          yscal(i)=abs(y(i))+abs(h*dydx(i))+TINY
12      continue
        if(kmax.gt.0)then
          if(abs(x-xsav).gt.abs(dxsav)) then
            if(kount.lt.kmax-1)then
              kount=kount+1
              xp(kount)=x
              do 13 i=1,nvar
                yp(i,kount)=y(i)
13            continue
              xsav=x
            endif
          endif
        endif
        if((x+h-x2)*(x+h-x1).gt.0.) h=x2-x
        call rkqs(y,dydx,nvar,x,h,eps,yscal,hdid,hnext,derivs)
        if(hdid.eq.h)then
          nok=nok+1
        else
          nbad=nbad+1
        endif
        if((x-x2)*(x2-x1).ge.0.)then
          do 14 i=1,nvar
            ystart(i)=y(i)
14        continue
          if(kmax.ne.0)then
            kount=kount+1
            xp(kount)=x
            do 15 i=1,nvar
              yp(i,kount)=y(i)
15          continue
          endif
          return
        endif
        if(abs(hnext).lt.hmin) pause
     *'stepsize smaller than minimum in odeint'
        h=hnext
16    continue
      pause 'too many steps in odeint'
      return
      END
!  (C) Copr. 1986-92 Numerical Recipes Software v%1jw#<0(9p#3.

!---------------------------------------------------------

      SUBROUTINE rkqs(y,dydx,n,x,htry,eps,yscal,hdid,hnext,derivs)
      INTEGER n,NMAX
      double precision eps,hdid,hnext,htry,x,dydx(n),y(n),yscal(n)
      EXTERNAL derivs
      PARAMETER (NMAX=50)
!U    USES derivs,rkck
      INTEGER i
      double precision errmax,h,htemp,xnew,yerr(NMAX),ytemp(NMAX),SAFETY,PGROW,
     *PSHRNK,ERRCON
      PARAMETER (SAFETY=0.9,PGROW=-.2,PSHRNK=-.25,ERRCON=1.89e-4)
      h=htry
1     call rkck(y,dydx,n,x,h,ytemp,yerr,derivs)
      errmax=0.
      do 11 i=1,n
        errmax=max(errmax,abs(yerr(i)/yscal(i)))
11    continue
      errmax=errmax/eps
      if(errmax.gt.1.)then
        htemp=SAFETY*h*(errmax**PSHRNK)
        h=sign(max(abs(htemp),0.1*abs(h)),h)
        xnew=x+h
        if(xnew.eq.x)pause 'stepsize underflow in rkqs'
        goto 1
      else
        if(errmax.gt.ERRCON)then
          hnext=SAFETY*h*(errmax**PGROW)
        else
          hnext=5.*h
        endif
        hdid=h
        x=x+h
        do 12 i=1,n
          y(i)=ytemp(i)
12      continue
        return
      endif
      END
!  (C) Copr. 1986-92 Numerical Recipes Software v%1jw#<0(9p#3.

!---------------------------------------------------------

      SUBROUTINE rkck(y,dydx,n,x,h,yout,yerr,derivs)
      INTEGER n,NMAX
      double precision h,x,dydx(n),y(n),yerr(n),yout(n)
      EXTERNAL derivs
      PARAMETER (NMAX=50)
!U    USES derivs
      INTEGER i
      double precision ak2(NMAX),ak3(NMAX),
     *ak4(NMAX),ak5(NMAX),ak6(NMAX),
     *ytemp(NMAX),A2,A3,A4,A5,A6,B21,B31,B32,B41,B42,B43,B51,B52,B53,
     *B54,B61,B62,B63,B64,B65,C1,C3,C4,C6,DC1,DC3,DC4,DC5,DC6
      PARAMETER (A2=.2,A3=.3,A4=.6,A5=1.,A6=.875,B21=.2,B31=3./40.,
     *B32=9./40.,B41=.3,B42=-.9,B43=1.2,B51=-11./54.,B52=2.5,
     *B53=-70./27.,B54=35./27.,B61=1631./55296.,B62=175./512.,
     *B63=575./13824.,B64=44275./110592.,B65=253./4096.,C1=37./378.,
     *C3=250./621.,C4=125./594.,C6=512./1771.,DC1=C1-2825./27648.,
     *DC3=C3-18575./48384.,DC4=C4-13525./55296.,DC5=-277./14336.,
     *DC6=C6-.25)
      do 11 i=1,n
        ytemp(i)=y(i)+B21*h*dydx(i)
11    continue
      call derivs(x+A2*h,ytemp,ak2)
      do 12 i=1,n
        ytemp(i)=y(i)+h*(B31*dydx(i)+B32*ak2(i))
12    continue
      call derivs(x+A3*h,ytemp,ak3)
      do 13 i=1,n
        ytemp(i)=y(i)+h*(B41*dydx(i)+B42*ak2(i)+B43*ak3(i))
13    continue
      call derivs(x+A4*h,ytemp,ak4)
      do 14 i=1,n
        ytemp(i)=y(i)+h*(B51*dydx(i)+B52*ak2(i)+B53*ak3(i)+B54*ak4(i))
14    continue
      call derivs(x+A5*h,ytemp,ak5)
      do 15 i=1,n
        ytemp(i)=y(i)+h*(B61*dydx(i)+B62*ak2(i)+B63*ak3(i)+B64*ak4(i)+
     *B65*ak5(i))
15    continue
      call derivs(x+A6*h,ytemp,ak6)
      do 16 i=1,n
        yout(i)=y(i)+h*(C1*dydx(i)+C3*ak3(i)+C4*ak4(i)+C6*ak6(i))
16    continue
      do 17 i=1,n
        yerr(i)=h*(DC1*dydx(i)+DC3*ak3(i)+DC4*ak4(i)+DC5*ak5(i)+DC6*
     *ak6(i))
17    continue
      return
      END
!  (C) Copr. 1986-92 Numerical Recipes Software v%1jw#<0(9p#3.

       function eta2p(za,z1,nrml1,Dz,pzrms,z0)

      implicit double precision (a-h,o-z)
      integer, parameter   :: dp=kind(0.d0),nl=41,nn=25,nn2=nn*3
      integer                       :: x,y,z,bin1,bin2,bin3,m,t
      real (kind=dp)  :: q,q2,a,b,c,d,e,f,g,h,i,j,k,l,pnrml,zp,zpstep
      real (kind=dp)  :: wwx(nn),wwy(nn),wwz(nn2),wa,za,aa,wzb,wza
      real (kind=dp)  :: px,py,wx,ra,pi,eta,ppx(nn),ppy(nn),pnrmlz(nn)
      real (kind=dp)  :: c7(nl),c8(nl),ell(nl),dcigg(nl),nrml,qq,qq2
      real (kind=dp)  :: z1,z2,z3,zx,zy,zz,zxstep,zystep,zzstep,qb
      real (kind=dp)  :: tau0,tau1,tau,rx,ry,rz,nrml1,h0,nzx,nzy,nzz
      real (kind=dp)  :: dzchi,deltai,wijk,b1,b2,ww(nn),nrml2,nrml3
      real (kind=dp)  :: wy,wz,w(nn),ft1,ft2,ft,ax,ay,az,rombint,ny(nn)
      real (kind=dp)  :: di(nn),frac,fsky,p1(nn),pp(nn),pnrmla,nx(nn)
      real (kind=dp)  :: zt, wwt(nn), wt, pt,nzt,ztstep,eta2p
	  real (kind=dp)  :: Dz,pzrms,z0
	  common /cosmos/ omegam,omegah

      external func1

!     PDF normalization

      pi=3.14159

!     Eta Numerator

      h0=omegah/omegam*100. !71. !1 !2.998d5/71.
      tau0=rombint(func1,0.,1.0,1.0d-5)/h0
      zxstep=0.2/nn
       zzstep=(5.-za)/nn2
       aa=1./(za+1.)
       tau=rombint(func1,0.,aa,1.0d-5)/h0
       ra=tau0-tau
      do z=1,nn2
       zz=za+z*zzstep
       az=1./(zz+1.)
       tau=rombint(func1,0.,az,1.0d-5)/h0
       rz=tau0-tau

      do x=1,nn
       zx=z1+x*zxstep
       px=exp(-(zz-zx-Dz)**2/2/(pzrms*(1+zz))**2)
     2 /sqrt(2.*pi)/pzrms/(1+zz)
       ppx(x)=px
       nzx=zz**2/2/z0**3*exp(-zz/z0)/nrml1 !nzx=zx**2/2/z0**3*exp(-zx/z0)/nrml1

       zystep=(z1+0.2-zx)/nn
       do y=1,nn
        zy=zx+y*zystep
        py=exp(-(za-zy-Dz)**2/2/(pzrms*(1+za))**2)
     2 /sqrt(2.*pi)/pzrms/(1+za)
        nzy=za**2/2/z0**3*exp(-za/z0)/nrml1 !nzy=zy**2/2/z0**3*exp(-zy/z0)/nrml1

        wwy(y)=nzy*py
       end do
       call integ(wwy,wy,nn)
       wy=wy*zystep
       wwx(x)=nzx*px*wy
      end do
      call integ(wwx,wx,nn)
      wx=wx*zxstep

      wwz(z)=(1-ra/rz)*(1+za)*ra*wx
      end do
      call integ(wwz,wz,nn2)
      wza=wz*zzstep

!     Eta Denominator

      zystep=0.2/nn

      do z=1,nn2
       zz=za+z*zzstep
       az=1./(zz+1.)
       tau=rombint(func1,0.,az,1.0d-5)/h0
       rz=tau0-tau

      do x=1,nn
       zx=z1+x*zxstep
       px=exp(-(zz-zx-Dz)**2/2/(pzrms*(1+zz))**2)
     2 /sqrt(2.*pi)/pzrms/(1+zz)
       nzx=zz**2/2/z0**3*exp(-zz/z0)/nrml1 !nzx=zx**2/2/z0**3*exp(-zx/z0)/nrml1

       do y=1,nn
        zy=z1+y*zystep
        py=exp(-(za-zy-Dz)**2/2/(pzrms*(1+za))**2)
     2 /sqrt(2.*pi)/pzrms/(1+za)
        nzy=za**2/2/z0**3*exp(-za/z0)/nrml1 !nzy=zy**2/2/z0**3*exp(-zy/z0)/nrml1

        wwy(y)=nzy*py
       end do
       call integ(wwy,wy,nn)
       wy=wy*zystep
       wwx(x)=nzx*px*wy

      end do
      call integ(wwx,wx,nn)
      wx=wx*zxstep

      wwz(z)=(1-ra/rz)*(1+za)*ra*wx
      end do
      call integ(wwz,wz,nn2)
      wzb=wz*zzstep
      eta2p=2.*wza/wzb
	  if (wzb .le. wza) then
		eta2p=0.0;
	  end if
      !print *, eta2p,wza,wzb
      end function eta2p
!-----------------------------------------------------------------
