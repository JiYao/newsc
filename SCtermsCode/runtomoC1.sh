#!/bin/bash

# SRUN OPTIONS
# NOTE: #SBATCH is a command, not a comment
# ----------------------------------------------------------------------
# NECESSARY PARAMETERS

# The number of tasks
#SBATCH -n 1

# ----------------------------------------------------------------------
# OPTIONAL PARAMETERS

# The number of cores per task (used for multi-threaded application, default=1, 
# prefer a power of 6, no more than the max number of cores per node)
# Change this when using openMP in camb for CosmoMC.  Only advantageous when calculating 
# CMB and MPK
#SBATCH -c 6
##SBATCH -c 8
# The number of tasks per node (n * c)
# Can uncomment this to force x number of tasks per node.
##SBATCH --tasks-per-node=1

# The number of nodes
# SLURM issues a warning if you specify more nodes than tasks (N > n). 
# It's better to let slurm calculate it for you.
# IMPORTANT: MPI between nodes will slow down the program because of the 
# heavy I/O. It's recommended to more cores and less nodes for our jobs. 
# Can change below to run MPI jobs across multiple nodes
#SBATCH -N 1
##SBATCH -N 4

# Use --exclusive to get the whole nodes exclusively for this job
##SBATCH --exclusive

# Request a sepcific list of hosts (two different formats)
##SBATCH -w f7
##SBATCH -w ./hostfile
## Request a specific list of hosts NOT be included
#SBATCH --exclude=cosmo,f1
# Setting the name of the error-file to 'job.myjobid.err' and 
# the output-file to 'job.myjobid.out'
# The file paths below are relative to the directory from which you submitted
# Change to your preferences.
#SBATCH --error=%J.err --output=%J.out

#Print  detailed  event  logging to error file
#SBATCH -v

#Give your job a name, so you can more easily identify which job is which
#SBATCH -J TomoC1
# ------------------------------
# VERY OPTIONAL PARAMETERS

# Account name (project ID) to run under
##SBATCH -A <account>

# The maximum allowed run time (D-HH:MM:)
##SBATCH --time=15-00:00:00

# If this job needs 4GB of memory per mpi-task (=mpi ranks, =cores)
##SBATCH --mem-per-cpu=4000

# ----------------------------------------------------------------------
# MPI SET-UP FOR SLURM

# Different types of MPI may result in unique initiation procedures.
# IMPORTANT for mpich2: user assumes the system administrator already link
# your program with SLURM's implementation of the PMI library
# If SLURM is not configured with MpiDefault=pmi2
# then the srun command MUST BE invoked with the option --mpi=pmi2.
# Reference: http://wiki.mpich.org/mpich/index.php/Frequently_Asked_Questions#Q:_How_do_I_use_MPICH_with_slurm.3F
# Reference: http://slurm.schedmd.com/mpi_guide.html#mpich2
# ---------------------------------------------------------------------



# Run the MPI application

#uncomment the line below if you want to run with openMP as advised above.  
#change number of threads to match -c line above.
#export OMP_NUM_THREADS=6

echo "Starting at `date`"
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"

# Changing to director where my application is since I submit from scripts

rm generateinputfiles_spec.out
ifort generateinputfiles_spec.f -o generateinputfiles_spec.out

rm spec_tomo.out
ifort spec_tomo.f -o spec_tomo.out

rm tomo_input.out
ifort tomo_input.f -o tomo_input.out
./tomo_input.out < generateinputfiles_spec.in

# ------------------------------------------------------------

mkdir ./Spectrums/102/
cd Spectrums/102/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_102.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/103/
cd Spectrums/103/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_103.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/104/
cd Spectrums/104/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_104.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/105/
cd Spectrums/105/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_105.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/106/
cd Spectrums/106/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_106.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/107/
cd Spectrums/107/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_107.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/108/
cd Spectrums/108/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_108.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/109/
cd Spectrums/109/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_109.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/110/
cd Spectrums/110/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_110.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/203/
cd Spectrums/203/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_203.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/204/
cd Spectrums/204/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_204.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/205/
cd Spectrums/205/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_205.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/206/
cd Spectrums/206/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_206.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/207/
cd Spectrums/207/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_207.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/208/
cd Spectrums/208/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_208.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/209/
cd Spectrums/209/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_209.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/210/
cd Spectrums/210/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_210.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/304/
cd Spectrums/304/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_304.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/305/
cd Spectrums/305/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_305.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/306/
cd Spectrums/306/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_306.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/307/
cd Spectrums/307/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_307.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/308/
cd Spectrums/308/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_308.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/309/
cd Spectrums/309/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_309.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/310/
cd Spectrums/310/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_310.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGij.in > C_GGij.dat
./spec_tomo.out < C_GGjj.in > C_GGjj.dat
./spec_tomo.out < C_IG.in > C_IG.dat #2 (IGij)
./spec_tomo.out < C_IGii.in > C_IGii.dat
./spec_tomo.out < C_IGjj.in > C_IGjj.dat
./spec_tomo.out < C_II.in > C_II.dat #3
./spec_tomo.out < C_IIjj.in > C_IIjj.dat
./spec_tomo.out < C_gg.in > C_gg.dat #4
./spec_tomo.out < C_Gg.in > C_Gg.dat #5
./spec_tomo.out < C_Ig.in > C_Ig.dat #6
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)
./spec_tomo.out < C_GGNjj.in > C_GGNjj.dat
./spec_tomo.out < C_ggN.in > C_ggN.dat #8

# IG spec
./spec_tomo.out < IG_1p.in > IG_1p.dat
./spec_tomo.out < IG_1m.in > IG_1m.dat

./spec_tomo.out < IG_2p.in > IG_2p.dat
./spec_tomo.out < IG_2m.in > IG_2m.dat

./spec_tomo.out < IG_3p.in > IG_3p.dat
./spec_tomo.out < IG_3m.in > IG_3m.dat

./spec_tomo.out < IG_4p.in > IG_4p.dat
./spec_tomo.out < IG_4m.in > IG_4m.dat

./spec_tomo.out < IG_5p.in > IG_5p.dat
./spec_tomo.out < IG_5m.in > IG_5m.dat

./spec_tomo.out < IG_6p.in > IG_6p.dat
./spec_tomo.out < IG_6m.in > IG_6m.dat

./spec_tomo.out < IG_7p.in > IG_7p.dat
./spec_tomo.out < IG_7m.in > IG_7m.dat

./spec_tomo.out < IG_8p.in > IG_8p.dat
./spec_tomo.out < IG_8m.in > IG_8m.dat

./spec_tomo.out < IG_pz1p.in > IG_pz1p.dat
./spec_tomo.out < IG_pz1m.in > IG_pz1m.dat

./spec_tomo.out < IG_pz2p.in > IG_pz2p.dat
./spec_tomo.out < IG_pz2m.in > IG_pz2m.dat

./spec_tomo.out < IG_IA1p.in > IG_IA1p.dat
./spec_tomo.out < IG_IA1m.in > IG_IA1m.dat

./spec_tomo.out < IG_IA2p.in > IG_IA2p.dat
./spec_tomo.out < IG_IA2m.in > IG_IA2m.dat

# Ig spec
./spec_tomo.out < Ig_1p.in > Ig_1p.dat
./spec_tomo.out < Ig_1m.in > Ig_1m.dat

./spec_tomo.out < Ig_2p.in > Ig_2p.dat
./spec_tomo.out < Ig_2m.in > Ig_2m.dat

./spec_tomo.out < Ig_3p.in > Ig_3p.dat
./spec_tomo.out < Ig_3m.in > Ig_3m.dat

./spec_tomo.out < Ig_4p.in > Ig_4p.dat
./spec_tomo.out < Ig_4m.in > Ig_4m.dat

./spec_tomo.out < Ig_5p.in > Ig_5p.dat
./spec_tomo.out < Ig_5m.in > Ig_5m.dat

./spec_tomo.out < Ig_6p.in > Ig_6p.dat
./spec_tomo.out < Ig_6m.in > Ig_6m.dat

./spec_tomo.out < Ig_7p.in > Ig_7p.dat
./spec_tomo.out < Ig_7m.in > Ig_7m.dat

./spec_tomo.out < Ig_8p.in > Ig_8p.dat
./spec_tomo.out < Ig_8m.in > Ig_8m.dat

./spec_tomo.out < Ig_pz1p.in > Ig_pz1p.dat
./spec_tomo.out < Ig_pz1m.in > Ig_pz1m.dat

./spec_tomo.out < Ig_pz2p.in > Ig_pz2p.dat
./spec_tomo.out < Ig_pz2m.in > Ig_pz2m.dat

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_5p.in > GG_5p.dat
./spec_tomo.out < GG_5m.in > GG_5m.dat

./spec_tomo.out < GG_6p.in > GG_6p.dat
./spec_tomo.out < GG_6m.in > GG_6m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

./spec_tomo.out < GG_pz1p.in > GG_pz1p.dat
./spec_tomo.out < GG_pz1m.in > GG_pz1m.dat

./spec_tomo.out < GG_pz2p.in > GG_pz2p.dat
./spec_tomo.out < GG_pz2m.in > GG_pz2m.dat

cd ../..

#matlab -nodesktop -nosplash -r "plot_spec_result; FisherMatrix_C1;"

echo "Program finished with exit code $? at: `date`"

# ---------------------------------------------------
# Reference: http://www.hpc2n.umu.se/batchsystem/examples_scripts,
#            http://www.hpc2n.umu.se/slurm-submit-file-design
#            https://computing.llnl.gov/tutorials/linux_clusters/man/srun.txt
# ---------------------------------------------------
