        program auto_input

		  implicit double precision (a-h,o-z)
      integer :: i,j
		  character*30 :: savefile

	  read(*,*) omegam,omegah,sigma8,xn,alphas,omegab,wQ0,ibn,wQ1
	  read(*,*) redshift
	  read(*,*) ng
	  read(*,*) nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	  read(*,*) Agp,alphagp,Amp0

	  do i=1, 20

      j=i
      if (i.gt.9) then
        write(savefile,'("./bins/",I2,".in")')i
      else
        write(savefile,'("./bins/",I1,".in")')i
      endif
			open(unit=9,file=savefile,status='replace')
			write(9,*) omegam,omegah,sigma8,xn,alphas,omegab,wQ0,ibn,wQ1
			write(9,*) redshift
			write(9,*) ng
			write(9,*) i,j,z0,Dz,pzrms,zmin,zmax,nbins
			write(9,*) Agp,alphagp,Amp0
			close(9)

	  enddo

	  end
