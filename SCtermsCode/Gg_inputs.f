       program generateinputfiles
c
c  Automatize the creation of the 11 files for the
c  the fiducial model and the files with p_i=p_i +/- epsilon
c
        implicit double precision (a-h,o-z)
        parameter (partial=0.15d0,dalphas=0.1d0,dwa=0.3d0,dDz=0.02d0)
		parameter (ilin=1,domegab=0.01,dpzrms=0.02d0)
		! partial derivative fraction & uncertainty of Delta_z
		parameter (IAtype=22) ! 21: power law correlation, 22: amplitude
c
	write(*,*)'Enter the parameters for the fiducial model'
        write(*,*)'omegam,omegah,sigma8,n,alphas,omegab,
     2  wQ0,ibn,wQ1 (0 for Mo etal)'
      read(*,*)omegam,omegah,sigma8,xn,alphas,omegab,wQ0,ibn,wQ1

	write(*,*)'Enter redshift to be used: '
        read(*,*) redshift

	write(*,*)'Enter density of galaxies: '
		read(*,*) ng
	write(*,*)'nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins'
		read(*,*) nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	write(*,*) 'power law: Agp alphagp, IA amplitude Amp0'
		read(*,*) Agp,alphagp,Amp0

c
!	C^Gg_ii
!
        open(unit=16,file='C_Gg.in',status='replace')
        rewind 16
	write(16,*)3
        write(16,*)omegam,omegah,sigma8,xn,
     2  alphas,omegab,wQ0,ibn,wQ1
	write(16,*)ilin
	write(16,*)redshift,ng
	write(16,*)5
	write(16,*)nbin1,nbin1,z0,Dz,pzrms,zmin,zmax,nbins
	write(16,*)Agp,alphagp,Amp0


	close(16)
	
        stop
        end
