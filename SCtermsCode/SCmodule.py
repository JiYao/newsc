from cosmosis.datablock import names, option_section
import numpy as np
from scipy import interpolate
import ConfigParser

def pdf(zph,z,sigma_z, Delta_z): # n_tot(z) * pdf(z|zph)
	z0=0.3
	return 1./2./z0*(z/z0)**2*np.exp(-z/z0) * 1./np.sqrt(2.*np.pi)/sigma_z/(1+z)*np.exp(-(z-zph-Delta_z)**2/2./(sigma_z*(1+z))**2)

def setup(options):
	config = ConfigParser.RawConfigParser()
	config.read(r'SelfCalibration/photoz_para.ini')
	sigma_z = float(config.get('photo-z parameters','sigma_z'))
	Delta_z = float(config.get('photo-z parameters','Delta_z'))
	return sigma_z, Delta_z

def execute(block,config):

	sigma_z, Delta_z = config

	c_kms = 299792.4580
	
	zmin=0.4
	bin_width = 0.2
	zP_num = 20 #20
	step_zP = bin_width / zP_num
	
	z_distance = block[names.distances, 'z'][::-1].copy()
	a_distance = block[names.distances, 'a'][::-1].copy()
	chi_distance = block[names.distances, 'd_m'][::-1].copy()
	
	h0 = block[names.cosmological_parameters, "h0"]
	omega_m = block[names.cosmological_parameters, "omega_m"]
	
	nz_name = "nz_sample"
	z_nz = block[nz_name, "z"]
	nbin = block[nz_name,"nbin"]
	step_z = z_nz[2] - z_nz[1]
	lenz = len(z_nz)
	
	nz=[]
	for i in xrange(1,nbin+1):
		line = block[nz_name, "bin_{0}".format(i)]
		nz.append(line)
	
	# interpolation
	tck = interpolate.splrep(z_distance, chi_distance, s=0)
	chi_nz = interpolate.splev(z_nz, tck, der=0)
	dchidz = interpolate.splev(z_nz, tck, der=1)
	
	Wij = np.zeros([nbin,nbin])
	Delta_inv = np.zeros(nbin)
	for i in xrange(nbin):
		n_i = nz[i]
		for j in xrange(i,nbin):
			n_j = nz[j]
			for z_L in xrange(1,lenz-1): # lens galaxy
				for z_G in xrange(z_L,lenz): # shear galaxy z_L > z_G
					W_L = 3./2. * omega_m * (1.+z_nz[z_L]) * (100.*h0/c_kms)**2. * chi_nz[z_L] * (1.-chi_nz[z_L]/chi_nz[z_G])
					Wij[i,j] += W_L * n_i[z_L] * n_j[z_G]
		for z in xrange(2,len(z_nz)):
			Delta_inv[i] += n_i[z]**2. / dchidz[z] * step_z
	Wij *= step_z**2.
	Delta_i = 1./Delta_inv
	
	block.put("SCterms","Wij",Wij)
	block.put("SCterms","Delta_i",Delta_i)
	
	print Wij
	print Delta_i
	
	eta = np.zeros([nbin,lenz-2])
	for i in xrange(nbin):
		for z in xrange(1,lenz-1): # z = z_L = z_g
			num = 0.
			den = 0.
			for z_G in xrange(z,lenz): # z_G > z
				W_L = 3./2. * omega_m * (1.+z_nz[z]) * (100.*h0/c_kms)**2. * chi_nz[z] * (1.-chi_nz[z]/chi_nz[z_G])
				tmp = [zmin+0.2*i+step_zP*(0.5+tmp1) for tmp1 in xrange(zP_num)]
				for zP_G in tmp:
					for zP_g in tmp:
						if zP_G < zP_g :
							num += W_L * pdf(zP_G,z_nz[z_G],sigma_z,Delta_z) * pdf(zP_g,z_nz[z],sigma_z,Delta_z)
						den += W_L * pdf(zP_G,z_nz[z_G],sigma_z,Delta_z) * pdf(zP_g,z_nz[z],sigma_z,Delta_z)
				# num and den should * step_zP**2 * step_z, but canceled
			eta[i][z-1] = 2. * num / den
			#print num,den
			
	block.put("SCterms","eta",eta)
	
	print eta
	print Wij
	print Delta_i
	
	print "~~~~~~~~~~~~~~~~~~~~~~~~~~ no error so far!"
	return 0

def cleanup(config):
	pass

