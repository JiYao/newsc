clc;clear;close all;

% take C^(1)=GG spectrum, and C^GG=C^(1)-C^IG. IG is anti-correlation, and
% recall we normally ignore the - sign in spectrum calculation. Thus the
% actual C^GG we want is calculated by GG+IG spectrum

%% load fiducial spectrums

p=[0.315 0.211995 0.829 0.9603 0 0.049 -1.0 0 0 0.05 1.0];
% parameters: Omega_m Omega_m*h sigma8 xn alpha_s z0 w_0 w_a Delta_z sigma_z A0

load C_GG.dat; % C^GG_ii
l=C_GG(:,1);
slt=find(20<=l & l<=5000);
l=l(slt);
GG=C_GG(slt,2);

load C_GGij.dat;
GGij=C_GGij(slt,2);
load C_GGjj.dat;
GGjj=C_GGjj(slt,2);

load C_IG.dat; % C^IG_ij
IG=C_IG(slt,2);
Wij=C_IG(slt,3); % get Wij from IG as both of them are using ij other than ii

load C_IGii.dat;
IGii=C_IGii(slt,2);
load C_IGjj.dat;
IGjj=C_IGjj(slt,2);

load C_II.dat;
II=C_II(slt,2);

load C_IIjj.dat;
IIjj=C_IIjj(slt,2);

load C_gg.dat;
gg=C_GG(slt,2);

load C_Gg.dat;
Gg=C_Gg(slt,2);
Delta_inv=C_Gg(slt,4); % get Delta_i^-1 from Gg based on definition
Q=C_Gg(slt,5);

load C_IG.dat;
IG=C_IG(slt,2);

load C_GGN.dat; % C^GG,N_ii
GGN=C_GGN(slt,2);

load C_GGNjj.dat;
GGNjj=C_GGNjj(slt,2);

load C_ggN.dat;
ggN=C_ggN(slt,2);

load C_Ig.dat
Ig=C_Ig(slt,2);

%% load +/- spectrums and calculate partial derivatives

partial=0.15;
dalphas=0.1;
dwa=0.3;
domegab=0.01;
dDz=0.01;
bi=1;
dIGdp=zeros(11,length(slt));
dC1dp=zeros(11,length(slt));

load IG_1p.dat;
load Ig_1p.dat;
load IG_1m.dat;
load Ig_1m.dat;
dIGdp(1,:)=( IG_1p(slt,3)./Ig_1p(slt,4)/bi.*Ig_1p(slt,2) - IG_1m(slt,3)./Ig_1m(slt,4)/bi.*Ig_1m(slt,2) ) ./ (2*partial*p(1));
load GG_1p.dat;
load GG_1m.dat;
dC1dp(1,:)=( GG_1p(slt,2)-IG_1p(slt,2)-GG_1m(slt,2)+IG_1m(slt,2) ) ./ (2*partial*p(1));

load IG_2p.dat;
load Ig_2p.dat;
load IG_2m.dat;
load Ig_2m.dat;
dIGdp(2,:)=( IG_2p(slt,3)./Ig_2p(slt,4)/bi.*Ig_2p(slt,2) - IG_2m(slt,3)./Ig_2m(slt,4)/bi.*Ig_2m(slt,2) ) ./ (2*partial*p(2));
load GG_2p.dat;
load GG_2m.dat;
dC1dp(2,:)=( GG_2p(slt,2)-IG_2p(slt,2)-GG_2m(slt,2)+IG_2m(slt,2) ) ./ (2*partial*p(2));

load IG_3p.dat;
load Ig_3p.dat;
load IG_3m.dat;
load Ig_3m.dat;
dIGdp(3,:)=( IG_3p(slt,3)./Ig_3p(slt,4)/bi.*Ig_3p(slt,2) - IG_3m(slt,3)./Ig_3m(slt,4)/bi.*Ig_3m(slt,2) ) ./ (2*partial*p(3));
load GG_3p.dat;
load GG_3m.dat;
dC1dp(3,:)=( GG_3p(slt,2)-IG_3p(slt,2)-GG_3m(slt,2)+IG_3m(slt,2) ) ./ (2*partial*p(3));

load IG_4p.dat;
load Ig_4p.dat;
load IG_4m.dat;
load Ig_4m.dat;
dIGdp(4,:)=( IG_4p(slt,3)./Ig_4p(slt,4)/bi.*Ig_4p(slt,2) - IG_4m(slt,3)./Ig_4m(slt,4)/bi.*Ig_4m(slt,2) ) ./ (2*partial*p(4));
load GG_4p.dat;
load GG_4m.dat;
dC1dp(4,:)=( GG_4p(slt,2)-IG_4p(slt,2)-GG_4m(slt,2)+IG_4m(slt,2) ) ./ (2*partial*p(4));

load IG_5p.dat;
load Ig_5p.dat;
load IG_5m.dat;
load Ig_5m.dat;
dIGdp(5,:)=( IG_5p(slt,3)./Ig_5p(slt,4)/bi.*Ig_5p(slt,2) - IG_5m(slt,3)./Ig_5m(slt,4)/bi.*Ig_5m(slt,2) ) ./ (2*dalphas);
load GG_5p.dat;
load GG_5m.dat;
dC1dp(5,:)=( GG_5p(slt,2)-IG_5p(slt,2)-GG_5m(slt,2)+IG_5m(slt,2) ) ./ (2*dalphas);

load IG_6p.dat;
load Ig_6p.dat;
load IG_6m.dat;
load Ig_6m.dat;
dIGdp(6,:)=( IG_6p(slt,3)./Ig_6p(slt,4)/bi.*Ig_6p(slt,2) - IG_6m(slt,3)./Ig_6m(slt,4)/bi.*Ig_6m(slt,2) ) ./ (2*domegab);
load GG_6p.dat;
load GG_6m.dat;
dC1dp(6,:)=( GG_6p(slt,2)-IG_6p(slt,2)-GG_6m(slt,2)+IG_6m(slt,2) ) ./ (2*domegab);

load IG_7p.dat;
load Ig_7p.dat;
load IG_7m.dat;
load Ig_7m.dat;
dIGdp(7,:)=( IG_7p(slt,3)./Ig_7p(slt,4)/bi.*Ig_7p(slt,2) - IG_7m(slt,3)./Ig_7m(slt,4)/bi.*Ig_7m(slt,2) ) ./ (2*partial*p(7));
load GG_7p.dat;
load GG_7m.dat;
dC1dp(7,:)=( GG_7p(slt,2)-IG_7p(slt,2)-GG_7m(slt,2)+IG_7m(slt,2) ) ./ (2*partial*p(7));

load IG_8p.dat;
load Ig_8p.dat;
load IG_8m.dat;
load Ig_8m.dat;
dIGdp(8,:)=( IG_8p(slt,3)./Ig_8p(slt,4)/bi.*Ig_8p(slt,2) - IG_8m(slt,3)./Ig_8m(slt,4)/bi.*Ig_8m(slt,2) ) ./ (2*dwa);
load GG_8p.dat;
load GG_8m.dat;
dC1dp(8,:)=( GG_8p(slt,2)-IG_8p(slt,2)-GG_8m(slt,2)+IG_8m(slt,2) ) ./ (2*dwa);

load IG_pz1p.dat;
load Ig_pz1p.dat;
load IG_pz1m.dat;
load Ig_pz1m.dat;
dIGdp(9,:)=( IG_pz1p(slt,3)./Ig_pz1p(slt,4)/bi.*Ig_pz1p(slt,2) - IG_pz1m(slt,3)./Ig_pz1m(slt,4)/bi.*Ig_pz1m(slt,2) ) ./ (2*dDz);
load GG_pz1p.dat;
load GG_pz1m.dat;
dC1dp(9,:)=( GG_pz1p(slt,2)-IG_pz1p(slt,2)-GG_pz1m(slt,2)+IG_pz1m(slt,2) ) ./ (2*dDz);

load IG_pz2p.dat;
load Ig_pz2p.dat;
load IG_pz2m.dat;
load Ig_pz2m.dat;
dIGdp(10,:)=( IG_pz2p(slt,3)./Ig_pz2p(slt,4)/bi.*Ig_pz2p(slt,2) - IG_pz2m(slt,3)./Ig_pz2m(slt,4)/bi.*Ig_pz2m(slt,2) ) ./ (2*partial*p(10));
load GG_pz2p.dat;
load GG_pz2m.dat;
dC1dp(10,:)=( GG_pz2p(slt,2)-IG_pz2p(slt,2)-GG_pz2m(slt,2)+IG_pz2m(slt,2) ) ./ (2*partial*p(10));

load IG_IA1p.dat;
load IG_IA1m.dat;
dIGdp(11,:)=( IG_IA1p(slt,2)-IG_IA1m(slt,2) ) ./ (2*partial*p(11));
dC1dp(11,:)=-dIGdp(11,:);


%% DeltaC^IG_ij

fsky=0.436;

D_C_IGij=Wij./Delta_inv/bi.* sqrt( (1./(2*l+1)./(0.2*l)/fsky) .* ( gg.*GG + (1+1/3./(1-Q).^2).*(gg.*GGN+ggN.*(GG+II)) + ggN.*GGN.*(1+1./(1-Q).^2)) );
%D_C_IGij=Wij./Delta_inv/bi.* sqrt( (1./(2*l+1)./(0.2*l)/fsky) .* ( gg.*(GG-2*IGii+II) + (1+1/3./(1-Q).^2).*(gg.*GGN+ggN.*(GG+II-2*IGii)) + ggN.*GGN.*(1+1./(1-Q).^2)) );
D_C_IGij=D_C_IGij';

%% DeltaC^(1)_ij
n=26 /(1/60/180*3.14)^2; % number density [per arcmin^2] -> [rad^-2]
gamma_rms=0.26; % rms shape error, need to be corrected after correction in spec_tomo.f

%D_C_1ij=sqrt(1./l./(0.2.*l)./fsky).*(GG+gamma_rms^2/n);
C1jj=GGjj-IGjj-IGjj+IIjj;
C1ii=GG-IGii-IGii+II;
C1ij=GGij-IG; % C^GI_ij and C^II_ij are neglectable
%D_C_1ij=sqrt(1/2./l./fsky.*(2*GGN.*(GG+GGN)));
D_C_1ij=sqrt( 1./(2*l+1)./(0.2*l)/fsky.* (GGN.*GGNjj+GGN.*C1jj+GGNjj.*C1ii+C1ij.^2+C1ii.*C1jj) );
D_C_1ij=D_C_1ij';

%% Cov[dC^(1)_ij,dC^IG_ij]

%cross_1_IG= (1./(2*l+1)/fsky) .*Wij./Delta_inv/bi .*( (GGij-IG).*(Gg-Ig) ); % C^Gg+C^Ig=Gg-Ig, as G and I are in opposite directions, and for WL ellipticity is defined so that Gg is +
%cross_1_IG= (1./(2*l+1)./(0.2*l)/fsky) .*( (GGij-IG).*(-IG) + (-IGii+II).*(GGjj-IGjj) );
cross_1_IG= (1./(2*l+1)./(0.2*l)/fsky) .* (-2*Wij./Delta_inv/bi.*Ig .* (GGij-IG) );
D_C_GGij=sqrt(D_C_IGij.^2+D_C_1ij.^2-2*cross_1_IG'); % 2<d(1)dIG> <= (D_1)^2 + (D_IG)^2 is required

%% Included Parameters

IP=[1:4,6:8];
names={'\Omega_m','\Omega_m*h_0','\sigma_8','n_s','\Omega_b','w_0','w_a'};
%parameters={'\Omega_m','\Omega_m*h_0','\sigma_8','n_s','\alpha_s','\Omega_b','w_0','w_a','\Delta_z','\sigma_z','A0'};
%                 1           2            3        4       5          6       7     8        9         10       11

%% Fisher Matrix and Covariance Matrix

F1=zeros(11);
F2=zeros(11);
dp=zeros(3,length(IP)+1); % uncertainties on the parameters using SC (1,:), without SC (2,:), and Marginalization (3,:)
for alpha=1:11
    for beta=1:11
			F1(alpha,beta)=sum( 1./D_C_GGij.^2.*(dC1dp(alpha,:)+dIGdp(alpha,:)).*(dC1dp(beta,:)+dIGdp(beta,:)) ); % using C^GG=C^(1)-C^IG, C^(1)=GG, C^IG=-IG because of anti-correlation
    end;

    for beta=1:11
        F2(alpha,beta)=sum(1./D_C_1ij.^2.*dC1dp(alpha,:).*dC1dp(beta,:));
    end;
end;
F1=F1(IP,IP); % SC
F3=F2([IP,11],[IP,11]); % marginalization IA
F2=F2(IP,IP); % without SC
p=p([IP,11]);

p(2)=p(2)/p(1); % \Omega_m *h --> h
M=eye(length(IP));
M(1,2)=0; M(2,1)=p(2); M(2,2)=p(1);
F1=M'*F1*M;
F2=M'*F2*M;
M=eye(length(IP)+1);
M(1,2)=0; M(2,1)=p(2); M(2,2)=p(1);
F3=M'*F3*M;
names{2}='h_0';

C1=inv(F1); % covariance matrix
C2=inv(F2);
C3=inv(F3);
for alpha=1:length(IP)
	dp(1,alpha)=sqrt(C1(alpha,alpha));
	dp(2,alpha)=sqrt(C2(alpha,alpha));
end;
for alpha=1:(1+length(IP))
	dp(3,alpha)=sqrt(C3(alpha,alpha));
end;
%dp=dp(:,1:9);
x=[p;dp];
save 'FisherMatrix_C1.txt' -ascii F1;
save 'FisherMatrix_GG.txt' -ascii F2;
save 'ParameterConstrains.txt' -ascii x;

%% plot PDF

indx=[1 3 2 6 7]; % plot index
num_para=length(indx);
range=4; % +/- 4 sigma
ndot=50;
ps=300; % plot-size

figure;
set(gcf,'Position',[0,0,ps*(num_para+1),ps*2]);
for eko=1:num_para
    %figure(eko);
    subplot('Position',[1/(num_para+1)*(eko-0.5),0.25,1/(num_para+1),0.5]);
    
    fiducial=p(indx(eko));
    err=dp(2,indx(eko));
    para=linspace((fiducial-err*range),(fiducial+err*range),ndot);
    PDF=normpdf(para,fiducial,err);
    plot(para,PDF,'k','LineWidth',2);
    str{1}=['Without SC'];
    
    hold on;
    fiducial=p(indx(eko));
    err=dp(1,indx(eko));
    para=linspace((fiducial-err*range),(fiducial+err*range),ndot);
    PDF=normpdf(para,fiducial,err);
    plot(para,PDF,'b','LineWidth',2);
    str{2}=['With SC'];
	
	fiducial=p(indx(eko));
    err=dp(3,indx(eko));
    para=linspace((fiducial-err*range),(fiducial+err*range),ndot);
    PDF=normpdf(para,fiducial,err);
    plot(para,PDF,'r','LineWidth',2);
    str{3}=['Marginalize IA'];
    hold off;
    
%    legend(str);
    xlabel(names(eko),'FontSize',14);
    ylabel('Prob','FontSize',14);
    set(gca,'FontSize',14);
    xlim([(fiducial-err*range),(fiducial+err*range)]);
    
    %saveas(gca,['PDF_',num2str(eko)],'tif');
end;

legend(str,'Position',[0.8,0.8,0.01,0.01]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperUnits', 'points');
set(gcf, 'PaperPosition', [0 0 1600 320]);
saveas(gca,'PDF','png');

%% plot Omeag_m - sigma_8 contour

omega_m=linspace(p(1)-range*dp(2,1),p(1)+range*dp(2,1),500);
sigma_8=linspace(p(3)-range*dp(2,3),p(3)+range*dp(2,3),500);
[X,Y]=meshgrid(omega_m,sigma_8);

rho=C2(1,3)/dp(2,1)/dp(2,3);
chi2=( (X-p(1)).^2./C2(1,1) + (Y-p(3)).^2./C2(3,3) - 2*rho*(X-p(1))/dp(2,1).*(Y-p(3))/dp(2,3) ) /(1-rho^2);
figure;
contour(X,Y,chi2,[2.3,2.3],'k');
lgd{1}=['Without SC'];

rho=C1(1,3)/dp(1,1)/dp(1,3);
chi2=( (X-p(1)).^2./C1(1,1) + (Y-p(3)).^2./C1(3,3) - 2*rho*(X-p(1))/dp(1,1).*(Y-p(3))/dp(1,3) ) /(1-rho^2);
hold on;
contour(X,Y,chi2,[2.3,2.3],'b'); 
lgd{2}=['With SC'];

rho=C3(1,3)/dp(3,1)/dp(3,3);
chi2=( (X-p(1)).^2./C3(1,1) + (Y-p(3)).^2./C3(3,3) - 2*rho*(X-p(1))/dp(3,1).*(Y-p(3))/dp(3,3) ) /(1-rho^2);
contour(X,Y,chi2,[2.3,2.3],'r'); 
lgd{3}=['Marginalize IA'];

hold off;
legend(lgd);

xlabel(['\Omega_m']);
ylabel(['\sigma_8']);
saveas(gca,'contour','pdf');

%% plot all contours

figure;
ps=500;
set(gcf,'Position',[0,0,ps*num_para,ps*num_para]);

for i=1:num_para-1
	for j=i+1:num_para
		subplot('Position',[1/num_para*i,1/num_para*(num_para+1-j),1/num_para,1/num_para]);
		p1=linspace( p(indx(i))-range*dp(2,indx(i)), p(indx(i))+range*dp(2,indx(i)), 500 );
		p2=linspace( p(indx(j))-range*dp(2,indx(j)), p(indx(j))+range*dp(2,indx(j)), 500 );
		[X,Y]=meshgrid(p1,p2);
		
		rho=C2(indx(i),indx(j))/dp(2,indx(i))/dp(2,indx(j));
		chi2=( (X-p(indx(i))).^2./C2(indx(i),indx(i)) + (Y-p(indx(j))).^2./C2(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/dp(2,indx(i)).*(Y-p(indx(j)))/dp(2,indx(j)) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'k');
		lgd{1}=['Without SC'];
		
		rho=C1(indx(i),indx(j))/dp(1,indx(i))/dp(1,indx(j));
		chi2=( (X-p(indx(i))).^2./C1(indx(i),indx(i)) + (Y-p(indx(j))).^2./C1(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/dp(1,indx(i)).*(Y-p(indx(j)))/dp(1,indx(j)) )/(1-rho^2);
		hold on;
		contour(X,Y,chi2,[2.3,2.3],'g');
		lgd{2}=['With SC'];
		
		rho=C3(indx(i),indx(j))/dp(3,indx(i))/dp(3,indx(j));
		chi2=( (X-p(indx(i))).^2./C3(indx(i),indx(i)) + (Y-p(indx(j))).^2./C3(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/dp(3,indx(i)).*(Y-p(indx(j)))/dp(3,indx(j)) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'r');
		lgd{3}=['Marginalize IA'];
        
        %MU=[p(indx(i)) p(indx(j))];
        %C=[C2(indx(i),indx(i)) C2(indx(i),indx(j)); C2(indx(j),indx(i)) C2(indx(j),indx(j))];
        %error_ellipse(C,MU,0.95);
        %lgd{4}=['2 \sigma w/o SC error\_ellipse'];
		hold off;
		
		if j==num_para xlabel(names{indx(i)}); end;
		if i==1 ylabel(names{indx(j)}); end;
	end;
end;

legend(lgd,'Position',[0.8,0.8,0.01,0.01]);
saveas(gca,'contour','png');
