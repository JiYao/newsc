from cosmosis.datablock import names, option_section
import numpy as np
from scipy import interpolate
import ConfigParser
from time import time

zmin = 0.4
zmed = 0.9
# z0 = zmed/1.412
z0 = 0.5
a = 1.27
b = 1.02

def pdf(zph,z,sigma_z, Delta_z): # n_tot(z) * pdf(z|zph)
	global z0, a, b
	return z**a * np.exp( -(z/z0)**b ) * 1./np.sqrt(2.*np.pi)/sigma_z/(1+z)*np.exp(-(z-zph-Delta_z)**2/2./(sigma_z*(1+z))**2)

def setup(options):
	config = ConfigParser.RawConfigParser()
	config.read(r'SelfCalibration/photoz_para.ini')
	sigma_z = float(config.get('photo-z parameters','sigma_z'))
	Delta_z = float(config.get('photo-z parameters','Delta_z'))
	return sigma_z, Delta_z

def execute(block,config):

	t0=time()

	sigma_z, Delta_z = config

	c_kms = 299792.4580

	zmin=0.4
	bin_width = 0.1
	zP_num = 10 #20
	step_zP = bin_width / zP_num

	z_distance = block[names.distances, 'z'][::-1].copy()
	a_distance = block[names.distances, 'a'][::-1].copy()
	chi_distance = block[names.distances, 'd_m'][::-1].copy()
	l_GG = block['shear_cl','ell']

	z = [tmp for tmp in z_distance if 0<tmp<=3.5]
	lenz = len(z)
	a = a_distance[1:lenz+1] # remove z=0 incase chi=0 and get divided
	chi = chi_distance[1:lenz+1]
	ell = [tmp for tmp in l_GG if tmp<5000]
	nl = len(ell)

	# interpolation: dchi/dz
	tck = interpolate.splrep(z, chi, s=0)
	dchidz = interpolate.splev(z, tck, der=1)

	step_z = z[2] - z[1]

	h0 = block[names.cosmological_parameters, "h0"]
	omega_m = block[names.cosmological_parameters, "omega_m"]

	nz_name = "nz_sample"
	z_nz = block[nz_name, "z"]
	nbin = block[nz_name,"nbin"]

	# load n(z) with interpolation
	nz=[]
	for i in xrange(1,nbin+1):
		nz_i = block[nz_name, "bin_{0}".format(i)]
		tck = interpolate.splrep(z_nz, nz_i, s=0)
		line = interpolate.splev(z, tck, der=0)
		nz.append(line)

	# W_i in Zhang SC, q_i in Bridle & King
	W_i = np.zeros([nbin,lenz])
	for i in xrange(nbin):
		n_i = nz[i]
		for z_L in xrange(lenz-1):
			for z_G in xrange(z_L+1,lenz):
				W_L = 3./2. * omega_m * (1.+z[z_L]) * (100.*h0/c_kms)**2. * chi[z_L] * (1.-chi[z_L]/chi[z_G])
				W_i[i,z_L] += W_L * n_i[z_G] * step_z

	Wij = np.zeros([nbin,nbin])
	Delta_inv = np.zeros(nbin)
	for i in xrange(nbin):
		n_i = nz[i]
		for j in xrange(nbin): #(i,nbin):
			W_j = W_i[j]
			tmp = n_i * W_j * np.array([step_z]*lenz)
			Wij[i,j] = np.sum(tmp)
			#for z_L in xrange(0,lenz): # lens galaxy
			#	Wij[i,j] += n_i[z_L] * W_j[z_L] * step_z
				#for z_G in xrange(z_L+1,lenz): # shear galaxy z_L > z_G
				#	W_L = 3./2. * omega_m * (1.+z[z_L]) * (100.*h0/c_kms)**2. * chi[z_L] * (1.-chi[z_L]/chi[z_G])
				#	Wij[i,j] += W_L * n_i[z_L] * n_j[z_G]
		tmp = n_i**2 /dchidz * np.array([step_z]*lenz)
		Delta_inv[i] = np.sum(tmp)
		#for z1 in xrange(0,lenz):
		#	Delta_inv[i] += n_i[z1]**2. / dchidz[z1] * step_z
	Delta_i = 1./Delta_inv

	print 'W_ij = ',Wij
	print 'Delta_i = ',Delta_i
	print 'Wij & Delta_i runtime = ',time()-t0
	t0=time()

	block.put("SCterms","Wij",Wij)
	block.put("SCterms","Delta_i",Delta_i)

	# eta = np.zeros([nbin,lenz])
	# for i in xrange(nbin):
	# 	for z_L in xrange(0,lenz-1): # z_L = z_g
	# 		num = 0.
	# 		den = 0.
	# 		for z_G in xrange(z_L+1,lenz): # z_G > z_L
	# 			W_L = 3./2. * omega_m * (1.+z[z_L]) * (100.*h0/c_kms)**2. * chi[z_L] * (1.-chi[z_L]/chi[z_G])
	# 			tmp = [zmin+0.2*i+step_zP*(0.5+tmp1) for tmp1 in xrange(zP_num)]
	# 			for zP_G in tmp:
	# 				for zP_g in tmp:
	# 					if zP_G < zP_g :
	# 						num += W_L * pdf(zP_G,z[z_G],sigma_z,Delta_z) * pdf(zP_g,z[z_L],sigma_z,Delta_z)
	# 					den += W_L * pdf(zP_G,z[z_G],sigma_z,Delta_z) * pdf(zP_g,z[z_L],sigma_z,Delta_z)
	# 			# num and den should * step_zP**2 * step_z, but canceled
	# 		if num==0:
	# 			eta[i,z_L] = num
	# 		else:
	# 			eta[i,z_L] = 2. * num / den
	# 		#print num,den
	#
	# block.put("SCterms","eta",eta)

	# print 'eta = ',eta
	# print 'eta runtime = ',time()-t0
	# t0=time()

	# k_mg, z_mg ,P_mg = block.get_grid(names.matter_galaxy_power, "k_h", "z", "p_k")
	#
	# Gg = np.zeros([nbin,nl])
	# GgS = np.zeros([nbin,nl])
	# for l in xrange(nl): # l loop
	# 	k = np.array([ell[l]]*lenz) / chi
	# 	P = np.zeros(lenz)
	# 	#print 'k=',k
	# 	for j in xrange(lenz): # z loop
	# 		P_z = P_mg[:,j]
	# 		tck = interpolate.splrep(k_mg, P_z)
	# 		P[j] = interpolate.splev(k[j], tck, der=0)
	# 	for i in xrange(nbin): # bin loop
	# 		tmp = P * W_i[i] * nz[i] /chi /chi * np.array([step_z]*lenz)
	# 		Gg[i,l] = np.sum(tmp)
	# 		tmp = P_z * W_i[i] * nz[i] /chi /chi *eta[i] * np.array([step_z]*lenz)
	# 		GgS[i,l] = np.sum(tmp)
	# 		# (new no dchidz term) here /dchidz to convert variables as n(z) dz = n(chi) dchi
	# Q_i = GgS / Gg
	#
	# block.put("SCterms","Q_i",Q_i)
	# block.put("SCterms","ell",ell)
	# block.put("SCterms","Gg",Gg)
	#
	# print 'C^Gg_ii = ',Gg
	# print 'Q_i = ',Q_i
	# print 'Q_i runtime = ',time()-t0

	print "~~~~~~~~~~~~~~~~~~~~~~~~~~ no error so far!"
	return 0

def cleanup(config):
	pass
