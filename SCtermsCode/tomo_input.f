        program tomo_input
		
		implicit double precision (a-h,o-z)
		integer :: i,j
		character*30 :: savefile
		
	  read(*,*) omegam,omegah,sigma8,xn,alphas,omegab,wQ0,ibn,wQ1
	  read(*,*) redshift
	  read(*,*) ng
	  read(*,*) nbin1,nbin2,z0,Dz,pzrms,zmin,zmax,nbins
	  read(*,*) Agp,alphagp,Amp0
	  
	  do i=3,9
		do j=i,9
			write(savefile,'("./bins/tomo_",I3,".in")')i*100+j
			open(unit=9,file=savefile,status='replace')
			write(9,*) omegam,omegah,sigma8,xn,alphas,omegab,wQ0,ibn,wQ1
			write(9,*) redshift
			write(9,*) ng
			write(9,*) i,j,z0,Dz,pzrms,zmin,zmax,nbins
			write(9,*) Agp,alphagp,Amp0
			close(9)
			
		enddo
	  enddo
	  
	  end