#IA model from maccrann et al. 2015 - NLA with power law redshift scaling
#P_II(k,z) = ((1+z)^alpha) * F(z))^2 * P_delta(k,z), P_GI(k,z) = (1+z)^alpha * F(z) * P_delta(k,z)
#The P_II and P_GI before the redshift scaling are already in the block
#so just read them in and rescale

from cosmosis.datablock import names, option_section
import numpy as np

def setup(options):
	method = options[option_section, "method"].lower()
	grid_mode = options.get_bool(option_section, "grid_mode", default=False)
	gal_intrinsic_power = options.get_bool(option_section,"do_galaxy_intrinsic", False)
	name = options.get_string(option_section, "name", default="").lower()
	if name:
		suffix = "_" + name
	else:
		suffix = ""

	if method not in ["krhb", "bk", "bk_corrected","linear"]:
		raise ValueError('The method in the linear alignment module must'
			'be either "KRHB" (for Kirk, Rassat, Host, Bridle) or BK for '
			'Bridle & King or "BK_corrected" for the corrected version of that')


	return method, gal_intrinsic_power, grid_mode, suffix

def execute(block, config):

	method, gal_intrinsic_power, grid_mode, suffix = config
	ia_section = names.intrinsic_alignment_parameters
	ia_gi = names.galaxy_intrinsic_power #+ suffix
	ia_ii = names.intrinsic_power #+ suffix
	ia_mi = names.matter_intrinsic_power #+ suffix

	#read in power spectra
	z,k,p_ii=block.get_grid(ia_ii,"z","k_h","p_k")
	z,k,p_gi=block.get_grid(ia_mi,"z","k_h","p_k")

	#read alpha from ia_section values section - JAZ this is a change from the internal one
	alpha = block[ia_section,'alpha']
	_,z_grid=np.meshgrid(k,z)

	#Construct and Apply redshift scaling
	z_scaling=(1+z_grid)**alpha
	p_ii*=z_scaling**2
	p_gi*=z_scaling

	# Add random (flat/gaussian) IA signal to test SC
	lenz=len(z)
	binz=np.floor(z/0.2)
	# rands=np.random.rand(25)+np.array([0.5]*25)
	rands=np.random.normal(1,0.2,25)
	print rands
	noise_z=rands[binz.astype(int)]
	#noise_z=np.random.rand(lenz)+np.array([0.5]*lenz)
	#noise_z=np.random.normal(1,0.2,lenz)
	_,noise_grid=np.meshgrid(k,noise_z)
	p_ii*=noise_grid**2
	p_gi*=noise_grid

	#Save grid
	block.replace_grid(ia_ii, "z", z, "k_h", k, "p_k", p_ii)
	block.replace_grid(ia_mi, "z", z, "k_h", k, "p_k", p_gi)
	return 0

def cleanup(config):
	pass
